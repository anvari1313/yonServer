const fs = require('fs');
const config = require('./../config/database').pg;


fs.writeFile("./config/default.json", JSON.stringify({
    "db": {
        "user": config.rootUser,
        "password": config.rootPassword,
        "host": config.host,
        "port": config.port,
        "name": config.database
    }
}), function(err) {
    if(err) {
        console.log(err);
    }
    console.log(config);
    console.log("Config file generated successfully.");
});