exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN auth_sms_token text;')
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN auth_sms_token;')
};
