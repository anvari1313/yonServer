exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public."users"' +
        '(' +
        '_id serial NOT NULL,' +
        'fname character varying(120),' +
        'lname character varying(120),' +
        'birthdate date,' +
        'point integer,' +
        'cash integer,' +
        'cell_phone character varying(14),' +
        'email citext NOT NULL,' +
        'avatar character varying(300),' +
        'password text NOT NULL,' +
        'is_verified boolean DEFAULT false,' +
        'CONSTRAINT users_pkey PRIMARY KEY (_id),' +
        'CONSTRAINT user_email_key UNIQUE (email),' +
        'CONSTRAINT user_cellphone_key UNIQUE (cell_phone)' +
        ');'
    );
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE users');
};
