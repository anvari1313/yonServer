exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN point integer;');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN point;')
};
