exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN avatar text;');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN avatar;')
};
