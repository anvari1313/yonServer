exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public."tags_exists"' +
        '(' +
        '_id serial NOT NULL,' +
        'tag_id integer NOT NULL,' +
        'restaurant_id integer NOT NULL,' +
        'CONSTRAINT tags_exists_pkey PRIMARY KEY (_id),' +
        'CONSTRAINT tags_exists_restaurant_id_fkey FOREIGN KEY (restaurant_id)' +
            'REFERENCES public.restaurants (_id) MATCH SIMPLE ON ' +
            'UPDATE NO ACTION ON DELETE CASCADE,' +
        'CONSTRAINT tags_exists_tag_id_fkey FOREIGN KEY (tag_id) ' +
            'REFERENCES public.tags (_id) MATCH SIMPLE ' +
            'ON UPDATE NO ACTION ON DELETE CASCADE' +
        ');'
    );
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE tags_exists')
};
