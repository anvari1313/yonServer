exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.users ALTER COLUMN password DROP NOT NULL;');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.users ALTER COLUMN password SET NOT NULL;');
};
