exports.up = (pgm) => {
    pgm.sql('CREATE OR REPLACE FUNCTION public."isReservationValid"(\n' +
        '    "restaurantId" integer,\n' +
        '    "tableId" text,\n' +
        '    "pDateTime" timestamp without time zone)\n' +
        '  RETURNS boolean AS\n' +
        '$BODY$DECLARE\n' +
        '\tisValidAfterMax boolean;\n' +
        '\tmaxValidTime integer;\n' +
        '\treservNum bigint;\n' +
        'BEGIN\n' +
        '\tSELECT is_valid_res_after_max INTO isValidAfterMax FROM restaurants WHERE _id = "restaurantId";\n' +
        '\tSELECT max_table INTO maxValidTime FROM restaurants WHERE _id = "restaurantId";\n' +
        '\t\n' +
        '\tIF isValidAfterMax = TRUE THEN\n' +
        '\t\tSELECT count(*) INTO reservNum FROM reservations WHERE\n' +
        '\t\t\tis_canceled = False AND\n' +
        '\t\t\trestaurant_id = "restaurantId" AND\n' +
        '\t\t\ttable_id = "tableId" AND\n' +
        '\t\t\t(datetime AT TIME ZONE \'UTC\')::date <> ("pDateTime")::date\n' +
        '\t\t\tAND (\n' +
        '\t\t\t\t(abs (EXTRACT(epoch FROM ( \n' +
        '\t\t\t\t\tCAST(datetime AS timestamp) - CAST("pDateTime" AS timestamp)))) < maxValidTime)\n' +
        '\t\t\t\tOR\n' +
        '\t\t\t\t((EXTRACT (EPOCH FROM "pDateTime")) BETWEEN (EXTRACT (EPOCH FROM datetime)) AND ((EXTRACT (EPOCH FROM datetime)) + maxValidTime))\n' +
        '\t\t\t);\n' +
        '\n' +
        '\t\tIF reservNum = 0 THEN\n' +
        '\t\t\tRETURN TRUE;\n' +
        '\t\tELSE\n' +
        '\t\t\tRETURN FALSE;\n' +
        '\t\tEND IF;\n' +
        '\tELSE\n' +
        '\t\tSELECT count(*) INTO reservNum FROM reservations AS s WHERE\n' +
        '\t\t\ts.is_canceled = False AND\n' +
        '\t\t\ts.restaurant_id = "restaurantId" AND\n' +
        '\t\t\ts.table_id = "tableId" AND\n' +
        '\t\t\t(date (s."datetime") - date("pDateTime")) = 0;\n' +
        '\n' +
        '\t\tIF reservNum = 0 THEN\n' +
        '\t\t\tRETURN TRUE;\n' +
        '\t\tELSE\n' +
        '\t\t\tRETURN FALSE;\n' +
        '\t\tEND IF;\n' +
        '\tEND IF;\n' +
        'END;$BODY$\n' +
        '  LANGUAGE plpgsql VOLATILE\n' +
        '  COST 100;\n' +
        'ALTER FUNCTION public."isReservationValid"(integer, text, timestamp without time zone)\n' +
        '  OWNER TO postgres;\n');

};

exports.down = (pgm) => {
    pgm.sql('DROP FUNCTION IF EXISTS public."isReservationValid"( ' +
        '"restaurantId" integer, ' +
        '"tableId" text, ' +
        '"pDateTime" timestamp without time zone);')
};
