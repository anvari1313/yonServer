exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.users ADD COLUMN auth_sms_token text;')
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.users DROP COLUMN auth_sms_token;')
};
