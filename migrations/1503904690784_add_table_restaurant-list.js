exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public.restaurants_lists\n' +
        '(\n' +
        '  _id serial NOT NULL,\n' +
        '  name character varying(200),\n' +
        '  description text,\n' +
        '  CONSTRAINT restaurants_lists_pkey PRIMARY KEY (_id)\n' +
        ')\n' +
        'WITH (\n' +
        '  OIDS=FALSE\n' +
        ');\n' +
        'ALTER TABLE public.restaurants_lists\n' +
        '  OWNER TO postgres;\n');
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE restaurants_lists;')
};
