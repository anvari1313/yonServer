exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public.restaurants' +
        '(' +
        '_id serial NOT NULL,' +
        'name character varying(120) NOT NULL,' +
        'branch character varying(120),' +
        'address text NOT NULL,' +
        'location point NOT NULL,' +
        'cash bigint,' +
        'email citext NOT NULL,' +
        'password text NOT NULL,' +
        'max_table integer,' +
        'price bigint,' +
        'rating real,' +
        'info json,' +
        'description text,' +
        'CONSTRAINT restaurants_pkey PRIMARY KEY (_id),' +
        'CONSTRAINT restaurants_email_key UNIQUE (email)' +
        ');'
    );
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE restaurants');
};
