exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations ADD COLUMN restaurant_id integer;' +
        'ALTER TABLE public.reservations\n' +
        '  ADD CONSTRAINT reservations_restaurant_id_fkey FOREIGN KEY (restaurant_id)\n' +
        '      REFERENCES public.restaurants (_id) MATCH SIMPLE\n' +
        '      ON UPDATE CASCADE ON DELETE CASCADE;\n');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations\n' +
        '  DROP CONSTRAINT reservations_restaurant_id_fkey;' +
        'ALTER TABLE public.reservations DROP COLUMN restaurant_id;');
};
