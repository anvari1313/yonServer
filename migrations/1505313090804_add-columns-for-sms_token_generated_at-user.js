exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.users ADD COLUMN auth_sms_token_generated_at timestamp without time zone;')
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.users DROP COLUMN auth_sms_token_generated_at;')
};
