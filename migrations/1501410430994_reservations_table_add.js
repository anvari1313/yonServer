exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public.reservations' +
        '(' +
        '_id serial NOT NULL,' +
        'user_id integer NOT NULL, ' +
        'datetime timestamp without time zone NOT NULL, ' +
        'guest_count integer NOT NULL, ' +
        'table_id text, ' +
        'is_canceled boolean NOT NULL DEFAULT false,' +
        '"createdAt" time without time zone, ' +
        '"updatedAt" time without time zone, ' +
        'CONSTRAINT reservations_pkey PRIMARY KEY (_id), ' +
        'CONSTRAINT reservations_user_id_fkey FOREIGN KEY (user_id) ' +
            'REFERENCES public.users (_id) MATCH SIMPLE ' +
            'ON UPDATE NO ACTION ON DELETE CASCADE' +
        ');');
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE reservations');
};
