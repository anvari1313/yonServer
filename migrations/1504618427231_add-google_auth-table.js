exports.up = (pgm) => {
    pgm.sql('CREATE TABLE google_auth\n' +
        '(\n' +
        '  _id serial NOT NULL,\n' +
        '  user_id integer,\n' +
        '  sub text,\n' +
        '  CONSTRAINT google_auth_pkey PRIMARY KEY (_id),\n' +
        '  CONSTRAINT google_auth_user_id_fkey FOREIGN KEY (user_id)\n' +
        '      REFERENCES users (_id) MATCH SIMPLE\n' +
        '      ON UPDATE NO ACTION ON DELETE CASCADE\n' +
        ');')
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE google_auth;')
};
