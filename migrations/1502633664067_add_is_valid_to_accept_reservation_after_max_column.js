exports.up = (pgm) => {
    pgm.sql('ALTER TABLE restaurants ADD COLUMN is_valid_res_after_max boolean;\n' +
        'ALTER TABLE restaurants ALTER COLUMN is_valid_res_after_max SET DEFAULT true;\n');

};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE restaurants DROP COLUMN is_valid_res_after_max boolean;\n');
};
