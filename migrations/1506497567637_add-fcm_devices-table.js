exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public.fcm_devices\n' +
        '(\n' +
        '  _id bigserial NOT NULL ,\n' +
        '  fcm_token_id text,\n' +
        '  user_id integer,\n' +
        '  "createdAt" timestamp without time zone DEFAULT now(),\n' +
        '  CONSTRAINT fcm_devices_pkey PRIMARY KEY (_id),\n' +
        '  CONSTRAINT fcm_devices_user_id_fkey FOREIGN KEY (user_id)\n' +
        '      REFERENCES public.users (_id) MATCH SIMPLE\n' +
        '      ON UPDATE NO ACTION ON DELETE NO ACTION\n' +
        ')\n' +
        'WITH (\n' +
        '  OIDS=FALSE\n' +
        ');');
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE public.fcm_devices;');
};
