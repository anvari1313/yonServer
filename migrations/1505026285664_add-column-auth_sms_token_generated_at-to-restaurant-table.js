exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN auth_sms_token_generated_at timestamp without time zone;')
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN auth_sms_token_generated_at;')
};
