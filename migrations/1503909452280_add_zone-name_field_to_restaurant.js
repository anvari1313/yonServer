exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN zone_name character varying(200);');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN zone_name;');
};
