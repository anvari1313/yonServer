exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.users ADD COLUMN is_cell_phone_verified boolean;')
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.users DROP COLUMN is_cell_phone_verified;')
};
