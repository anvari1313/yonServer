exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations ADD COLUMN temp_user_id integer;' +
        'ALTER TABLE public.reservations\n' +
        '  ADD CONSTRAINT reservations_temp_user_id_fkey FOREIGN KEY (temp_user_id)\n' +
        '      REFERENCES public.temp_users (_id) MATCH SIMPLE\n' +
        '      ON UPDATE NO ACTION ON DELETE CASCADE;');
};

exports.down = (pgm) => {
    pgm.sql('')
};
