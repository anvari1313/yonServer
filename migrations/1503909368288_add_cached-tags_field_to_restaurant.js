exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN cached_tags json;');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN cached_tags;');
};
