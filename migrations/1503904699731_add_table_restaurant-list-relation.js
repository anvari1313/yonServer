exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public.restaurants_lists_relations\n' +
        '(\n' +
        '  _id bigserial NOT NULL,\n' +
        '  list_id integer,\n' +
        '  restaurant_id integer,\n' +
        '  CONSTRAINT restaurants_lists_relations_pkey PRIMARY KEY (_id),\n' +
        '  CONSTRAINT restaurants_lists_relations_list_id_fkey FOREIGN KEY (list_id)\n' +
        '      REFERENCES public.restaurants_lists (_id) MATCH SIMPLE\n' +
        '      ON UPDATE NO ACTION ON DELETE NO ACTION,\n' +
        '  CONSTRAINT restaurants_lists_relations_restaurant_id_fkey FOREIGN KEY (restaurant_id)\n' +
        '      REFERENCES public.restaurants (_id) MATCH SIMPLE\n' +
        '      ON UPDATE NO ACTION ON DELETE NO ACTION\n' +
        ')\n' +
        'WITH (\n' +
        '  OIDS=FALSE\n' +
        ');\n' +
        'ALTER TABLE public.restaurants_lists_relations\n' +
        '  OWNER TO postgres;\n');
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE restaurants_lists_relations;')
};
