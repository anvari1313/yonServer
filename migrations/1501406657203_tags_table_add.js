exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public."tags"' +
        '(' +
        '_id serial NOT NULL,' +
        'name character varying(120) NOT NULL,' +
        'slug character varying(120) NOT NULL,' +
        'image text,' +
        'description text,' +
        'CONSTRAINT tags_pkey PRIMARY KEY (_id),' +
        'CONSTRAINT tags_slug_key UNIQUE (slug)' +
        ');'
    );
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE tags')
};
