exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations ALTER COLUMN user_id DROP NOT NULL;');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations ALTER COLUMN user_id SET NOT NULL;')
};
