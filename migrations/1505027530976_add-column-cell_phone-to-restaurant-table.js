exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN cell_phone character varying(14);')
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN cell_phone;')
};
