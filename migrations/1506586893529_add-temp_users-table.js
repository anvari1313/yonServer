exports.up = (pgm) => {
    pgm.sql('CREATE TABLE public.temp_users\n' +
        '(\n' +
        '  _id serial NOT NULL,\n' +
        '  name text,\n' +
        '  "createdAt" timestamp without time zone DEFAULT now(),\n' +
        '  cell_phone character varying(14),' +
        '  CONSTRAINT temp_users_pkey PRIMARY KEY (_id)\n' +
        ')\n' +
        'WITH (\n' +
        '  OIDS=FALSE\n' +
        ');'
    );
};

exports.down = (pgm) => {
    pgm.sql('DROP TABLE public.temp_users;');
};
