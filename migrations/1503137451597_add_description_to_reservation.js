exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations ADD COLUMN description text;');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.reservations DROP COLUMN description;');
};
