exports.up = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants ADD COLUMN zone_slug character varying(120);');
};

exports.down = (pgm) => {
    pgm.sql('ALTER TABLE public.restaurants DROP COLUMN zone_slug;')
};