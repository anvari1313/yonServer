require('dotenv').config();

let statics = {
    staticContentPath: process.env.STATIC_CONTENT_PATH || '/home/yon-statics',
    staticContentURL : process.env.STATIC_CONTENT_URL || '/'
};

let eatableImages = {
    featureImage:{
        path: statics.staticContentPath + 'eatable/featured/',
        url: statics.staticContentURL+ 'eatable/featured/',
    },
    album:{
        path: statics.staticContentPath + 'eatable/album/',
        url: statics.staticContentURL + 'eatable/album/',
    },
};

let restaurantImages = {
    avatar:{
        path: statics.staticContentPath + 'restaurant/avatar/',
        url: statics.staticContentURL+ 'restaurant/avatar/',
    }
};

let tagImages = {
    image: {
        path: statics.staticContentPath + 'tag/image/',
        url: statics.staticContentURL + 'tag/image/'
    }
}

module.exports.staticsPath = statics;
module.exports.eatbleImages = eatableImages;
module.exports.restaurantImages = restaurantImages;
module.exports.tagImages = tagImages;
