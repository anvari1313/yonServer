require('dotenv').config();

let mongo = {
    host: process.env.DB_MONGO_HOST || 'localhost',
    port: process.env.DB_MONGO_PORT || 27017,
    database: process.env.DB_MONGO_DATABASE || 'yon-server',
    user: process.env.DB_MONGO_USER || 'admin',
    password: process.env.DB_MONGO_PASSWORD || 'mohtadin',
    auth_db: process.env.DB_MONGO_AUTHDB || 'admin'
};

let pg = {
    host: process.env.DB_PG_HOST || '162.243.174.32',
    port: process.env.DB_PG_PORT || 5432,
    database: process.env.DB_PG_DATABASE || 'yondb',
    user: process.env.DB_PG_USER || 'yon',
    rootUser: process.env.DB_PG_ROOT_USER || 'postgres',
    password: process.env.DB_PG_PASSWORD || 'yonjedatabasepassword',
    rootPassword: process.env.DB_PG_PASSWORD_ROOT || '123'
};

let redis = {
    host: process.env.DB_REDIS_HOST || 'localhost',
    family: process.env.DB_REDIS_FAMILY || '4',
    db: process.env.DB_REDIS_DB || '0',
    password: process.env.DB_REDIS_PASSWORD || '1234',
};

let neo4j = {
    host: process.env.DB_NEO4J_HOST || 'localhost',
    protocol: process.env.DB_NEO4J_PROTOCOL || 'bolt',
    port: process.env.DB_NEO4J_PORT || 12,
    user: process.env.DB_NEO4J_USER || 'neo4j',
    password: process.env.DB_NEO4J_PASSWORD || 'neo4j'
};

module.exports = {
    pg: pg,
    mongoUrl: `mongodb://${mongo.user}:${mongo.password}@${mongo.host}:${mongo.port}/${mongo.database}?authMechanism=DEFAULT&authSource=${mongo.auth_db}`,
    neo4j: neo4j,
    redis: redis
};
