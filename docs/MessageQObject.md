# MessageQ
This class would handle the queue of the messages should be sent to client restaurant and store it in redis database in a format of a JSON hashed. Also this object would give the message to socket module. 
## Redis Object protocol
~~~javascript
{
    messages:[],
    restaurantId:12
}
~~~