# UserRequest

User requests are transfered through the Firebase platform.

One types of messages are allowed:

## ReservationCancel

~~~json
{
    "event":"/events/reservations/cancel",
    "reservation_id" : "1",
    "reservation_datetime": "123",
    "reservation_guest_count": "1",
    "restaurant_name": "Restaurant",
    "restaurant_avatar": "static.mizit.ir/restaurant/....."
}
~~~