# Socket Protocol

## Authentication

Protocol for user authentication is token. A **POST** request should be sent to `/auth/restaurant/login` with the appropriate credentials and the passed token is used to connect via socket.

This token could be passed by GET query or the header of the connection. Examples are like the following:

* GET query:
~~~javascript
client.connect('ws://localhost:8080?authorization=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNTAwNDAyNDgzfQ.mp8GB-QUNIskeDSun9wvkgpr4no7Lq9LHXpVuCRwSb4', 'echo-protocol', 'nullexp', {});
~~~

* Header request:
~~~javascript
client.connect('ws://162.243.174.32:8080', 'echo-protocol', 'nullexp', { authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTAwODczMDk3fQ.AUS2o6SCbd2eTi3Q27L-EQbp8c6FcUlPrI5E4bhIj2Q'});
~~~

## Socket Object
~~~json
{
    "event":"event-name",
    "payload":{}
}
~~~

## Special Router Object
~~~json
{
    "event":"event-name",
    "payload":{
        "restaurantId": 32,
        "payload": {/*main payload*/}
    }
}
~~~


### Possible value for event:
* getUser
