# Socket Architecture

For handling message passing between socket clients and the server three modules declared:

* **MessageQ:** A simple queue for messages that saves every received message in a list in Redis database with following key structure: `restaurant-[restaurantID]`

* **Socket:** This layer handle socket connection. With every request connections handshake authentication is checked with the `username` and `password` fields in the header and if the requested connection is valid it will be accepted otherwise connection will be rejected. For every accepted connection a callback is passed to the MessageQ module to be call whenever new message for current client is received. If the current client does'nt have connection at the time being messages in the queue should'nt be deleted from the queue.

* **ResponseHandler:** A class with two functions: `send` and `sendTo`. The `send` is used when the controller wants to send a message to the current client ,requested a request. It's obvious that the `sendTo` is used for sending a message to other client. 

* **Controller:** Controllers are modules that can interact with data and they are data data abstracted. No socket manipulation or any other things is happened here. Every controller is called with a function that get `req` and `res`. The `res` is an instant of the `ResponseHandler` that get the result of the controller.

**Caution:** Every request from the clients should have a `request_id` field to determining the related response. 