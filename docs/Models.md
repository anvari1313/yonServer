# Model class document
model class is supposed to handle database combined data, it should be completely independent from any layer.

## Class body
~~~javascript
class Obj extends Model {
    constructor(/* initial params */) {
        super();
        
        // public attributes
        
        // private attributes
    }
    
    static convertRawItem(){
        
    }
    
    static get query() {
        return {}
    }
}
~~~

1. super class constructor should be called
2. attributes should be undefined safe (use default value for constructor params)
3. every Model class child, should implement `convertRawItem()`
4. `convertRawItem()` is supposed to convert raw database object to our model
5. raw sql query strings should be saved in query getter
6. all collection are generators, for example `getAllUsers()` returns generator that must be iterated via `for...of`
