/**
 * Created by ahmad on 5/18/17.
 */
const jwt = require('jsonwebtoken');
const Promise = require('bluebird');

module.exports.verifyAsync = function (token, secretOrPublicKey) {
        return new Promise(function (resolve, reject) {
            try {
                jwt.verify(token, secretOrPublicKey, resolve);
            } catch(err) {
                reject(err);
            }
        });
    };

module.exports.decodeAsync = function (token) {

    return new Promise((resolve, reject)=>{
        if (token){
            try {
                resolve(jwt.decode(token));
            }catch (err){
                console.log(err);
                reject(err);
            }
        }else
            resolve(null);
    });
};