/**
 * Created by ahmad on 7/15/17.
 *
 * This function will get the error object of the db and parse it to appropriate error message
 */

const responseObj = require('../rest/responses/codes');

module.exports.parseError = (errorObject)=>{
    switch (errorObject.code){
        case '23505':     // Unique constraint broke up
            switch (errorObject.constraint){
                case 'user_email_key':          // Email for each user is unique
                    console.log('-------------------user_email_key--------------------');
                    return responseObj.invalidSignUpFail.repeatedEmail;
                    break;
                case 'user_cellphone_key':     // Cell phone of each user is unique
                    console.log('-------------------user_cell_phone_key--------------------');
                    return responseObj.invalidSignUpFail.repeatedCellPhone;
                    break;
                default:                        // Unknown field unique constraint
                    console.log(errorObject.constraint);
                    break;
            }
            break;
        case '22001':     // Too long data entrance
            break;

        default:
            console.log(errorObject);
            console.log(console.log(errorObject.code));
            break;
    }
};

