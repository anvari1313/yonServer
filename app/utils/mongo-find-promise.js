const Promise = require('bluebird');

module.exports.find = (collection, criteria) => new Promise((resolve, reject)=>{
        collection.find(criteria).toArray((err, result)=>{
            if(err)
                reject(err);
            else
                resolve(result);
        });
    });

module.exports.findOne = (collection, criteria) => new Promise((resolve, reject)=>{
    collection.find(criteria).toArray((err, result)=>{
        if(err)
            reject(err);
        else
            resolve(result[0]);
    });
});

module.exports.sortedFind = (collation, criteria, sortObj) => new Promise((resolve, reject)=>{
    collation.find(criteria).sort(sortObj).toArray((err, result)=>{
        if (err)
            reject(err);
        else
            resolve(result);
    });
});