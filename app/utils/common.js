const moment = require('moment');
const Promise = require('bluebird');

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    let port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

module.exports.normalizePort = normalizePort;

/**
 * This function checks whether the given parameter is after the time being or  not
 * @param parameter the time that should be checked that is in unix_timestamp format
 */
module.exports.isAfterNow = (parameter)=>{
    return new Promise((resolve, reject)=>{
        if (parameter > moment().unix())
            resolve();
        else
            reject('Not a valid unix timestamp');
    });
};

module.exports.convertTimestampToDaystamp = (timestamp) => {
    return Math.trunc(timestamp / (24 * 60 * 60));
};