/**
 * Created by amirhosein on 3/8/17.
 */
const Promise = require('bluebird');
const MongoClient = require('mongodb').MongoClient;
const pgp = require('pg-promise')({promiseLib: Promise});
const neo4j = require('neo4j-driver').v1;

const dbConfig = require('../config/database');

let mongodbConnection = null;
let pgConnection = null;
let neo4jdriver = null;


module.exports.neo4jConnect = () => new Promise((resolve, reject)=>{
    neo4jdriver = neo4j.driver(dbConfig.neo4j.protocol + '://' + dbConfig.neo4j.host, neo4j.auth.basic(dbConfig.neo4j.user, dbConfig.neo4j.password));

    // Start a session to check if the connection is OK or not
    let neo4jsession = neo4jdriver.session();
    neo4jsession.run('MATCH(n) RETURN n;').then(()=>neo4jsession.close());

    neo4jdriver.onCompleted = () => {
        // proceed with using the driver, it was successfully instantiated
        resolve(neo4jdriver);
    };

    neo4jdriver.onError = (error)=>{
        console.log('Driver instantiation failed', error);
        reject(error);
    };
});
module.exports.mongoConnect = () => new Promise(function (resolve, reject) {
    console.log(dbConfig.mongoUrl);
    MongoClient.connect(dbConfig.mongoUrl, {
        poolSize: 10,
        promiseLibrary: Promise
    }).then(function (db) {
        mongodbConnection = db;
        resolve(db)
    }).catch(function (err) {
        reject(err);
    })
});

module.exports.mongodb = () => {
    if (!mongodbConnection) {
        throw new Error("Mongo client isn't initialized!");
    }

    return mongodbConnection;
};

module.exports.neo4jDriver = ()=>{
    if (!neo4jdriver)
        throw new Error("Neo4j driver isn't initialized!");
    return neo4jdriver;
};

// module.exports.neo4jSession = ()=>{
//     if (!neo4jsession)
//         throw new Error("Neo4j session isn't initialized!");
//     return neo4jsession;
// };
module.exports.pg = () => {
    if (!pgConnection) {
        pgConnection = pgp(dbConfig.pg)
    }

    return pgConnection;
};

