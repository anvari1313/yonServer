const BasicResponse = require('./../basic');
const HttpStatus = require('http-status-codes');
const reservationErrors = require('./../codes').reservation;

class CancelReservation extends BasicResponse{
    constructor(isOk, errorType){
        if (isOk === true){
            super(200);
        }else{
            if (errorType) {
                if (errorType.reservationNotOwnedToUser === true) super(HttpStatus.FORBIDDEN, reservationErrors.notOwnedToUser.code, reservationErrors.notOwnedToUser.message);
                else super(null);
            } else
                super(null);
        }
    }
}

module.exports = CancelReservation;