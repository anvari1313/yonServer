const BasicResponse = require('./basic');

class RestaurantHome extends BasicResponse{
    constructor(response, error){
        if (response){
            super(200);
            this.response = response;
        }else{
            super(404, error.code, error.message);
        }
    }
}

module.exports = RestaurantHome;