/**
 * Created by amirhosein on 5/18/17.
 */
module.exports = {
    okStatus:{
        code: 200
    },

    notAcceptable:{
        code: 406
    },

    notFound:{
        code: 404
    },

    signUpFail:{
        code: 1030,
        message: 'Signup failed.'
    },

    invalidSignUpFail:{
        repeatedCellPhone:{
            code:1010,
            message: 'This cellphone is already existed.'
        },
        repeatedEmail:{
            code:1020,
            message: 'This email is already existed.'
        }
    },
    invalidCredentials: {
        detailed:{
            code: 2053,
            message: 'Credential details doesn\'t match.'
        },
        token:{
            code: 1033,
            message: 'Invalid authorization token.'
        },
        googleAuth:{
            code: 1034,
            message: 'Invalid google id token.'
        }
    },

    userHasNoCellPhone:{
        code: 15101,
        message: 'User has no cell_phone'
    },
    userCellPhoneNotVerified:{
        code: 15102,
        message: 'Cell_phone is not verified'
    },

    mapNotFound:{
        code:12500,
        message: 'Not a valid map_id.'
    },

    tableNotFound:{
        code:12501,
        message: 'Not a valid table_id.'
    },

    restaurantNotFound:{
        code:12502,
        message: 'Not a valid restaurant with given id.'
    },

    tableIsNotBelongedToRestaurant:{
        code: 12503,
        message: 'This table is not belonged to this restaurant.'
    },
    cellPhoneNotFound: {
        code: 12504,
        message: 'Cellphone not found'
    },

    reservationNotAllowed:{
        code: 13501,
        message: 'Yon cannot reserve this table.'
    },

    reservation: {
        notOwnedToUser: {
            code: 13201,
            message: 'Reservation not owned to user.'
        }
    },

    userNotAllowed:{
        addReview:{
            code: 14401,
            message: 'User is not allowed to add review to this restaurant because has no reservations.'
        }
    },

    validationFail:{
        code: 2030,
        message: 'Validation failed.'
    },

    validationFailCodes: {
        blankField: {
            code: 2540,
            message: 'Can\'t be blank.'
        },

        notArrayType:{
            code: 1002,
            message: 'Not of type array.'
        },
        invalidEmail: {
            code: 2537,
            message: 'Not a valid email.'
        },

        notDate:{
            code: 2538,
            message: 'Not a valid date.'
        },

        notNumber:{
            code: 2539,
            message: 'Not a valid number.'
        },

        notValidOrNotInRangeNumber:{
            code: 2540,
            message: 'Not a valid number or not in the range(%{min} to %{max}).'
        },

        tooShort: {
            code: 2543,
            message: 'Length should be %{count} or more.'
        },
        notValidTimestamp:{
            code:2541,
            message: 'Not a valid timestamp.'
        },
        notValidCellphone: {
            code: 2542,
            message: 'Not a valid cell_phone'
        }
    }
};