const BasicResponse = require('./basic');

class ReservationResponse extends BasicResponse{
    constructor(result, error){
        if (result){
            super(200);
            this.response = result;
        }else if(error){
            super(406, null, error);
        }else{
            super(500);
        }
    }
}

module.exports = ReservationResponse;