/**
 * Created by ahmad on 6/11/17.
 */
const BasicResponse = require('./basic');
const invalidCredentials = require('./codes').invalidCredentials.detailed;
const invalidToken = require('./codes').invalidCredentials.token;
const invalidGoogleAuthToken = require('./codes').invalidCredentials.googleAuth;

class UserAuthResponse extends BasicResponse{
    constructor(isSuccessful, token, user, isInvalidToken, googleAuth){
        let httpStatus, code, message;
        if (isSuccessful) {
            httpStatus = 200;
        } else {
            httpStatus = 401;

            if(isInvalidToken){
                if (googleAuth === true) {
                    // When invalid google token is provided
                    code = invalidGoogleAuthToken.code;
                    message = invalidGoogleAuthToken.message;
                } else {
                    // When invalid token is provided
                    code = invalidToken.code;
                    message = invalidToken.message;
                }
            }else{
                // when invalid authentication details are provided
                code = invalidCredentials.code;
                message = invalidCredentials.message;
            }
        }

        super(httpStatus, code, message);

        if (token)
            this.token = token;

        if (user)
            this.user = user;

    }
}

module.exports = UserAuthResponse;