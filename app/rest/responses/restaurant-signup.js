/**
 * Created by ahmad on 6/6/17.
 */
const BasicResponse = require('./basic');
const invalidSignUpFail = require('./codes').invalidSignUpFail;

class SignUpResponse extends BasicResponse {
    constructor(isSuccessful, error, token) {
        let httpStatus, code, message;
        if (isSuccessful) {
            httpStatus = 200;
        } else {

            // when invalid sign up details are provided
            httpStatus = 400;
            code = error.code;
            message = error.message;
        }


        super(httpStatus, code, message);

        if (token)
            this.token = token;
    }
}

module.exports = SignUpResponse;