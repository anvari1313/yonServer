const HttpStatus = require('http-status-codes');
const BasicResponse = require('./../basic');

class UserDeviceResponse extends BasicResponse{
    constructor(result, errorType, validationErrorObject){
        if (result){
            super(HttpStatus.OK);
            this.response = result;
        }else if (errorType) {
            if (errorType.fcmTokenNotOwnedToUser === true) super(HttpStatus.FORBIDDEN, 555, 'not owned to user');
            else super(HttpStatus.INTERNAL_SERVER_ERROR);
        } else if (validationErrorObject) {
            super(HttpStatus.NOT_ACCEPTABLE);
            this.response = validationErrorObject;
        } else {
            super(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

module.exports = UserDeviceResponse;