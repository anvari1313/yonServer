const BasicResponse = require('./basic');

class LocationResponse extends BasicResponse{
    constructor(result){
        if (result){
            super(200);
            if (Array.isArray(result)) this.locations = result;
            else this.location = result;
        }else{
            super(406, null, 'Should give long&lat or name.');
        }
    }
}

module.exports = LocationResponse;