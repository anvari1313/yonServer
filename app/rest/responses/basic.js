/**
 * Created by amirhosein on 5/18/17.
 */
/**
 * Object returned to api end point
 *
 * @property httpStatus:int raw HTTP status code example: 200, 400, 401 & etc
 * @property code:int computer readable code to recognize message
 * @property message:string human readable message
 */
class BasicResponse {
    constructor(httpStatus, messageCode, message) {
        this.httpStatus = httpStatus || 500;

        if (messageCode)
            this.code = messageCode;

        if (message)
            this.message = message;
    }

    removeFields(...fields) {
        this.removedFields = fields;
        return this;
    }

    export() {
        this.httpStatus = undefined;
        if (this.removedFields) {
            for (let field of this.removedFields) {
                this[field] = undefined;
            }
            this.removedFields = undefined;
        }

        if (this.response) return this.response;
        else return this;
    }
}

module.exports = BasicResponse;