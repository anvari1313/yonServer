const BasicResponse = require('./basic');
const code = require('./codes');

class ReviewResponse extends BasicResponse{
    constructor(result){
        if (result){
            super(200);
            if (result.menu_sections) this.response = result.menu_sections;

        // }else if(insertionDenied) {
        //     super(403, null, code.userNotAllowed.addReview);
        // }else if(insertionError){
        //     super(406, null, insertionError);
        // }else{
        //     super();
        }else{
            super();
        }
    }
}

module.exports = ReviewResponse;