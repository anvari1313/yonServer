const BasicResponse = require('./basic');

class RestaurantHomePendingReservation extends BasicResponse{
    constructor(restaurant, pendingOfCurrentRestaurant, error){
        if (restaurant){
            super(200);
            this.restaurant = restaurant;
            if (pendingOfCurrentRestaurant) this.reservations = pendingOfCurrentRestaurant;
        }else{
            super(404, error.code, error.message);
        }
    }
}

module.exports = RestaurantHomePendingReservation;