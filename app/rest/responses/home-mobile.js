const BasicResponse = require('./basic');

class HomeMobile extends BasicResponse{
    constructor(list, location,errorMessage, errorCode){
        if (list){
            super(200);
            this.location = location;
            this.items = list;
        }else{
            super(null, errorMessage, errorCode);
        }
    }
}

module.exports = HomeMobile;