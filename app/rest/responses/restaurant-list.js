const BasicResponse = require('./basic');

class RestaurantListResponse extends BasicResponse{
    constructor(result){
        if (result){
            super(200);
            this.response = result
        }else{
            super();
        }
    }
}

module.exports = RestaurantListResponse;