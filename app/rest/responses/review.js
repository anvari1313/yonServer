const BasicResponse = require('./basic');
const code = require('./codes');

class ReviewResponse extends BasicResponse{
    constructor(result, insertionDenied, insertionError){
        if (result){
            super(200);
            if (result.review) this.response = result.review;
            else if (result.reviews) this.response = result.reviews;

        }else if(insertionDenied) {
            super(403, null, code.userNotAllowed.addReview);
        }else if(insertionError){
            super(406, null, insertionError);
        }else{
            super();
        }
    }
}

module.exports = ReviewResponse;