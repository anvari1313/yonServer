/**
 * Created by amirhosein on 5/18/17.
 */
const BasicResponse = require('./basic');
const validationFail = require('./codes').validationFail;

class ValidationFailResponse extends BasicResponse {
    constructor(errors) {
        super(422, validationFail.code, validationFail.message);

        if (errors)
            this.errors = errors;
    }
}

module.exports = ValidationFailResponse;