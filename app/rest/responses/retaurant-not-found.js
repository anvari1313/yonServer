const BasicResponse = require('./basic');
const error = require('./codes');

class RestaurantNotFound extends BasicResponse{
    constructor(){
        super(404, error.restaurantNotFound.code, error.restaurantNotFound.message);
    }
}

module.exports = RestaurantNotFound;