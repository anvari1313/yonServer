const HttpStatus = require('http-status-codes');
const BasicResponse = require('./basic');

const cellPhoneNotFound = require('./codes').cellPhoneNotFound;
const blankField = require('./codes').validationFailCodes.blankField;
const userHasNoCellPhone = require('./codes').userHasNoCellPhone;
const userCellPhoneNotVerified = require('./codes').userCellPhoneNotVerified;
const invalidGoogleAuthToken = require('./codes').invalidCredentials.googleAuth;

class UserAuthentication extends BasicResponse{
    constructor(acceptedResponse, errorType, validationErrorObject){
        if (acceptedResponse){
            super(HttpStatus.OK);
            if (typeof  acceptedResponse === 'object') {
                this.token = acceptedResponse.token;
                this.user = acceptedResponse.user;
            }
        } else if (errorType) {
            console.log(errorType);
            if (errorType.cellPhoneNotProvided === true) super(HttpStatus.NOT_ACCEPTABLE, blankField.code, blankField.message);
            else if (errorType.cellPhoneNotFound === true) super(HttpStatus.NOT_ACCEPTABLE, cellPhoneNotFound.code, cellPhoneNotFound.message);
            else if (errorType.userHasNoCellPhone === true) super(HttpStatus.FORBIDDEN, userHasNoCellPhone.code, userHasNoCellPhone.message);
            else if (errorType.unauthorized === true) super(HttpStatus.FORBIDDEN);
            else if (errorType.cellPhoneNotVerified === true) super(HttpStatus.FORBIDDEN, userCellPhoneNotVerified.code, userCellPhoneNotVerified.message);
            else if (errorType.invalidGoogleIdToken === true) super(HttpStatus.FORBIDDEN, invalidGoogleAuthToken.code, invalidGoogleAuthToken.message);
            else if (errorType.cellPhoneNotVerifiedButUserCreated === true) super(HttpStatus.CREATED, userCellPhoneNotVerified.code, userCellPhoneNotVerified.message);

            if (validationErrorObject) this.cell_phone = ((validationErrorObject.cell_phone) ? validationErrorObject.cell_phone : undefined);

        } else if (validationErrorObject) {
            super(HttpStatus.NOT_ACCEPTABLE);
            this.response = validationErrorObject;
        } else
            super();

    }
}

module.exports = UserAuthentication;