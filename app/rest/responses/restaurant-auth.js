/**
 * Created by amirhosein on 5/18/17.
 */
const BasicResponse = require('./basic');
const invalidCredentials = require('./codes').invalidCredentials;

class RestaurantAuthResponse extends BasicResponse {
    constructor(isSuccessful, token, restaurant) {
        let httpStatus, code, message;
        if (isSuccessful) {
            httpStatus = 200;
        } else {
            // when invalid authentication details are provided
            httpStatus = 401;
            code = invalidCredentials.code;
            message = invalidCredentials.message;
        }

        super(httpStatus, code, message);

        if (token)
            this.token = token;

        if (restaurant)
            this.restaurant = restaurant;
    }
}

module.exports = RestaurantAuthResponse;