const BasicResponse = require('./basic');

class SearchResponse extends BasicResponse{
    constructor(restaurants, tags, zones, error){
        if (!error) {
            super(200);
            if (restaurants) this.restaurants = restaurants;
            if (tags) this.tags = tags;
            if (zones) this.zones = zones;
        }else{
            super();
        }
    }
}

module.exports = SearchResponse;