const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const Promise = require('bluebird');

const index = require('./route/index');
const restaurant = require('./route/restaurant');
const example = require('./route/example');
const user = require('./route/user');
const home = require('./route/home');
const banner = require('./route/banner');
const location = require('./route/location');
const reservation = require('./route/reservation');
const review = require('./route/review');
const menu = require('./route/menu');
const tag = require('./route/tag');
const search = require('./route/search');
const restaurantList = require('./route/restaurant-list');
const admin = require('./route/admin');
const cors = require('cors');

const util = require('./route/util');

const passport = require('./middlewares/passport-jwt-strategy').passport;
const jwt_auth_middleware = require('./middlewares/passport-jwt-strategy').jwt_auth_middleware;
const restaurant_finder_middleware = require('./middlewares/restaurant-finder').findRestaurant;

const jwt_extractor = require('./middlewares/jwt-extractor');

const multer = require('multer');

global.Promise = Promise;

let app = express();

app.use(cors());
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'public')));

// Start from here

app.use(passport.initialize());
// End of test

app.use('/', index);
app.use('/util', util);
app.use('/example', example);
app.use('/restaurant', jwt_extractor, restaurant);
app.use('/user', user);
app.use('/home', jwt_extractor, home);
app.use('/banners', banner);
app.use('/location', jwt_extractor, location);
app.use('/restaurant/:restaurant_id/reservation', restaurant_finder_middleware, reservation);
app.use('/restaurant/:restaurant_id/review', restaurant_finder_middleware, review);
app.use('/restaurant/:restaurant_id/menu', restaurant_finder_middleware, menu);
app.use('/restaurant/list', restaurantList);
app.use('/tag', tag);
app.use('/search', search);
app.use('/admin', (r,s, n)=> {console.log('s'); n()}, admin);

app.use('/secrete', jwt_auth_middleware, (req, res)=>{
    res.json({state:'success', user: req.user});
});

// catch 404 and forward to error handler
app.use((req, res, next)=>{
    res.status(404).json({ERROR:'NOT FOUND'})
});

// error handler
app.use((err, req, res, next)=>{
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.httpStatus = err.httpStatus || 500;
    res.send(err);
});

module.exports.app = app;