const express = require('express');
const Promise = require('bluebird');
const validate = require('validate.js');
const moment = require('moment-jalaali');

const ValidationFailResponse = require('../responses/validation-fail');

const rules = require('../../validation-rules/location');
const HomeMobileResponse = require('../responses/home-mobile');
const zoneController = require('./../../controllers/zone');
const BasicResponse = require('../responses/basic');
const LocationResponse = require('../responses/location');

validate.Promise = Promise;
let router = express.Router();

router.get('/search', function (req, res, next) {
    let long = req.query['long'];
    let lat = req.query['lat'];
    let name = req.query['name'];
    if(name){
        zoneController.searchByName(name).
        then((response)=>{
            let r = new LocationResponse(response);
            res.status(r.httpStatus).json(r.export())}).
        catch((error)=>{
            res.status(500).json({});
            console.log(error);
        });

    }else if (long && lat){
        validate.async({long: long, lat: lat}, rules.searchByLongLat, {cleanAttributes: true}).
        then((result)=>zoneController.nearestZone({long:parseFloat(result.long), lat: parseFloat(result.lat)})).
        then((result)=> {
            let response = new LocationResponse(result);
            res.status(response.httpStatus).json(response.export()) }).
        catch((error)=>{
            if (error instanceof BasicResponse)
                res.status(error.httpStatus).json(error.export());
            else {
                res.status(406).json(error);
            }
        });
    } else {
        let notAllParameterGivenResponse = new LocationResponse(null, null ,'Not all paramters specified in request');
        res.status(406).json(notAllParameterGivenResponse.export());
    }
});

module.exports = router;