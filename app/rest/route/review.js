const express = require('express');
const Promise = require('bluebird');
const jwt_auth_middleware = require('../middlewares/passport-jwt-strategy').jwt_auth_middleware;
const reviewController = require('./../../controllers/review');
const userController = require('./../../controllers/user');
const ReviewResponse = require('../responses/review');
const rules = require('../../validation-rules/review');
const validate = require('validate.js');

let router = express.Router();

router.get('/', (req, res)=>{
    let response;
    reviewController.getReviewsOfRestaurant(req.restaurant._id).
    then((result)=>{
        response = new ReviewResponse({reviews: result});
        res.status(response.httpStatus).json(response.export());
    }).catch((error)=>{
        response = new ReviewResponse();
        res.status(response.httpStatus).json(response.export());
    });
});

router.post('/new', jwt_auth_middleware, (req, res)=>{
    let response;
    let reviewObj;

    reviewController.isAllowToSendReview(req.user.id).
    then(()=> validate.async(req.body, rules.add, {cleanAttributes: true})).
    then((attr)=> { reviewObj = attr; return userController.getUserById(req.user.id);}).
    then((user)=> reviewController.addReview(req.restaurant._id, user, {rate: reviewObj.rate, text: reviewObj.text})).
    then((result)=>{
        response = new ReviewResponse({review:reviewObj});
        res.status(response.httpStatus).json(response.export());
    }).
    catch((error)=>{
        if (error instanceof Error)
            throw error;
        else{
            console.log(error);
            if (error === 'user has no reservation') response = new ReviewResponse(null, true);
            else response = new ReviewResponse(null, false, error);

            res.status(response.httpStatus).json(response.export());
        }
    }).
    catch((error)=>{
        console.log(error);
        res.status(500).json({s:'error'});
    });
});

module.exports = router;