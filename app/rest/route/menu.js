const express = require('express');
const Promise = require('bluebird');
const menuSectionController = require('./../../controllers/menu-section');
const MenuResponse = require('../responses/menu');


let router = express.Router();

router.get('/', (req, res)=>{
    let response;
    menuSectionController.getMenuSectionsOfRestaurant(req.restaurant._id).
    then((result)=>{
        response = new MenuResponse({menu_sections:result});
        res.status(response.httpStatus).json(response.export());
    }).
    catch((error)=>{
        response = new MenuResponse();
        res.status(response.httpStatus).json(response.export());
    });
});


module.exports = router;