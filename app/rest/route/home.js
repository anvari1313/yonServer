const express = require('express');
const Promise = require('bluebird');
const validate = require('validate.js');
const homeController = require('./../../controllers/home');
const userController = require('../../controllers/user');
const moment = require('moment-jalaali');
const BasicResponse = require('../responses/basic');

const ValidationFailResponse = require('../responses/validation-fail');

const rules = require('../../validation-rules/user');
const HomeMobileResponse = require('../responses/home-mobile');

validate.Promise = Promise;
let router = express.Router();

router.get('/mobile', function (req, res, next) {
    let authorizationHeader = req.header('Authorization');

    let token = null;
    if(authorizationHeader){ token = authorizationHeader.split(' ')[1];
        userController.decodeToken(token).
        then((decoded) =>
            {
                let userId = (decoded)? decoded.id: null;
                return homeController.homeMobile({
                    long: req.query['long'],
                    lat: req.query['lat'],
                    userId: userId});
            }
        ).
        then((resObj) => res.status(resObj.httpStatus).json(resObj.export())).
        catch((error) => {
            if (error instanceof BasicResponse)
                res.status(error.httpStatus).json(error.export());
            else
                res.status(500).json(error);
        });
    }else{
        homeController.homeMobile({
            long: req.query['long'],
            lat: req.query['lat']}).
        then((resObj) => res.status(resObj.httpStatus).json(resObj.export())).
        catch((error) => {
            if (error instanceof BasicResponse)
                res.status(error.httpStatus).json(error.export());
            else
                res.status(500).json(error);
        });
    }

});

module.exports = router;
