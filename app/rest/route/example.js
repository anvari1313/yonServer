const express = require('express');
const db = require('../../database');

const Restaurant = require('../../models/restaurant');
const Reservation = require('../../models/reservation');
const Tag = require('../../models/tag');
const User = require('../../models/user');
const messageQ = require('../../socket/messageQ/messageQ');

/* GET example listing. */
let router = express.Router();
router.get('/user', function (req, res, next) {
    db.pg().one(User.query.findById, [1])
        .then(data => User.convert(data))
        .then(user => {
            console.log(user);
            console.log(user.birthdate);
            res.send('ok');
        })
        .catch(error => {
            console.log(error);
            res.send('error');
        });
});

router.get('/reservation', function (req, res, next) {
    Reservation.findById(1, db.mongodb(), db.pg())
        .then(reservation => res.send(reservation))
        .catch(error => console.log(error));
});

router.get('/route', function (req, res, next) {
    db.pg().one(Restaurant.query.findById, [2])
        .then(data => Restaurant.convert(data))
        .then(restaurant => restaurant.getTags(db.pg()))
        .then(tags => {
            for (let tag of tags) {
                console.log(tag);
            }

            res.send('ok');
        })
        .catch(error => {
            console.log(error);
            res.send('error');
        });
});

router.get('/tag', function (req, res, next) {
    db.pg().one(Tag.query.findById, [1])
        .then(data => Tag.convert(data))
        .then(tag => tag.getRestaurants(db.pg()))
        .then(restaurants => {
            for (let restaurant of restaurants)
                console.log(restaurant);

            res.send('ok');
        })
        .catch(error => {
            console.log(error);
            res.send('error');
        });
});

router.get('/send', (req, res)=>{
    console.log(req.query.id);
    messageQ.sendRequest(req.query.id, 'addFood', {'this':'that'});
    res.json({status:'successful'});
});

router.get('/test', (req, res)=>{
    const moment = require('moment-jalaali');

    let days = [];
    let date = moment(moment().unix() * 1000);
    for (let i = 0; i < 7; i++){
        days.push({d: date.format('jYYYY/jM/jD'), w: date.day(), s: date.unix()});
        date = date.add('days', 5);
    }

    res.json({now: moment(new Date(moment().unix() * 1000)).day(), timestamp: moment().format('jYYYY/jM/jD'), days: days});
});
router.post('/send', (req, res)=>{
    messageQ.sendRequest(parseInt(req.body.id), req.body.event, req.body.payload);
    res.json(req.body);
});
module.exports = router;
