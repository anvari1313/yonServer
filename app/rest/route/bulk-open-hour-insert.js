const Router = require('express').Router;
const Promise = require('bluebird');
const db = require('../../database');

function generateOpenHour(id) {
    return {
        "restaurant_id": id,
        "base": [
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ],
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ],
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ],
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ],
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ],
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ],
            [
                {
                    "start": 39600,
                    "end": 59400
                },
                {
                    "start": 70200,
                    "end": 86400
                }
            ]
        ],
        "exceptions": [
            {
                "date": 1504051200,
                "open": [
                    {
                        "start": 80200,
                        "end": 96400
                    }
                ]
            }
        ]
    };
}

let router = new Router();

router.get("/an", (req, res, next) => {
    let ids = [];
    db.pg().many('select _id from restaurants where _id != 1 and _id != 100')
        .then(_ids => ids = _ids)
        .then(x => db.mongoConnect())
        .then(x => {
            let promises = [];
            for (let r of ids)
                promises.push(db.mongodb().collection('open_hours').insertOne(generateOpenHour(r._id)))

            return Promise.all(promises);
        })
        .then(x => res.send("ok"))
        .catch(exp => {
            console.log(exp);
            res.send("fail");
        })
});

module.exports = router;