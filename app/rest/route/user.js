const express = require('express');
const Promise = require('bluebird');
const validate = require('validate.js');
const moment = require('moment-jalaali');

const ValidationFailResponse = require('../responses/validation-fail');
let userController = require('../../controllers/user');
const fcmDeviceController = require('../../controllers/fcm-device');
let reservationController = require('../../controllers/reservation');

const UserAuthResponse = require('../responses/user-auth');
const UserAuthenticationResponse = require('./../responses/user-authentication');
const UserDeviceResponse = require('./../responses/user/device');
const rules = require('../../validation-rules/user');

const jwt_auth_middleware = require('../middlewares/passport-jwt-strategy').jwt_auth_middleware;
const jwt_extractor = require('./../middlewares/jwt-extractor');

validate.Promise = Promise;
let router = express.Router();

validate.extend(validate.validators.datetime, {
    // The value is guaranteed not to be null or undefined but otherwise it
    // could be anything.
    parse: function (value, options) {
        return +moment.utc(value);
    },
    // Input is a unix timestamp
    format: function (value, options) {
        var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
        return moment.utc(value).format(format);
    }
});
router.post('/auth/login', (req, res, next) => {
    validate.async(req.body, rules.login, {cleanAttributes: true}).then((attrs) => userController.login(attrs.email, attrs.password)).then(result => {
        let response = new UserAuthResponse(true, result.token, result.user);
        res.status(result.httpStatus).json(result.export());
    }).catch(error => {
        console.log(error);
        if (error instanceof Error)
            throw error;

        let response = new ValidationFailResponse(error);
        res.status(response.httpStatus).send(response.export());
    }).catch(error => {
        res.status(500).send('error');
    });
});

router.get('/auth/token', (req, res) => {
    let cellPhone = req.query['cell_phone'];
    let response;

    ((cellPhone !== undefined) ? Promise.resolve() : Promise.reject('no cell_phone')).
    then(() => userController.sendSMSToken(cellPhone)).
    then(() => {
        response = new UserAuthenticationResponse(true);
        res.status(response.httpStatus).json(response.export());
    }).catch(error => {
        switch (error) {
            case 'cell_phone not found':
                response = new UserAuthenticationResponse(null, {cellPhoneNotFound: true});
                break;
            case 'no cell_phone':
                response = new UserAuthenticationResponse(null, {cellPhoneNotProvided: true});
                break;
            default:
                response = new UserAuthenticationResponse(null);
        }
        console.log('response is :');
        console.log(response);
        console.log('end of response');
        res.status(response.httpStatus).json(response.export());
    });
});

router.post('/auth/login/token', (req, res) => {
    validate.async(req.body, rules.tokenLogin, {cleanAttributes: true}).
    then(attr => userController.tokenLogin(attr.cell_phone, attr.sms_token)).
    then(result => {
        let response = new UserAuthResponse(true, result.token, result.user);
        res.status(response.httpStatus).json(response.export());
    }).catch(error => {
        console.log(error);
        if (error instanceof Error)
            throw error;

        let response = new ValidationFailResponse(error);
        res.status(response.httpStatus).send(response.export());
    }).catch(error => {
        res.status(500).send('error');
    });
});

router.post('/auth/signup', (req, res, next) => {
    let response;
    validate.async(req.body, rules.signup).then((attributes) => {
        let user = attributes;
        return userController.localSignUp(user);
    }).then(result => {
        res.status(result.httpStatus).json(result.export());
    })
        .catch(error => {
            console.log(error);
            switch (error){
                case '':
                    response = new UserAuthenticationResponse();
                    break;
                case '':
                    response = new UserAuthenticationResponse();
                    break;
                default:
                    if (typeof error === 'object') {
                        if (error.cellPhoneNotVerified === true) response = new UserAuthenticationResponse(null, { cellPhoneNotVerified: true }, error);
                        else if (error.cellPhoneNotVerifiedButUserCreated === true) response = new UserAuthenticationResponse(null, { cellPhoneNotVerifiedButUserCreated: true }, error);
                        else response = new UserAuthenticationResponse(null, null, error);
                    } else
                        response = new UserAuthenticationResponse();
            }
            res.status(response.httpStatus).json(response.export());
        })
});

router.post('/auth/google', (req, res) => {
    // let response;
    // userController.googleAuth(req.body.token).
    // then(result => {
    //     response = response = new UserAuthResponse(true, result.token, result.user);
    //     res.status(response.httpStatus).json(response.export())
    // }).
    // catch(error => {
    //     if (error === 'google auth token is not valid')
    //         response = response = new UserAuthResponse(false, null, null, true, true);
    //     else
    //         response = response = new UserAuthResponse(false);
    //     res.status(response.httpStatus).json(response.export())
    // });

    let response;

    validate.async(req.body, rules.googleAuth).
    then(body => {
        let idToken = body['id_token'];
        let cellPhone = body['cell_phone'];

        let promise;

        promise = ((cellPhone !== undefined) ? userController.googleAuthWithCellphone(idToken, cellPhone) : userController.googleAuth(idToken));

        promise.catch(error => {console.log(error)});
        return promise;
    }).
    then(result => { let response = new UserAuthenticationResponse(result); res.status(response.httpStatus).json(response.export()) }).
    catch(error => {

        switch (error){
            case 'unauthorized':
                response = new UserAuthenticationResponse(null, { unauthorized: true});
                break;
            case 'invalid google_id_token':
                response = new UserAuthenticationResponse(null, { invalidGoogleIdToken: true});
                break;
            case 'no cell_phone':
                response = new UserAuthenticationResponse(null, { userHasNoCellPhone: true });
                break;
            default:
                if (typeof error === 'object') {
                    if (error.cellPhoneNotVerified === true) response = new UserAuthenticationResponse(null, { cellPhoneNotVerified: true }, error);
                    else response = new UserAuthenticationResponse(null, null, error);
                } else
                    response = new UserAuthenticationResponse();
        }

        res.status(response.httpStatus).json(response.export());
    })
});

router.get('/reservation/pending', jwt_auth_middleware, (req, res) => {
    reservationController.getAllReservationsOfAUser(req.user.id).
    then((result) => res.json(result)).catch(error => console.log(error));
});

router.post('/device', jwt_extractor, (req, res) => {
    let response;
    console.log(req.body);
    validate.async(req.body, rules.device.new).
    then(attr => fcmDeviceController.newDevice((req.user)? req.user.id: undefined, attr.new_fcm_token)).
    then(result => {
        response = new UserDeviceResponse(result);
        res.status(response.httpStatus).json(response.export());
    }).
    catch(error => {
        console.log(error);
        response = new UserDeviceResponse(null, null, error);
        res.status(response.httpStatus).json(response.export());
    });
});

router.put('/device', jwt_extractor, (req, res) => {
    let response;

    validate.async(req.body, rules.device.update).
    then(attr => fcmDeviceController.updateByLastToken(attr.old_fcm_token, attr.new_fcm_token, (req.user)? req.user.id: undefined)).
    then(result => {
        response = new UserDeviceResponse(result);
        res.status(response.httpStatus).json(response.export());
    }).
    catch(error => {
        console.log(error);
        switch (error){
            case 'unauthorized':
                response = new UserDeviceResponse(null, {fcmTokenNotOwnedToUser: true});
                break;
            default:
                response = new UserDeviceResponse(null, null, error);
        }

        res.status(response.httpStatus).json(response.export());
    });
});

router.get('/device', jwt_extractor, (req, res) => {

    fcmDeviceController.fcmTokensOfUser(req.user.id).
    then(result => res.json(result))
});

module.exports = router;
