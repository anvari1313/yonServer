var express = require('express');
var router = express.Router();
const messageQ = require('../../socket/messageQ/messageQ');

/* GET home page. */
router.get('/', function(req, res, next) {
  // res.render('index', { title: 'Express' });
    res.httpStatus(200).json({some:'this', hello:12});
    messageQ.sendRequest(2, 'someEvent', {'this': 'that'});
});

router.post('/removemessages', function(req, res, next) {
    // res.render('index', { title: 'Express' });
    console.log('some is this');
    const jwt = require('jsonwebtoken');
    const jwt_secret_key = require('./../../../config/secrets').jwt;

    let decodedJWTToken = jwt.decode(req.body.token, jwt_secret_key);
    require('../../socket/messageQ/messageQ').clearMessages(decodedJWTToken.id).
    then((_res)=>console.log(_res));
    res.httpStatus(200).json({thi:'that'});
});



module.exports = router;
