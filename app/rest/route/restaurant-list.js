const express = require('express');
let router = express.Router();
const RestaurantList = require('../responses/restaurant-list');
const restaurantListController = require('./../../controllers/restaurant-list');

router.get('/:list_id', (req, res)=>{
    let response;
    let listId = req.params.list_id;
    restaurantListController.getList(listId).
    then(result => { response = new RestaurantList(result); res.status(response.httpStatus).json(response.export()); }).
    catch(error => { response = new RestaurantList(); console.log(error); res.status(response.httpStatus).json(response.export()); })
});
router.get('/new', (req, res, next)=>{

});

module.exports = router;
