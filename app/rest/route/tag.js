const express = require('express');
const Promise = require('bluebird');
const validate = require('validate.js');

const tagController = require('./../../controllers/tag');


const RestaurantHomeResponse = require('../responses/restaurant-home');


validate.Promise = Promise;
let router = express.Router();

router.get('/', (req, res)=>{
    let name = req.query['name'] || '';

    let response;
    tagController.getAllTags(name).
    then((result)=>{
        console.log(result);
        response = new RestaurantHomeResponse(result);
        res.status(response.httpStatus).json(response.export())
    }).catch((error)=>{
        console.log(error);
        response = new RestaurantHomeResponse();
        res.status(response.httpStatus).json(response.export())
    })
});

module.exports = router;