const express = require('express');
const Promise = require('bluebird');
const validate = require('validate.js');

const ValidationFailResponse = require('../responses/validation-fail');
const restaurantController = require('../../controllers/restaurant');
const reservationController = require('../../controllers/reservation');
const tagController = require('../../controllers/tag');
const mapController = require('../../controllers/map');
const openHourController = require('../../controllers/open-hour');

const RestaurantHomeResponse = require('../responses/restaurant-home');
const RestaurantHomePendingReservation = require('../responses/restaurant-home-pending-res');
const validationCode = require('../responses/codes');
const restaurant_finder_middleware = require('../middlewares/restaurant-finder').findRestaurant;
const rules = require('../../validation-rules/restaurant');

validate.Promise = Promise;
let router = express.Router();

/**
 * Home route for query of zone Or tags
 */
router.get('/', (req, res, next) => {
    let zone = req.query['zone'];
    let tags = req.query['tag'];

    let long = req.query['long'];
    let lat = req.query['lat'];
    let location;
    if (long && lat) location = {x: long, y: lat};
    let response;

    if (zone){
        restaurantController.searchByZone(zone, location).
        then( result => {
            response = new RestaurantHomeResponse(result);
            res.status(response.httpStatus).json(response.export());
        }).catch((error)=>{
            console.log(error);
            response = new RestaurantHomeResponse();
            res.status(response.httpStatus).json(response.export());
        });
    } else if (tags){
        if (!Array.isArray(tags)) { tags = []; tags[0] = req.query['tag']; }
        restaurantController.searchByTag(tags, location).
        then( result => {
            response = new RestaurantHomeResponse(result);
            res.status(response.httpStatus).json(response.export());
        }).catch((error)=>{
            console.log(error);
            response = new RestaurantHomeResponse();
            res.status(response.httpStatus).json(response.export());
        });
    } else
        next();
});

router.get('/search', (req, res) => {
    let response;
    let query = req.query['q'];
    restaurantController.searchByNameOrBranch(query).
    then(result => { response = new RestaurantHomeResponse(result); res.status(response.httpStatus).json(response.export()); }).
    catch(error => { response = new RestaurantHomeResponse(null, error); res.status(response.httpStatus).json(response.export()); })
});

router.get('/auth/token', (req, res) => {
    let cell_phone = req.query['cell_phone'];
    restaurantController.sendSMSToken(cell_phone).
    then(result => res.json(result)).
    catch(error => res.status(500).json(error));
});

router.post('/auth/login', (req, res) => {
    validate.async(req.body, rules.login, {cleanAttributes: true}).then(attrs => restaurantController.login(attrs.email, attrs.password)).then(result => {
        res.status(result.httpStatus).json(result.export());
    }).catch(error => {
        console.log(error);
        if (error instanceof Error)
            throw error;

        let response = new ValidationFailResponse(error);
        res.status(response.httpStatus).send(response.export());
    }).catch(error => {
        res.status(500).send('error');
    });
});

router.post('/auth/login/token', (req, res) => {
    let cellPhone = req.body.cell_phone;
    let smsToken = req.body.sms_token;

    restaurantController.loginSMSToken(cellPhone, smsToken).
    then(result => res.status(result.httpStatus).json(result.export())).
    catch(error => { console.log(error); res.status(500).json(error);});
});

router.post('/auth/signup', (req, res) => {
    validate.async(req.body, rules.login, {cleanAttributes: true}).then(attrs => restaurantController.login(attrs.email, attrs.password)).then(result => {
        res.status(result.httpStatus).json(result.export());
    }).catch(error => {
        console.log(error);
        if (error instanceof Error)
            throw error;

        let response = new ValidationFailResponse(error);
        res.status(response.httpStatus).send(response.export());
    }).catch(error => {
        res.status(500).send('error');
    });
});

router.get('/:restaurant_id', (req, res, next) => {
    let restaurantId;
    let restaurant;

    let pendingReservationOfUser;
    restaurantId = parseInt(req.params.restaurant_id);

    let promise = (req.user) ? reservationController.getReservationsOfAUserOfRestaurant(req.user.id, restaurantId) : Promise.resolve(null);


    promise.then(reservations => { pendingReservationOfUser = reservations; return restaurantController.findInfo(restaurantId);})
    .then(_restaurant => {
        restaurant = _restaurant;
        return tagController.getAllTagsOf(restaurantId);
    }).then((tags) => {
        restaurant.tags = tags;
        return mapController.getMap(restaurantId);
    }).then((maps)=>{
        restaurant.maps = maps;
        let restaurantHomeResponse = new RestaurantHomePendingReservation(restaurant, pendingReservationOfUser);
        res.status(restaurantHomeResponse.httpStatus).json(restaurantHomeResponse.export());
    }).catch((error) => {
        console.log(error);
        let restaurantHomeResponse = new RestaurantHomeResponse(null, validationCode.restaurantNotFound);
        res.status(restaurantHomeResponse.httpStatus).json(restaurantHomeResponse.export());
    });

});

router.get('/:restaurant_id/hour', restaurant_finder_middleware, (req, res) => {
    let restaurantId;
    let date = req.query['date'];

    restaurantId = req.restaurant._id;
    openHourController.openHoursOfARestaurant(restaurantId, date).
    then((hours)=>{
        let response = new RestaurantHomeResponse(hours);
        res.status(response.httpStatus).json(response.export());
    }).
    catch((error)=>{
        console.log(error);
        let response = new RestaurantHomeResponse();
        res.status(response.httpStatus).json(response.export());
    });
});

router.post('/:restaurant_id/hour/set', restaurant_finder_middleware, (req, res) => {
    let restaurantId;

    restaurantId = req.restaurant._id;
    console.log(req.body);
    openHourController.addRestaurant(restaurantId, req.body).
    then((result)=>{
        res.json(result);
        console.log(result);
    }).
    catch(error => res.status(500).json(error));


});


module.exports = router;