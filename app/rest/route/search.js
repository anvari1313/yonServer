const express = require('express');
const router = express.Router();
const restaurantController = require('./../../controllers/restaurant');
const zoneController = require('./../../controllers/zone');
const tagController = require('./../../controllers/tag');
const SearchResponse = require('../responses/search');

router.get('/', (req, res)=>{
    let query = req.query['q'];

    let restaurants;
    let tags;
    let zones;

    let response;

    restaurantController.searchByNameOrBranch(query).
    then( result => { restaurants = result; return tagController.getAllTags(query); }).
    then( result => { tags = result; return zoneController.searchByName(query)}).
    then( result => {
        zones = result;
        console.log(zones);
        response = new SearchResponse(restaurants, tags, zones);
        res.status(response.httpStatus).json(response.export());
    }).catch( error => {
        console.log(error);
        response = new SearchResponse();
        res.status(response.httpStatus).json(response.export());
    });
});

module.exports = router;
