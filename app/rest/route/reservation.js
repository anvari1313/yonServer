const express = require('express');
const Promise = require('bluebird');
const validate = require('validate.js');
const moment = require('moment-jalaali');
const util = require('./../../utils/common');
const ReservationResponse = require('../responses/reservation');
const isAfterNow = require('./../../utils/common').isAfterNow;
const rules = require('../../validation-rules/reservation');
const validationFailCodes = require('../responses/codes');
const tableController = require('./../../controllers/table');
const reservationController = require('./../../controllers/reservation');
const restaurantController = require('./../../controllers/restaurant');
const mapController = require('./../../controllers/map');
const userController = require('./../../controllers/user');
const messageQ = require('../../socket/messageQ/messageQ');
const jwt_auth_middleware = require('../middlewares/passport-jwt-strategy').jwt_auth_middleware;
const CancelReservationResponse = require('./../responses/reservation/cancel-reservation');

let router = express.Router();

router.get('/', (req, res, next)=>{
    let date = req.query['date'];
    let datetime = req.query['datetime'];
    let restaurantId = req.restaurant._id;

    if (date){
        validate.async({date: date}, rules.getAllOfDay, {cleanAttributes: true}).
        then(()=>reservationController.reservationsOfADay(restaurantId, date)).
        then((result)=>{
            let response;
            response = new ReservationResponse(result);
            res.status(response.httpStatus).json(response.export());
        }).catch((error)=>{
            if (error instanceof Error)
                throw error;
            else{
                let response;
                response = new ReservationResponse(null, error);
                res.status(response.httpStatus).json(response.export());
            }
        }).catch((error)=>{
            let response;
            console.log(error);
            response = new ReservationResponse();
            res.status(response.httpStatus).json(response.export());
        });
    }else if(datetime){
        validate.async({datetime: datetime}, rules.getAllOfDayInATime, {cleanAttributes: true}).
        then((attr)=>reservationController.reservationsOfATime(restaurantId,attr.datetime)).
        then((result)=>{
            let response;
            response = new ReservationResponse(result);
            res.status(response.httpStatus).json(response.export());
        }).catch((error)=>{
            if (error instanceof Error)
                throw error;
            else{
                let response;
                response = new ReservationResponse(null, error);
                res.status(response.httpStatus).json(response.export());
            }
        }).catch((error)=>{
            let response;
            console.log(error);
            response = new ReservationResponse();
            res.status(response.httpStatus).json(response.export());
        });
    }else
        next();
});

router.post('/new', jwt_auth_middleware, (req, res)=>{
    let isTable = req.query['table'];

    if (isTable){

        let params;
        let map;
        validate.async(req.body, rules.newWithTableFromUser, {cleanAttributes: true}).
        then((attr)=> {
            params = attr;
            params.restaurant_id = req.restaurant._id;
            params.userId = req.user.id;
            return util.isAfterNow(attr.datetime);
        }).
        then(()=> restaurantController.isValid(params.restaurant_id)).
        then(()=> tableController.isValidTable(params.table_id)).
        then((result)=> tableController.getMap(params.table_id)).
        then((_map)=> {map = _map; return mapController.isMapBelongToRestaurant(params.restaurant_id, map); }).
        then(()=> reservationController.checkIfReservable(params)).
        then(()=> reservationController.new(params)).
        then((p)=> {params._id = p._id; return userController.getUserById(params.userId)}).
        then((user)=>{
            params.user = {
                id: user._id,
                fname: user.fname,
                lname: user.lname
            };

            delete params.userId;
            let response;
            response = new ReservationResponse(params);

            res.status(response.httpStatus).json(response.export());
            params.datetime = parseInt(params.datetime);
            params.guest_count = parseInt(params.guest_count);
            console.log(params);
            messageQ.sendRequest(map.restaurant_id, '/events/reservations/new', params);
        }).
        catch((error)=>{
            if (error instanceof Error)
                throw error;

            let response;
            if (error === 'Not a table') response = new ReservationResponse(null,validationFailCodes.tableNotFound);
            else if (error === 'Not a valid restaurant') response = new ReservationResponse(null,validationFailCodes.restaurantNotFound);
            else if (error === 'Table not belonged to restaurant.') response = new ReservationResponse(null, validationFailCodes.tableIsNotBelongedToRestaurant);
            else response = new ReservationResponse(null, error);

            res.status(response.httpStatus).json(response.export());
        }).catch((error)=>{
            let response = new ReservationResponse();
            console.log(error);
            res.status(response.httpStatus).json(response.export());
        });
    }else{
        let params;
        validate.async(req.body, rules.newWithoutTableFromUser, {cleanAttributes: true}).
        then((attr)=> {
            params = attr;
            params.restaurant_id = req.restaurant._id;
            params.userId = req.user.id;
            return util.isAfterNow(attr.datetime);
        }).
        then(()=> restaurantController.isValid(params.restaurant_id)).
        then(()=> reservationController.new(params)).
        then((p)=> {params._id = p._id; return userController.getUserById(params.userId)}).
        then((user)=>{
            params.user = {
                id: user._id,
                fname: user.fname,
                lname: user.lname
            };

            delete params.userId;
            let response;
            params.datetime = parseInt(params.datetime);
            params.guest_count = parseInt(params.guest_count);
            response = new ReservationResponse(params);

            res.status(response.httpStatus).json(response.export());
            messageQ.sendRequest(params.restaurant_id, '/events/reservations/new', params);
        }).
        catch((error)=>{
            if (error instanceof Error)
                throw error;

            let response;
            if (error === 'Not a valid restaurant') response = new ReservationResponse(null,validationFailCodes.restaurantNotFound);
            else response = new ReservationResponse(null, error);

            res.status(response.httpStatus).json(response.export());
        }).catch((error)=>{
            let response = new ReservationResponse();
            console.log(error);
            res.status(response.httpStatus).json(response.export());
        });
    }

});

router.delete('/:reservation_id', jwt_auth_middleware, (req, res)=>{
    let reservationId = parseInt(req.params['reservation_id']);
    let response;

    reservationController.cancelReservationByUser(reservationId, req.user.id).
    then(result => { response = new CancelReservationResponse(true); res.status(response.httpStatus).json(response.export()); }).
    catch(err => {
        switch (err.toString()){
            case 'reservation not owned to user':
                response = new CancelReservationResponse(false, {reservationNotOwnedToUser : true});
                break;
            case '':
                break;
            default:
                console.log(err);
                response = new CancelReservationResponse(false);
        }

        res.status(response.httpStatus).json(response.export());
    });
});
module.exports = router;