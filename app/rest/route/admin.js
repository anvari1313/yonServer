const express = require('express');
let router = express.Router();
const nodemailer = require('nodemailer');
var fs = require('fs');
const zoneController = require('./../../controllers/zone');
const emailController = require('./../../controllers/email');

router.post('/zone/add', (req, res)=>{
    zoneController.addZone({name: req.body.name, slug: req.body.slug, long: parseFloat(req.body.long), lat: parseFloat(req.body.lat)}).
    then(result => res.json(result));
});

router.post('/zone/connect', (req, res)=>{
    zoneController.connectZone(req.body.slug1, req.body.slug2).
    then(result =>res.json(result))
});

router.get('/t', (req, res)=>{

});

router.get('/new', (req, res, next)=>{
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'forfuntest69@gmail.com',
            pass: 'forfuntest'
        }
    });

    fs.readFile('/home/ahmad/WebstormProjects/yonServer/app/route/route/mail.htm', 'utf8', function(err, contents) {
        // fs.readFile('./data.txt', 'utf8', function(err, contents) {
        if (err)
            res.status(500).json(err);
        else{
            var mailOptions = {
                from: 'info@yonje.com',
                to: ['anvarimas@gmail.com'],
                subject: 'Sending Email using Node.js',
                html: contents
            };

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    res.status(500).json(error)
                } else {
                    res.json(info);
                }
            });
        }

    });
});

module.exports = router;
