const Router = require('express').Router;
const Promise = require('bluebird');
const db = require('../../database');
const mapController = require('../../controllers/map');
const tableController = require('../../controllers/table')
const _ = require('underscore');


let map0 = {
    width: 4.6,
    height: 2.5,
    name: "روباز",
    tables: [
        {
            name: "T1",
            shape_type: "square-4",
            capacity: 4,
            x: 0.5,
            y: 0.5,
            angle: 0,
        },
        {
            name: "T2",
            shape_type: "square-4",
            capacity: 4,
            x: 1.7,
            y: 0.5,
            angle: 0,
        },
        {
            name: "T3",
            shape_type: "square-4",
            capacity: 4,
            x: 2.9,
            y: 0.5,
            angle: 0,
        },
        {
            name: "T4",
            shape_type: "square-4",
            capacity: 4,
            x: 4.1,
            y: 0.5,
            angle: 0,
        },
        {
            name: "T5",
            shape_type: "square-4",
            capacity: 4,
            x: 0.5,
            y: 2,
            angle: 45,
        },
        {
            name: "T6",
            shape_type: "circle-8",
            capacity: 8,
            x: 3.9,
            y: 1.8,
            angle: 0,
        }
    ]
};

let map1 = {
    name: "هم‌کف",
    width: 4.9,
    height: 4.9,
    tables: [
        {
            capacity: 4,
            name: 'T1',
            x: 0.5,
            y: 0.5,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 4,
            name: 'T2',
            x: 0.5,
            y: 1.8,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 4,
            name: 'T3',
            x: 0.5,
            y: 3.1,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 4,
            name: 'T4',
            x: 0.5,
            y: 4.4,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 4,
            name: 'T5',
            x: 3.1,
            y: 0.5,
            angle: 0,
            shape_type: 'square-4'
        },
        {
            capacity: 4,
            name: 'T6',
            x: 1.8,
            y: 0.5,
            angle: 0,
            shape_type: 'square-4'
        },
        {
            capacity: 4,
            name: 'T7',
            x: 4.4,
            y: 0.5,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 6,
            name: 'T8',
            x: 2,
            y: 2.2,
            angle: 90,
            shape_type: 'square-6'
        },
        {
            capacity: 4,
            name: 'T9',
            x: 3.5,
            y: 2.2,
            angle: 45,
            shape_type: 'square-4'
        },
        {
            capacity: 4,
            name: 'T10',
            x: 3.5,
            y: 3.8,
            angle: 45,
            shape_type: 'square-4'
        },
        {
            capacity: 4,
            name: 'T11',
            x: 2,
            y: 3.8,
            angle: 0,
            shape_type: 'square-4'
        },
    ]
}


let map2 = {
    name: "حیاط",
    width: 5,
    height: 3.75,
    tables: [
        {
            capacity: 4,
            name: 'T1',
            x: 2.5,
            y: 0.5,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 6,
            name: 'T2',
            x: 2.5,
            y: 1.8,
            angle: 90,
            shape_type: 'square-6'
        },
        {
            capacity: 4,
            name: 'T3',
            x: 2.5,
            y: 3.2,
            angle: 0,
            shape_type: 'square-4'
        },
        {
            capacity: 6,
            name: 'T4',
            x: 1.2,
            y: 3.2,
            angle: 0,
            shape_type: 'circle-8'
        },
        {
            capacity: 6,
            name: 'T5',
            x: 3.8,
            y: 3.2,
            angle: 0,
            shape_type: 'circle-8'
        },
    ]
}

let map3 = {
    name: "طبقه‌ی اول",
    width: 8,
    height: 6,
    tables: [
        {
            capacity: 4,
            name: 'T1',
            x: 4,
            y: 0.5,
            angle: 0,
            shape_type: 'circle-4'
        },
        {
            capacity: 2,
            name: 'T2',
            x: 1.71,
            y: 2.05,
            angle: 90,
            shape_type: 'circle-2'
        },
        {
            capacity: 8,
            name: 'T3',
            x: 2.95,
            y: 2.05,
            angle: 0,
            shape_type: 'circle-8'
        },
        {
            capacity: 8,
            name: 'T4',
            x: 5.05,
            y: 2.06,
            angle: 0,
            shape_type: 'circle-8'
        },
        {
            capacity: 2,
            name: 'T5',
            x: 6.34,
            y: 2.05,
            angle: 90,
            shape_type: 'circle-2'
        },
        {
            capacity: 4,
            name: 'T6',
            x: 3.2,
            y: 3.6,
            angle: 0,
            shape_type: 'square-4'
        },
        {
            capacity: 4,
            name: 'T7',
            x: 4.8,
            y: 3.6,
            angle: 0,
            shape_type: 'square-4'
        },
        {
            capacity: 4,
            name: 'T8',
            x: 4,
            y: 4.9,
            angle: 0,
            shape_type: 'circle-4'
        }
    ]
};


let maps = [map0, map1, map2, map3];
// let maps = [map0];
let mapsHashMap = {};
for (let map of maps) {
    mapsHashMap[map.name] = map;
}
console.log(mapsHashMap);


let router = new Router();

function subset(arr) {
    return _.sample(arr, _.random(arr.length));
}
// function subset(arr) {
//     return [arr[0], arr[0]];
// }

function insertTables(restaurantId) {
    return new Promise((resolve, reject) => {
        mapController.getMap(restaurantId)
            .then(maps => {
                let promises = [];
                for (let map of maps) {
                    promises.push(tableController.addMany(restaurantId, map._id, mapsHashMap[map.name].tables))
                }

                return Promise.all(promises);
            })
            .then(resolve)
            .catch(reject);
    });
}

router.get('/an', (req, res, next) => {
    let ids = [];
    db.pg().many('select _id from restaurants where _id != 1')
        .then(_ids => ids = _ids)
        .then(x => db.mongoConnect())
        // .then(x => {
        //     console.log(ids);
        //     let promises = [];
        //     for (let r of ids) {
        //         console.log(subset(maps));
        //         promises.push(mapController.addManyMap(1, subset(maps)))
        //     }
        //
        //     return Promise.all(promises);
        // })
        .then(x => mapController.addOneMap(1, map0))
        .then(x => res.send("ok"))
        .catch(exp => {
            console.log(exp);
            res.send("fail");
        })
});

router.get('/antar', (req, res, next) => {
    let ids = [];
    db.pg().many('select _id from restaurants where _id != 1')
        .then(_ids => ids = _ids)
        .then(x => db.mongoConnect())
        .then(x => {
            // console.log(ids);
            // let promises = [];
            // for (let r of ids) {
            //     promises.push(insertTables(r._id))
            // }
            //
            // return Promise.all(promises);
            return insertTables(1)
        })
        .then(x => res.send("ok"))
        .catch(exp => {
            console.log(exp);
            res.send("fail");
        })
});


module.exports = router;