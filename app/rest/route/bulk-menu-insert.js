const Router = require('express').Router;
const Promise = require('bluebird');
const db = require('../../database');
const ObjectId = require('mongodb').ObjectId;
const menuSectionController = require('../../controllers/menu-section');
const mongo_find_promise = require('../../utils/mongo-find-promise');

let tags = [
    {
        _id: 1,
        name: "پیتزا",
        slug: "pizza",
        image: "pizza.jpg",
        description: ""
    },
    {
        _id: 2,
        name: "چینی",
        slug: "chinees",
        image: "chinees.jpg",
        description: ""
    },
    {
        _id: 3,
        name: "فست‌فود",
        slug: "fastfood",
        image: "ffastfood.jpg",
        description: ""
    },
    {
        _id: 4,
        name: "سنتی",
        slug: "iranian",
        image: "iranian.jpg",
        description: ""
    },
    {
        _id: 6,
        name: "دریایی",
        slug: "sea-food",
        image: "seafood.jpg",
        description: ""
    },
    {
        _id: 7,
        name: "سالاد بار",
        slug: "salad-bar",
        image: "saladb.jpg",
        description: ""
    },
    {
        _id: 9,
        name: "اتاق سیگار",
        slug: "smooking-room",
        image: "ssmoke.jpg",
        description: ""
    },
    {
        _id: 10,
        name: "نوشیدنی",
        slug: "drinks",
        image: "drinks.jpg",
        description: ""
    },
];

let menu1 = [
    {
        name: "غذای اصلی",
        show_order: 1,
        eatables: [
            {
                name: "کوبیده",
                price: 12000,
                tags: [tags[3]],
                picture_album: ["koubide_01.jpg"],
                ingredients: ["گوشت چرخ‌کرده ۸۰ درصد", "پیاز", "دنبه", "گوجه"],
                featured_picture: "koubide_01.jpg"
            },
            {
                name: "جوجه کباب",
                price: 13500,
                tags: [tags[3]],
                picture_album: ["chiken_kebab.jpg"],
                ingredients: ["مرغ تازه", "پیاز", "سس مخصوص", "گوجه"],
                featured_picture: "chiken_kebab.jpg"
            },
            {
                name: "برگ",
                price: 22000,
                tags: [tags[3]],
                picture_album: ["barg.jpg"],
                ingredients: ["گوشت گوسفندی تازه", "پیاز"],
                featured_picture: "barg.jpg"
            },
            {
                name: "بختیاری",
                price: 19800,
                tags: [tags[3]],
                picture_album: ["bakhtiari.jpg"],
                ingredients: ["مرغ", "۱۰۰ گرم گوشت گوساله", "پیاز", "فلفل"],
                featured_picture: "bakhtiari.jpg"
            },
            {
                name: "خورشت قیمه",
                price: 7000,
                tags: [tags[3]],
                picture_album: ["gheyme.jpg"],
                ingredients: ["گوشت گوساله", "لپه", "سیب‌زمینی", "رب خانگی"],
                featured_picture: "gheyme.jpg"
            },
            {
                name: "دیزی",
                price: 40000,
                tags: [tags[3]],
                picture_album: ["dizi.jpg"],
                ingredients: ["گوشت گوسفندی", "گوجه", "سیب‌زمینی", "نخود",],
                featured_picture: "dizi.jpg"
            },
        ]
    },
    {
        name: "پیش‌غذا",
        show_order: 2,
        eatables: [
            {
                name: "ماست موسیر",
                price: 2000,
                tags: tags[5],
                picture_album: ["yoghert.jpg"],
                ingredients: [],
                featured_picture: "yoghert.jpg"
            },
            {
                name: "سالاد فصل",
                price: 4850,
                tags: tags[5],
                picture_album: ["season_salad.jpg"],
                ingredients: ["کاهو", "گوجه", "خیار"],
                featured_picture: "season_salad.jpg"
            },
            {
                name: "زیتون پرورده",
                price: 2100,
                tags: tags[5],
                picture_album: ["olive.jpg"],
                ingredients: [],
                featured_picture: "olive.jpg"
            },
            {
                name: "ترشی",
                price: 3300,
                tags: tags[5],
                picture_album: ["torshi.jpg"],
                ingredients: [],
                featured_picture: "torshi.jpg"
            },
        ]
    },
    {
        name: "نوشیدنی",
        show_order: 3,
        eatables: [
            {
                name: "دوغ",
                price: 3300,
                tags: [tags[7]],
                picture_album: ["fluid_yoghert.jpg"],
                ingredients: [],
                featured_picture: "fluid_yoghert.jpg"
            },
            {
                name: "نوشابه",
                price: 3000,
                tags: [tags[7]],
                picture_album: ["coca.jpg"],
                ingredients: [],
                featured_picture: "coca.jpg"
            },
            {
                name: "دلستر",
                price: 2500,
                tags: [tags[7]],
                picture_album: ["delester.jpg"],
                ingredients: [],
                featured_picture: "delester.jpg"
            },
        ]
    }
]

let menu2 = [
    {
        name: "غذای اصلی",
        show_order: 1,
        eatables: [
            {
                name: "پیتزا مرغ",
                price: 23500,
                tags: [tags[2]],
                picture_album: ["chiken_pizza.jpg"],
                ingredients: ["فیله مرغ", "قارچ", "زیتون", "پیاز"],
                featured_picture: "chiken_pizza.jpg"
            },
            {
                name: "پیتزا گوشت و قارچ",
                price: 25000,
                tags: [tags[2]],
                picture_album: ["meat_pizza.jpg"],
                ingredients: ["گوشت چرخ‌کرده", "قارچ", "پیاز", "فلفل دلمه‌ای"],
                featured_picture: "meat_pizza.jpg"
            },
            {
                name: "همبرگر",
                price: 16000,
                tags: [tags[2]],
                picture_album: ["hamburger.jpg"],
                ingredients: ["۱۰۰ گرم گوشت", "پنیر موزارلا", "پیاز", "کاهو"],
                featured_picture: "hamburger.jpg"
            },
            {
                name: "چیز برگر",
                price: 17300,
                tags: [tags[2]],
                picture_album: ["cheeze_berger.jpg"],
                ingredients: ["۲۰۰ گرم گوشت", "پنیر دوبل", "قارچ", "گوجه"],
                featured_picture: "cheeze_berger.jpg"
            },
            {
                name: "هات‌داگ تنوری",
                price: 11600,
                tags: [tags[2]],
                picture_album: ["tanouri-hot-dog.jpg"],
                ingredients: ["هات داگ ۷۰ درصد", "پنیز", "قارچ"],
                featured_picture: "tanouri-hot-dog.jpg"
            },
            {
                name: "بندری",
                price: 9500,
                tags: [tags[2]],
                picture_album: ["bandari_sandwich.jpg"],
                ingredients: ["۲ عدد کوکتل", "پیاز", "فلفل", "سیب‌زمینی"],
                featured_picture: "bandari_sandwich.jpg"
            },
            {
                name: "جوجه چینی",
                price: 20500,
                tags: [tags[7], tags[1],],
                picture_album: ["chineese_chiken.jpg"],
                ingredients: ["مرغ با سوخاری مخصوص", "سس ایتالیایی", "پیاز"],
                featured_picture: "chineese_chiken.jpg"
            },
        ]
    },
    {
        name: "پیش غذا",
        show_order: 2,
        eatables: [
            {
                name: "سیب زمینی سرخ‌شده",
                price: 5500,
                tags: [tags[2]],
                picture_album: ["chips.jpg"],
                ingredients: ["۳۰۰ گرم سیب‌زمینی", "سس کچاپ"],
                featured_picture: "chips.jpg"
            },
            {
                name: "سیب‌زمینی ویژه",
                price: 11100,
                tags: [tags[2]],
                picture_album: ["super_sib.jpg"],
                ingredients: ["۳۰۰ گرم سیب‌زمینی", "پنیر", "زامبون تنوری"],
                featured_picture: "super_sib.jpg"
            },
            {
                name: "سالاد سزار",
                price: 10500,
                tags: [tags[2]],
                picture_album: ["sezar_salad.jpg"],
                ingredients: ["مرغ تکه شده", "کاهو", "سیر", "سس سزار"],
                featured_picture: "sezar_salad.jpg"
            },
            {
                name: "قارچ سوخاری",
                price: 7500,
                tags: [tags[2]],
                picture_album: ["fried_mashrooms.jpg"],
                ingredients: ["۱۰۰ گرم قارچ", "پودر مخصوص", "سس کچاپ"],
                featured_picture: "fried_mashrooms.jpg"
            },
            {
                name: "نان سیر",
                price: 9000,
                tags: [tags[2]],
                picture_album: ["garlic_bread.jpg"],
                ingredients: ["نان تست سوخاری", "سیر", "پنیر"],
                featured_picture: "garlic_bread.jpg"
            },
        ]
    },
    {
        name: "نوشیدنی",
        show_order: 3,
        eatables: [
            {
                name: "دوغ",
                price: 3300,
                tags: [tags[7]],
                picture_album: ["fluid_yoghert.jpg"],
                ingredients: [],
                featured_picture: "fluid_yoghert.jpg"
            },
            {
                name: "نوشابه",
                price: 3000,
                tags: [tags[7]],
                picture_album: ["coca.jpg"],
                ingredients: [],
                featured_picture: "coca.jpg"
            },
            {
                name: "دلستر",
                price: 2500,
                tags: [tags[7]],
                picture_album: ["delester.jpg"],
                ingredients: [],
                featured_picture: "delester.jpg"
            },
        ]
    },
];

function an() {
    db.mongodb().collection('tables').insertMany(tableArr).then((res) => {
        return Map.addTableToMap(mapId, res.ops);
    });
}

function addTableToMap(mapId, table) {
    mongo_find_promise.find(db.mongodb().collection('maps'), {_id: new ObjectId(mapId)}).then((map) => {
        let tables = map[0].tables;
        if (tables === 'undefined' || !Array.isArray(tables)) {
            let tmeptables = [];

            tmeptables.push(table);
            map.tables = tmeptables;
            return db.mongodb().collection('maps').updateOne({_id: new ObjectId(mapId)}, {$push: {tables: tmeptables}})
        } else {
            if (!Array.isArray((table))) table = [table];
            return db.mongodb().collection('maps').updateOne({_id: new ObjectId(mapId)}, {$push: {tables: {$each: table}}})
        }

    });
}

function getEatables(menu, menuSection) {
    for (let ms of menu)
        if (ms.name === menuSection.name)
            return ms.eatables;

    console.log(menuSection);
    console.log(menu);

}

function addEeatablesToMenuSection(menuSectionId, eatables, restaurantId) {
    return new Promise((resolve, reject) => {
        for (let e of eatables)
            e.restaurant_id = restaurantId;

        db.mongodb()
            .collection('eatables')
            .insertMany(eatables)
            .then(res => {
                console.log(res.ops);
                return db.mongodb()
                    .collection('menu_sections')
                    .updateOne({_id: new ObjectId(menuSectionId)}, {$push: {eatables: res.ops}})
            })
            .then(resolve)
            .catch(reject);
    })
}

function addEeatables(restaurantId) {
    return new Promise((resolve, reject) => {
        let menu = [menu1, menu2][Math.floor(Math.random() * 2)];
        menuSectionController.getMenuSectionsOfRestaurant(restaurantId)
            .then(sections => {
                let promises = [];
                for (let s of sections)
                    promises.push(addEeatablesToMenuSection(s._id, getEatables(menu, s), restaurantId))

                return Promise.all(promises);
            })
            .then(resolve)
            .catch(reject)
    });
}

let router = new Router();

router.get("/an", (req, res, next) => {
    let ids = [];
    db.pg().many('select _id from restaurants where _id != 1 and _id != 100')
        .then(_ids => ids = _ids)
        .then(x => db.mongoConnect())
        .then(x => {
            let promises = [];
            for (let r of ids) {
                promises.push(menuSectionController.add(r._id, menu1[0]));
                promises.push(menuSectionController.add(r._id, menu1[1]));
                promises.push(menuSectionController.add(r._id, menu1[2]));
            }

            return Promise.all(promises);
        })
        .then(x => res.send("ok"))
        .catch(exp => {
            console.log(exp);
            res.send("fail");
        })
});


router.get("/antar", (req, res, next) => {
    let ids = [];
    db.pg().many('select _id from restaurants where _id != 1 and _id != 100')
        .then(_ids => ids = _ids)
        .then(x => db.mongoConnect())
        .then(x => {
            let promises = [];
            for (let r of ids)
                promises.push(addEeatables(r._id))

            return Promise.all(promises)
        })
        .then(x => res.send("ok"))
        .catch(x => {
            console.log(x);
            res.send('fail');
        })
});

module.exports = router;