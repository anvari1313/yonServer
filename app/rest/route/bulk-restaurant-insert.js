const Router = require('express').Router;
const Promise = require('bluebird');
const db = require('../../database');
const Restaurant = require('../../models/restaurant');
const pgp = require('pg-promise')({promiseLib: Promise, capSQL: true});

let router = new Router();

let info = {
    phone: "۰۲۱ ۲۲۳۳ ۴۴۴۳",
    price_range: "از ۴۰۰۰ تومان تا ۱۰۰،۰۰۰ تومان",
    parking_space: "دارد",
    opening_hour: "صبح ها: از ساعت ۸ تا ۱۱" + "\n" + "ظهر‌ها: از ساعت ۱۲ تا ۱۶" + "\n" + "شب‌ها: از ساعت ۱۸ تا ۲۲",
    desc: "این رستورات رستوران بسیار با کیفیت و خوبی است و از آشپزان بسیار حازق و با کلاس از سراسر دنیا استفاده می‌کند. و این متن صرفا برای خوش‌نمک است.",
};

let tags = [
    {
        _id: 1,
        name: "پیتزا",
        slug: "pizza",
        image: "pizza.jpg",
        description: ""
    },
    {
        _id: 2,
        name: "چینی",
        slug: "chinees",
        image: "chinees.jpg",
        description: ""
    },
    {
        _id: 3,
        name: "فست‌فود",
        slug: "fastfood",
        image: "ffastfood.jpg",
        description: ""
    },
    {
        _id: 4,
        name: "سنتی",
        slug: "iranian",
        image: "iranian.jpg",
        description: ""
    },
    {
        _id: 6,
        name: "دریایی",
        slug: "sea-food",
        image: "seafood.jpg",
        description: ""
    },
    {
        _id: 7,
        name: "سالاد بار",
        slug: "salad-bar",
        image: "saladb.jpg",
        description: ""
    },
    {
        _id: 9,
        name: "اتاق سیگار",
        slug: "smooking-room",
        image: "ssmoke.jpg",
        description: ""
    },
];

let restaurants = [
    new Restaurant({
        name: "محفل",
        address: "مبدان فاطمی، خیابان فلسطین شمالی",
        location: {
            x: 51.420872,
            y: 35.706837,
        },
        point: 0,
        cash: 0,
        email: "tskkesst5s@test.com",
        password: "$2a$10$Nbw.SrjEjRZ4AFO6BMqMGuTUECwaZ2vTBR3YF/1H1Ka/pKwMP4u4i",
        info: info,
        price_rate: 1,
        tags: [tags[6], tags[0], tags[1]],
        avatar: "855.jpg",
        max_table: 60,
        rating: 4.2,
        zone_slug: "Taleghani",
        zone_name: "طالقانی",
    }),
    new Restaurant({
        name: "دوگوش",
        address: "خیابان فلسطین شمالی، پلاک 483",
        location: {
            x: 51.420875,
            y: 35.706837,
        },
        point: 0,
        cash: 0,
        email: "tskkesst9s@test.com",
        password: "$2a$10$Nbw.SrjEjRZ4AFO6BMqMGuTUECwaZ2vTBR3YF/1H1Ka/pKwMP4u4i",
        info: info,
        price_rate: 1,
        tags: [tags[3], tags[4], tags[1]],
        avatar: "1590.jpg",
        max_table: 60,
        rating: 4.2,
        zone_slug: "Taleghani",
        zone_name: "طالقانی",
    }),
    new Restaurant({
        name: "برگر زغالی هیچ",
        address: "خیابان ولیعصر، بالاتر از چهارراه زرتشت",
        location: {
            x: 51.420873,
            y: 35.706837,
        },
        point: 0,
        cash: 0,
        email: "tskkesst7s@test.com",
        password: "$2a$10$Nbw.SrjEjRZ4AFO6BMqMGuTUECwaZ2vTBR3YF/1H1Ka/pKwMP4u4i",
        info: info,
        price_rate: 1,
        tags: [tags[6], tags[4], tags[5]],
        avatar: "129.jpg",
        max_table: 60,
        rating: 4.2,
        zone_slug: "Taleghani",
        zone_name: "طالقانی",
    }),
];

let tagHashMap = {};
for (let tag of tags) {
    tagHashMap[tag.slug] = tag;
}
console.log(tagHashMap);

// let query =
//     `insert into restaurants (name, address, location, point, cash, email,password, info, price, cached_tags, avatar, max_table, rating, zone_slug, zone_name)
//     values ($1, $2, $3, $4, $5, $6, $7, $8, $9 , $10 , $11, $12, $13 , $14 , $15)`;


const tagCs = new pgp.helpers.ColumnSet(['tag_id', 'restaurant_id'], {table: 'tags_exists'});

function insert(rests) {
    return new Promise((resolve, reject) => {
        const cs = new pgp.helpers.ColumnSet(["name", "address", "location", "point", "cash", "email", 'password', "info", "price", "cached_tags", "avatar", "max_table", 'rating', 'zone_slug', 'zone_name'], {table: 'restaurants'});
        const tagCs = new pgp.helpers.ColumnSet(['tag_id', 'restaurant_id'], {table: 'tags_exists'});
        let values = [];
        for (let r of rests) {
            values.push({
                name: r.name,
                address: r.address,
                location: "(" + r.location.x + "," + r.location.y + ")",
                point: r.point,
                cash: r.cash,
                email: r.email,
                password: r._password,
                info: JSON.stringify(r.info),
                price: r.price_rate,
                cached_tags: JSON.stringify(r.tags),
                avatar: r.avatar,
                max_table: r.max_table,
                rating: r.rating,
                zone_slug: r.zone_slug,
                zone_name: r.zone_name,
            });
        }

        const query = pgp.helpers.insert(values, cs) + ' RETURNING _id';
        // console.log(query);

        // db.pg().one(query, [
        //     r.name,
        //     r.address,
        //     "(" + r.location.x + "," + r.location.y + ")",
        //     r.point,
        //     r.cash,
        //     r.email,
        //     r._password,
        //     JSON.stringify(r.info),
        //     r.price,
        //     JSON.stringify(r.tags),
        //     r.avatar,
        //     r.max_table,
        //     r.rating,
        //     r.zone_slug,
        //     r.zone_name
        // ]).then(resolve)
        //     .catch(e => {
        //         console.log(e)
        //     })

        db.pg().many(query)
            .then(data => {
                console.log(data);
                let values = [];
                for (let i = 0; i < rests.length; i++) {
                    let r = rests[i];
                    for (let tagg of r.tags)
                        values.push({
                            tag_id: tagg._id,
                            restaurant_id: data[i]._id
                        });
                }

                let finalQ = pgp.helpers.insert(values, tagCs);
                return db.pg().none(finalQ)
            })
            .then(x => {
                resolve();
            })
            .catch(error => {
                reject(error);
            });
    });
}

router.get('/an', (req, res, next) => {
    // let inserts = [];
    // for (let r of restaurants) {
    //     inserts.push(insert(r))
    // }
    //
    // Promise.all(inserts)
    //     .then(x => res.send("ok"))
    //     .catch(exp => {
    //         console.log(exp);
    //     })

    insert(restaurants)
        .then(x => {
            res.send("ok");
        })
        .catch(exp => {
            console.log(exp);
            res.send("fail");
        })
});

router.get('/antar', (req, res, next) => {
    db.pg().many('select * from restaurants where zone_slug = \'ValiasrCrossroads\'')
        .then(data => {
            let values = [];
            for (let i = 0; i < data.length; i++) {
                let r = data[i];
                for (let tagg of r.cached_tags) {
                    console.log(tagg);
                    values.push({
                        tag_id: tagg._id ? tagg._id : tagHashMap[tagg.slug]._id,
                        restaurant_id: r._id
                    });
                }
            }

            let finalQ = pgp.helpers.insert(values, tagCs);
            return db.pg().none(finalQ)
        })
        .then(x => res.send("ok"))
        .catch(exp => {
            console.log(exp);
            res.send('fail');
        })
});

module.exports = router;