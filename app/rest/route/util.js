const express = require('express');
const router = express.Router();
const moment = require('moment');

/* GET home page. */
router.get('/timestamp', function(req, res) {
    res.json({now:moment().unix()});
});

module.exports = router;
