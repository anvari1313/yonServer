const userController = require('../../controllers/user');
const UserAuthResponse = require('../responses/user-auth');

module.exports = (req, res, next)=>{
    let authorizationHeader = req.header('Authorization');

    let token = null;
    if(authorizationHeader) {
        token = authorizationHeader.split(' ')[1];
        if (token){
            userController.decodeToken(token).then((decoded) => {
                req.user = decoded;
                next();
            }).catch((error) => {
                let response = new UserAuthResponse(false,null,  null, true);
                res.status(response.httpStatus).json(response.export());
            });
        }else {
            let response = new UserAuthResponse(false,null,  null, true);
            res.status(response.httpStatus).json(response.export());
        }

    }else{
        next();
    }
};