const restaurantController = require('../../controllers/restaurant');
const RestaurantNotFoundResponse = require('../responses/retaurant-not-found');

/**
 * This middleware is for those routes that has restaurant-id in their path
 * First it checks if the restaurant with that id is existed and then add the restaurant object
 * to the req and then call next
 * @param req
 * @param res
 * @param next
 */
module.exports.findRestaurant = (req, res, next)=>{
    let restaurantId = req.params.restaurant_id;
    restaurantController.find(restaurantId).
    then((_restaurant)=>{req.restaurant = _restaurant; next();}).
    catch((error)=>{
        console.log(error);
        let response = new RestaurantNotFoundResponse();
        res.status(response.httpStatus).json(response.export());
    });
};