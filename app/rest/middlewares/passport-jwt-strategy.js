var jwt = require('jsonwebtoken');

const passport = require("passport");
const passportJWT = require("passport-jwt");
const jwt_secrete = require('../../../config/secrets').jwt;

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();
jwtOptions.secretOrKey = jwt_secrete;

let strategy = new JwtStrategy(jwtOptions, (jwt_payload, next) => {
    // usually this would be a database call:
    let user = jwt_payload;
    if (user) {
        next(null, user);
    } else {
        next(null, false);
    }
});

passport.use(strategy);


module.exports.passport = passport;
module.exports.jwt_auth_middleware = passport.authenticate('jwt', { session: false });