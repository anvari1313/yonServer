const Promise = require('bluebird');
const fcmAuthorizationKey = require('./../../../config/secrets').fcm_authorization;
const request = require('request-promise');
const FCMDeviceController = require('./../../controllers/fcm-device');

class UserRequest{
    constructor(){}

    get RESERVATION_CANCEL_MESSAGE(){ return '/events/reservations/cancel';}

    sendCancelReservationToUser(userId, reservation){
        return FCMDeviceController.fcmTokensOfUser(userId).
        then(fcmTokens => Promise.each(fcmTokens.fcm_token_ids, fcmToken => this._sendMessage(fcmToken, this.RESERVATION_CANCEL_MESSAGE, reservation)));
    }

    _sendMessage(fcmTokenId, messageType, message){
        return new Promise((resolve, reject) => {
            let dataObj = message;
            dataObj.event = messageType;
            // console.log(dataObj);

            let options = {
                method: 'POST',
                uri: 'https://fcm.googleapis.com/fcm/send',
                headers:{
                    'Content-Type':'application/json',
                    'Authorization':'key=' + fcmAuthorizationKey
                },
                body: {
                    "to":fcmTokenId,
                    "data": dataObj
                },
                json: true // Automatically stringifies the body to JSON
            };

            // console.log(options);
            request(options)
                .then(parsedBody => {
                    // POST succeeded...
                    resolve(parsedBody);
                })
                .catch(err => {
                    // POST failed...
                    console.log('Error is here');
                    console.log(err);
                    reject(err);
                });
        });
    }
}

module.exports = new UserRequest();