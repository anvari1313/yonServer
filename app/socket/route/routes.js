const YonRouter = require('../yonrouter');
const restaurantController = require('../../controllers/restaurant');
const mapController = require('../../controllers/map');
const tableController = require('../../controllers/table');
const reservationController = require('../../controllers/reservation');
const orderController = require('../../controllers/order');
const openHourController = require('./../../controllers/open-hour');
const messageQ = require('../messageQ/messageQ');
const validate = require('validate.js');
const table_map_rules = require('../../validation-rules/table_map');
const order_rules = require('../../validation-rules/order');
const reservation_rules = require('../../validation-rules/reservation');

const statusCodesOK = require('../../rest/responses/codes').okStatus.code;
const statusCodesNotAcceptable = require('../../rest/responses/codes').notAcceptable.code;
const statusCodesNotFound = require('../../rest/responses/codes').notFound.code;
const validationFailCodes = require('../../rest/responses/codes').validationFailCodes;
const errorObjects = require('../../rest/responses/codes');

const Promise = require('bluebird');
const Map = require('../../models/map');
const menuSectionController = require('../../controllers/menu-section');
const responseRes = require('./../response/restaurant-response');
yonRouter = new YonRouter();


const SocketRouter = require('./../../../lib/sockrouter');
const routes = new SocketRouter();

const reservationRouter = require('./reservation');
const mapRouter = require('./map');
const tableRouter = require('./table');
const menuRouter = require('./menu');
const orderRouter = require('./order');
const hourRouter = require('./hour');
const toolsRouter = require('./tool');

routes.on('/maps', mapRouter);
routes.on('/tables', tableRouter);
routes.on('/reservations', reservationRouter);
routes.on('/menu', menuRouter);
routes.on('/order', orderRouter);
routes.on('/hours', hourRouter);
routes.on('/tools', toolsRouter);

routes.onNotFound(()=>{
    console.log('not found');
});

// yonRouter.on('/maps/add/one', (restaurantId, request_id, payload) => {
//     validate.async(payload, table_map_rules.addMap, {cleanAttributes: true})
//         .then((attr) => mapController.addOneMap(restaurantId, attr))
//         .then((attr) => messageQ.sendResponse(restaurantId, attr.ops[0], request_id, statusCodesOK))
//         .catch((error) => {
//             if (error instanceof Error)
//                 throw new Error();
//             else
//                 messageQ.sendResponse(restaurantId, error, request_id, statusCodesNotAcceptable);
//         });
// });
// yonRouter.on('/maps/add/many', (restaurantId, request_id, payload) => {
//     if (Array.isArray(payload)) {
//         let errors = [];
//         Promise.each(payload, (payloadItem, index) => {
//             let error = validate.validate(payloadItem, table_map_rules.addMap);
//             if (error) errors.push(error);
//         }).then(() => {
//             if (errors.length === 0)
//                 return mapController.addManyMap(restaurantId, payload);
//             else
//                 throw errors;
//         }).then((attr) => messageQ.sendResponse(restaurantId, attr.ops, request_id, statusCodesOK)).catch((error) => {
//             if (error instanceof Error)
//                 throw new Error();
//             else
//                 messageQ.sendResponse(restaurantId, errors, request_id, statusCodesNotAcceptable);
//         });
//     } else
//         messageQ.sendResponse(restaurantId, validationFailCodes.notArrayType, request_id, statusCodesNotAcceptable);
// });
// yonRouter.on('/maps/get', (restaurantId, request_id, payload) => {
//
//     let genPromise;
//     if (payload.map_id)
//         genPromise = mapController.isValidMap(payload.map_id);
//     else
//         genPromise = new Promise((resolve) => resolve());
//     genPromise.then(() => mapController.getMap(restaurantId, payload.map_id)).
//     then((res) => messageQ.sendResponse(restaurantId, res, request_id, statusCodesOK)).
//     catch((error) => {
//         if (error === 'Not a map')
//             messageQ.sendResponse(restaurantId, errorObjects.mapNotFound, request_id, statusCodesNotFound);
//         else {
//             console.log(error);
//             throw new Error();
//         }
//     });
// });

// yonRouter.on('/tables/add/one', (restaurantId, request_id, payload) => {
//     mapController.isValidMap(payload.map_id).
//     then(() => validate.async(payload, table_map_rules.addTable, {cleanAttributes: true})).
//     then((attr) => tableController.addOne(restaurantId, attr.map_id, attr)).
//     then((res) => messageQ.sendResponse(restaurantId, res, request_id, statusCodesOK)).
//     catch((error) => {
//         if (error instanceof Error)
//             throw error;
//         else if (error === 'Not a map')
//             messageQ.sendResponse(restaurantId, errorObjects.mapNotFound, request_id, statusCodesNotFound);
//         else
//             messageQ.sendResponse(restaurantId, error, request_id, statusCodesNotAcceptable);
//     });
// });

// yonRouter.on('/tables/add/many', (restaurantId, request_id, payload) => {
//     let errors = [];
//     mapController.isValidMap(payload.map_id).
//     then(()=>{
//         if (Array.isArray(payload.tables))
//             Promise.each(payload.tables, (tableItem, index) => {
//                 let error = validate.validate(tableItem, table_map_rules.addManyTable);
//                 if (error) errors.push(error);
//             });
//         else
//             messageQ.sendResponse(restaurantId, validationFailCodes.notArrayType, request_id, statusCodesNotAcceptable);
//     }).
//     then(()=>{
//         if (errors.length === 0)
//             return tableController.addMany(restaurantId, payload.map_id, payload.tables);
//         else
//             throw errors;
//     }).
//     then((attr) => messageQ.sendResponse(restaurantId, {tables: attr}, request_id, statusCodesOK)).
//     catch((error) => {
//         if (error instanceof Error)
//             throw error;
//         else if (error === 'Not a map')
//             messageQ.sendResponse(restaurantId, errorObjects.mapNotFound, request_id, statusCodesNotFound);
//         else
//             messageQ.sendResponse(restaurantId, error, request_id, error.code);
//     });
// });
// yonRouter.on('/tables/get', (restaurantId, request_id, payload) => {
//     let table_id;
//     validate.async(payload, table_map_rules.getTable, {cleanAttributes: true}).
//     then(attr => {table_id = attr.table_id; return tableController.isValidTable(table_id);}).
//     then(() => tableController.getTable(table_id)).
//     then((res) => messageQ.sendResponse(restaurantId, {table: res[0]}, request_id, statusCodesOK)).
//     catch((error) => {
//         if (error instanceof Error)
//             throw error;
//         else if (error === 'Not a table')
//             messageQ.sendResponse(restaurantId, errorObjects.tableNotFound, request_id, statusCodesNotFound);
//         else
//             messageQ.sendResponse(restaurantId, error, request_id, error.code);
//     });
// });
// yonRouter.on('/tables/edit', (restaurantId, request_id, payload) => {});
// yonRouter.on('/tables/delete', (restaurantId, request_id, payload) => {});

// yonRouter.on('/reservations/get', (restaurantId, request_id, payload) => {
//     reservationController.getAllReservations(restaurantId, {
//         start: payload.start,
//         end: payload.end
//     }).then((result) => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).catch((error) => {
//         if (error.code)
//             messageQ.sendResponse(restaurantId, error, request_id, error.code);
//         else {
//             console.log(error);
//             throw new Error();
//         }
//     });
// });

// yonRouter.on('/reservations/cancel', (restaurantId, request_id, payload) => {
//     validate.async(payload, reservation_rules.cancel, {cleanAttributes: true}).
//     then(attr => reservationController.cancelReservationByRestaurant(attr.reservation_id, restaurantId)).
//     then(result => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).
//     catch(error => {
//         if (error.code)
//             messageQ.sendResponse(restaurantId, error, request_id, error.code);
//         else if (error === 'reservation not owned to restaurant')
//             messageQ.sendResponse(restaurantId, {code: 12000, message: 'Reservation not owned to restaurant'}, request_id, 403);
//         else {
//             console.log(error);
//             throw new Error();
//         }
//     });
// });

// yonRouter.on('/reservations/add', (restaurantId, request_id, payload) => {
//     payload.restaurant_id = restaurantId;
//
//     validate.async(payload, reservation_rules.newFromRestaurant, {cleanAttributes: true}).
//     then(attr => reservationController.newWithTempUser(attr, {name: attr.user_name, cell_phone: attr.user_cell_phone})).
//     then(result => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).
//     catch(error => {
//         // if (error.code)
//         //     messageQ.sendResponse(restaurantId, error, request_id, error.code);
//         // else if (error === 'reservation not owned to restaurant')
//         //     messageQ.sendResponse(restaurantId, {code: 12000, message: 'Reservation not owned to restaurant'}, request_id, 403);
//         // else {
//         //     console.log(error);
//         //     throw new Error();
//         // }
//
//         messageQ.sendError(restaurantId, error, request_id, 500);
//     });
//
// });

// yonRouter.on('/reservations/assign/order', (restaurantId, request_id, payload) => {
//
// });

// yonRouter.on('/reservations/assign/table', (restaurantId, request_id, payload) => {
//     reservationController.assignTable(payload.reservation_id, payload.table_id).
//     then(result => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).
//     catch(error => messageQ.sendError(restaurantId, error, request_id, statusCodesNotAcceptable));
// });

// yonRouter.on('/orders/add', (restaurantId, request_id, payload) => {
//     const valid = require('../../validation-rules/order').validate;
//     validate.async(payload, order_rules.new, {cleanAttributes: true}).
//     then( attr => orderController.add(restaurantId, attr)).
//     then( result => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).
//     catch(error => {
//         if (error instanceof Error)
//             throw error;
//         else
//             messageQ.sendResponse(restaurantId, error, request_id, statusCodesNotAcceptable);
//     });
// });

// yonRouter.on('/orders/get', (restaurantId, request_id, payload) => {
//     orderController.listBetween(payload.start, payload.end).
//     then((result)=> messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK));
// });

// yonRouter.on('/orders/eatables/get', (restaurantId, request_id, payload) => {
//
// });
//
// yonRouter.on('/orders/eatables/add', (restaurantId, request_id, payload) => {
//
// });

// yonRouter.on('/orders/edit', (restaurantId, request_id, payload) => {
//     orderController.edit(payload.order_id, null)
// });

// yonRouter.on('/menu/all', (restaurantId, request_id, payload) => {
//     menuSectionController.getMenuSectionsOfRestaurant(restaurantId).
//     then((result)=>{
//         messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)
//     })
// });

// yonRouter.on('/menu/sections/add', (restaurantId, request_id, payload) => {
//     menuSectionController.add(restaurantId, payload).
//     then(result => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).
//     catch(error => console.log(error))
// });

// yonRouter.on('/menu/sections/eatables', (restaurantId, request_id, payload) =>  {
//
// });

// yonRouter.on('/menu/sections/eatables/add', (restaurantId, request_id, payload) => {
//     // validate.async(payload, table_map_rules.getTable, {cleanAttributes: true}).
//     // then(attr => console.log(attr));
//     menuSectionController.addEatableToSection(restaurantId, payload.menu_section_id, payload.eatables).
//     then(res => { messageQ.sendResponse(restaurantId, res.ops, request_id, statusCodesOK) })
// });


// yonRouter.on('/tools/connection/ping', (restaurantId, request_id, payload) => {
//     responseRes.pong(restaurantId);
// });

// yonRouter.on('/hours/get', (restaurantId, request_id, payload) => {
//     let date = payload.date;
//     openHourController.openHoursOfARestaurant(restaurantId, date).
//     then(result => messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK)).
//     catch(error => messageQ.sendError(restaurantId, error, request_id, statusCodesNotAcceptable))
// });

// yonRouter.on('/tt', (restaurantId, request_id, payload) => {
//     // const http = require("https");
//     // let options = {
//     //     hostname: 'fcm.googleapis.com',
//     //     path: '/fcm/send',
//     //     method: 'POST',
//     //     headers: {
//     //         'Content-Type' : 'application/json',
//     //         'Authorization' :'key=AAAAABUZDCs:APA91bHrc1ASiJnmNOlr8Hlzdj06SwJBrK62SWiAFzjpzzXaDPWIbxvLhBdYZuOyRN0_iu2JaxhSCrFrNfJnMSu3vCEUsTlj_RWPM3pQlx9S7x629G80UIruKZjmJFl7WfwkphYbai74'
//     //     }
//     // };
//     //
//     // let req = http.request(options, function(res) {
//     //     messageQ.sendResponse(restaurantId, res, request_id, res.statusCode)
//     //     console.log('Status: ' + res.statusCode);
//     //     console.log('Headers: ' + JSON.stringify(res.headers));
//     //     res.setEncoding('utf8');
//     //     res.on('data', function (body) {
//     //         console.log('body');
//     //         console.log('Body: ' + body);
//     //         console.log('-body-')
//     //     });
//     // });
//     // req.on('error', function(e) {
//     //     messageQ.sendResponse(restaurantId, e, request_id, e.statusCode);
//     //     console.log('problem with request: ' + e.message);
//     // });
//     // // write data to request body
//     // req.write('{"string": "Hello, World"}');
//     // req.end();
// });

// module.exports = yonRouter;
module.exports = routes;