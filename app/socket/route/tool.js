const SocketRouter = require('./../../../lib/sockrouter');
const responseRes = require('./../response/restaurant-response');

let router = new SocketRouter();


router.on('/connection/ping', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurantId;
    responseRes.pong(restaurantId);
});

module.exports = router;