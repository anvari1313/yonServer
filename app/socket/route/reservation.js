const SocketRouter = require('./../../../lib/sockrouter');
const statusCode = require('http-status-codes');
const validate = require('validate.js');
const reservationValidationRules = require('./../../validation-rules/reservation');
const messageQ = require('../messageQ/messageQ');
const reservation_rules = require('../../validation-rules/reservation');
const reservationController = require('../../controllers/reservation');

let router = new SocketRouter();

router.on('/add', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;
    payload.restaurant_id = authorizationObj.restaurant_id;

    validate.async(payload, reservation_rules.newFromRestaurant, {cleanAttributes: true}).
    then(attr => reservationController.newWithTempUser(attr, {name: attr.user_name, cell_phone: attr.user_cell_phone})).
    then(result => messageQ.sendResponse(authorizationObj.restaurant_id, result, requestId, statusCode.OK)).
    catch(error => messageQ.sendError(restaurantId, error, requestId, statusCode.INTERNAL_SERVER_ERROR));
});

router.on('/cancel', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;


    validate.async(payload, reservation_rules.cancel, {cleanAttributes: true}).
    then(attr => {
        console.log(attr.reservation_id);
        console.log(restaurantId);
        return reservationController.cancelReservationByRestaurant(attr.reservation_id, restaurantId)
    }).
    then(result => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
    catch(error => {
        if (error.code)
            messageQ.sendResponse(restaurantId, error, requestId, error.code);
        else if (error === 'reservation not owned to restaurant')
            messageQ.sendResponse(restaurantId, {code: 12000, message: 'Reservation not owned to restaurant'}, requestId, 403);
        else {
            console.log(error);
            throw new Error();
        }
    });
});

router.on('/get', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    reservationController.getAllReservations(restaurantId, {
        start: payload.start,
        end: payload.end
    }).
    then((result) => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
    catch((error) => {
        if (error.code)
            messageQ.sendResponse(restaurantId, error, requestId, error.code);
        else {
            console.log(error);
            throw new Error();
        }
    });
});

router.on('/filter/get', (authorizationObj, requestId, payload) => {
    let date = payload['date'];
    let datetime = payload['datetime'];
    let restaurantId = authorizationObj.restaurant_id;
    console.log('This is runned');

    if (date){
        console.log('This is runned1');
        validate.async({date: date}, reservationValidationRules.getAllOfDay, {cleanAttributes: true}).
        then(() => reservationController.reservationsOfADay(restaurantId, date)).
        then(result => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
        catch((error)=>{
            if (error instanceof Error)
                throw error;
            else{
                messageQ.sendError(restaurantId, error,requestId, statusCode.NOT_ACCEPTABLE)
            }
        }).catch(error => {
            messageQ.sendResponse(restaurantId, {}, requestId, statusCode.OK)
        });
    }else if(datetime){
        console.log('This is runned2');
        validate.async({datetime: datetime}, reservationValidationRules.getAllOfDayInATime, {cleanAttributes: true}).
        then((attr)=>reservationController.reservationsOfATime(restaurantId,attr.datetime)).
        then(result => {
            console.log('Log for what is sent from server:');
            console.log(result);
            console.log('Log for what is sent from server end');
            messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK);
        }).
        catch((error)=>{
            console.log(error);
            if (error instanceof Error)
                throw error;
            else{
                messageQ.sendError(restaurantId, error,requestId, statusCode.NOT_ACCEPTABLE)
            }
        }).catch((error)=>{
            messageQ.sendResponse(restaurantId, {}, requestId, statusCode.OK)
        });
    }else
        messageQ.sendError(restaurantId, error, requestId, statusCode.NOT_ACCEPTABLE)
});

router.on('/assign/table', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    reservationController.assignTable(payload.reservation_id, payload.table_id).
    then(result => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
    catch(error => messageQ.sendError(restaurantId, error, requestId, statusCode.NOT_ACCEPTABLE));
});

router.on('/assign/order', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

});
//
// router.on('/edit', (authorizationObj, requestId, payload) => {
//     let restaurantId = authorizationObj.restaurant_id;
//     orderController.edit(payload.order_id, null);
// });

module.exports = router;