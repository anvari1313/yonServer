const SocketRouter = require('./../../../lib/sockrouter');
const statusCode = require('http-status-codes');
const validate = require('validate.js');

const messageQ = require('../messageQ/messageQ');
const openHourController = require('./../../controllers/open-hour');

let router = new SocketRouter();

router.on('/get', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    let date = payload.date;
    openHourController.openHoursOfARestaurant(restaurantId, date).
    then(result => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
    catch(error => messageQ.sendError(restaurantId, error, requestId, statusCode.NOT_ACCEPTABLE))
});

module.exports = router;