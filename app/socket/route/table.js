const SocketRouter = require('./../../../lib/sockrouter');
const statusCode = require('http-status-codes');
const validate = require('validate.js');

const messageQ = require('../messageQ/messageQ');
const table_map_rules = require('../../validation-rules/table_map');
const tableController = require('../../controllers/table');
const mapController = require('../../controllers/map');
const errorObjects = require('../../rest/responses/codes');

let router = new SocketRouter();

router.on('/get', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    let table_id;
    validate.async(payload, table_map_rules.getTable, {cleanAttributes: true}).
    then(attr => {table_id = attr.table_id; return tableController.isValidTable(table_id);}).
    then(() => tableController.getTable(table_id)).
    then(res => messageQ.sendResponse(restaurantId, {table: res[0]}, requestId, statusCode.OK)).
    catch(error => {
        if (error instanceof Error)
            throw error;
        else if (error === 'Not a table')
            messageQ.sendResponse(restaurantId, errorObjects.tableNotFound, requestId, statusCode.NOT_FOUND);
        else
            messageQ.sendResponse(restaurantId, error, requestId, error.code);
    });
});

router.on('/add/one', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    mapController.isValidMap(payload.map_id).
    then(() => validate.async(payload, table_map_rules.addTable, {cleanAttributes: true})).
    then(attr => tableController.addOne(restaurantId, attr.map_id, attr)).
    then(res => messageQ.sendResponse(restaurantId, res, requestId, statusCode.OK)).
    catch((error) => {
        if (error instanceof Error)
            throw error;
        else if (error === 'Not a map')
            messageQ.sendResponse(restaurantId, errorObjects.mapNotFound, requestId, statusCode.NOT_FOUND);
        else
            messageQ.sendResponse(restaurantId, error, requestId, statusCode.NOT_ACCEPTABLE);
    });
});

router.on('/add/many', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    let errors = [];
    mapController.isValidMap(payload.map_id).
    then(()=>{
        if (Array.isArray(payload.tables))
            Promise.each(payload.tables, (tableItem, index) => {
                let error = validate.validate(tableItem, table_map_rules.addManyTable);
                if (error) errors.push(error);
            });
        else
            messageQ.sendResponse(restaurantId, validationFailCodes.notArrayType, requestId, statusCode.NOT_ACCEPTABLE);
    }).
    then(()=>{
        if (errors.length === 0)
            return tableController.addMany(restaurantId, payload.map_id, payload.tables);
        else
            throw errors;
    }).
    then((attr) => messageQ.sendResponse(restaurantId, {tables: attr}, requestId, statusCode.OK)).
    catch((error) => {
        if (error instanceof Error)
            throw error;
        else if (error === 'Not a map')
            messageQ.sendResponse(restaurantId, errorObjects.mapNotFound, requestId, statusCode.NOT_FOUND);
        else
            messageQ.sendResponse(restaurantId, error, requestId, error.code);
    });
});

router.on('/edit', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;


});

router.on('/delete', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;


});

module.exports = router;