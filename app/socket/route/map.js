const SocketRouter = require('./../../../lib/sockrouter');
const statusCode = require('http-status-codes');
const validate = require('validate.js');

const messageQ = require('../messageQ/messageQ');
const table_map_rules = require('../../validation-rules/table_map');
const mapController = require('../../controllers/map');
const errorObjects = require('../../rest/responses/codes');

let router = new SocketRouter();

router.onNotFound(() => {
    console.log('not found');
});

router.on('/add/one', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    validate.async(payload, table_map_rules.addMap, {cleanAttributes: true})
        .then((attr) => mapController.addOneMap(restaurantId, attr))
        .then((attr) => messageQ.sendResponse(restaurantId, attr.ops[0], requestId, statusCode.OK))
        .catch((error) => {
            console.log(error);
            if (error instanceof Error)
                throw new Error();
            else
                messageQ.sendResponse(restaurantId, error, requestId, statusCode.NOT_ACCEPTABLE);
        });
});

router.on('/add/many', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    if (Array.isArray(payload)) {
        let errors = [];
        Promise.each(payload, (payloadItem, index) => {
            let error = validate.validate(payloadItem, table_map_rules.addMap);
            if (error) errors.push(error);
        }).
        then(() => {
            if (errors.length === 0)
                return mapController.addManyMap(restaurantId, payload);
            else
                throw errors;
        }).
        then((attr) => messageQ.sendResponse(restaurantId, attr.ops, requestId, statusCode.OK)).
        catch((error) => {
            if (error instanceof Error)
                throw new Error();
            else
                messageQ.sendResponse(restaurantId, errors, requestId, statusCode.NOT_ACCEPTABLE);
        });
    } else
        messageQ.sendResponse(restaurantId, validationFailCodes.notArrayType, request_id, statusCodesNotAcceptable);
});

router.on('/get', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    let genPromise;
    if (payload.map_id)
        genPromise = mapController.isValidMap(payload.map_id);
    else
        genPromise = new Promise((resolve) => resolve());
    genPromise.then(() => mapController.getMap(restaurantId, payload.map_id)).
    then((res) => messageQ.sendResponse(restaurantId, res, requestId, statusCode.OK)).
    catch((error) => {
        if (error === 'Not a map')
            messageQ.sendResponse(restaurantId, errorObjects.mapNotFound, requestId, statusCode.OK);
        else {
            console.log(error);
            throw new Error();
        }
    });
});

module.exports = router;