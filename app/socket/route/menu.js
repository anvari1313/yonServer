const SocketRouter = require('./../../../lib/sockrouter');
const statusCode = require('http-status-codes');
const validate = require('validate.js');

const messageQ = require('../messageQ/messageQ');
const menuSectionController = require('../../controllers/menu-section');

let router = new SocketRouter();

router.on('/all', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    menuSectionController.getMenuSectionsOfRestaurant(restaurantId).
    then((result)=>{
        messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)
    })
});

router.on('/sections/add', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    menuSectionController.add(restaurantId, payload).
    then(result => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
    catch(error => console.log(error))
});

router.on('/sections/eatables', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;


});

router.on('/sections/eatables/add', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    menuSectionController.addEatableToSection(restaurantId, payload.menu_section_id, payload.eatables).
    then(res => { messageQ.sendResponse(restaurantId, res.ops, requestId, statusCode.OK) })
});

module.exports = router;