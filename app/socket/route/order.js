const SocketRouter = require('./../../../lib/sockrouter');
const statusCode = require('http-status-codes');
const validate = require('validate.js');

const messageQ = require('../messageQ/messageQ');
const order_rules = require('../../validation-rules/order');
const orderController = require('../../controllers/order');

let router = new SocketRouter();

router.on('/add', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    const valid = require('../../validation-rules/order').validate;
    validate.async(payload, order_rules.new, {cleanAttributes: true}).
    then( attr => orderController.add(restaurantId, attr)).
    then( result => messageQ.sendResponse(restaurantId, result, requestId, statusCode.OK)).
    catch(error => {
        if (error instanceof Error)
            throw error;
        else
            messageQ.sendResponse(restaurantId, error, requestId, statusCode.NOT_ACCEPTABLE);
    });

});

router.on('/get', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;

    orderController.listBetween(payload.start, payload.end).
    then((result)=> messageQ.sendResponse(restaurantId, result, request_id, statusCodesOK));
});

router.on('/edit', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;
    orderController.edit(payload.order_id, null);
});

router.on('/eatables/get', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;
    orderController.edit(payload.order_id, null);
});

router.on('/eatables/add', (authorizationObj, requestId, payload) => {
    let restaurantId = authorizationObj.restaurant_id;
    orderController.edit(payload.order_id, null);
});



module.exports = router;