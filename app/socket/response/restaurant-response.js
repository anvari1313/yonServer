const messageQ = require('./../messageQ/messageQ');

class RestaurantEvent{
    static get PONG() { return '/events/tools/connection/pong'; }
}

class RestaurantResponse{
    constructor(){

    }

    pong(restaurantId){
        messageQ.sendRequest(restaurantId, RestaurantEvent.PONG, { });
    }
}

module.exports = new RestaurantResponse();