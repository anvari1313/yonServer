/**
 * Created by ahmad on 3/31/17.
 */

const redis = require('redis');
const Promise = require('bluebird');
const conf = require('./../../../config/database').redis;

let client;

/**
 * This class is singleton in the project and just one instance of it is existed
 */
class MessageQ {

    constructor() {
        Promise.promisifyAll(redis.RedisClient.prototype);
        Promise.promisifyAll(redis.Multi.prototype);

        console.log(conf);
        client = redis.createClient({host: conf.host, family: conf.family, db: conf.db, password: conf.password}); //creates a new client

        client.on('connect', function (err) {
            if (err)
                console.log(new Date() + ' : Redis client encountered following error ' + err);

        });

        this.sendMessageCallbacks = new Map();
    }


    connectRestaurant(restaurantId, onReceiveMessage) {
        this.sendMessageCallbacks.set(restaurantId, onReceiveMessage);
    }

    sendRequest(restaurantId, event, payload){
        return this._sendMessage(restaurantId, {event: event, payload:payload});
    }

    sendError(restaurantId, errorMessage, requestId, status){
        return this._sendMessage(restaurantId, {status:status, request_id: requestId, error: errorMessage});
    }
    sendResponse(restaurantId, response, requestId, status) {
        return this._sendMessage(restaurantId, {status:status, request_id: requestId, response: response});
    }

    _sendMessage(restaurantId, message){

        let that = this;
        let obj = {};

        if(message.event) obj.event = message.event;
        if(message.payload) obj.payload = message.payload;
        if(message.status) obj.status = message.status;
        if(message.error) obj.error = message.error;
        if(message.response) obj.response = message.response;
        if(message.request_id) obj.request_id = message.request_id;


        client.rpushAsync(['restaurant-' + restaurantId, JSON.stringify(obj)])
            .then((reply) =>
                client.lrangeAsync('restaurant-' + restaurantId, 0, -1)
            ).then((allMessages) => {
                var sendToClient = that.sendMessageCallbacks.get(parseInt(restaurantId));
                if (sendToClient) {

                    allMessages.forEach(()=>{
                        client.lpopAsync('restaurant-' + restaurantId).
                        then((messageItem)=>{
                            var receivedStatus = sendToClient(messageItem);
                            if (receivedStatus !== true) {
                                return client.lpushAsync(['restaurant-' + restaurantId, messageItem]);
                            }
                        });

                    });

                    } else {
                    console.log('This is riddddddddddddddd');
                    
                }
                })
            .then((res)=> client.lrangeAsync('restaurant-' + restaurantId, 0, -1))
            // .then((res)=> console.log(res))
            .catch((err)=>{
                console.log(err);
            })
    }

    clearMessages(restaurantId){
        return client.delAsync('restaurant-' + restaurantId);
    }

}

module.exports = new MessageQ();
