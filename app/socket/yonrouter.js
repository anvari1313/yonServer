var SocketRouter = require('../../lib/socketrouter');

/**
 * Customized SocketRouter for this project that
 */
class YonRouter extends SocketRouter{
    constructor(){
        super();
    }

    on(eventHeader, eventHandler){
        super.on(eventHeader,(param, connection)=>{
            eventHandler(param.restaurantId, param.requestId ,param.payload);
        });
    }
}

module.exports = YonRouter;