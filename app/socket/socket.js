const WebSocketServer = require('websocket').server;
const routes = require('./route/routes');
const messageQ = require('./messageQ/messageQ');
const jwtAsync = require('../utils/jwt-promise');
const jwt = require('jsonwebtoken');

const jwt_secret_key = require('../../config/secrets').jwt;
const socket_protocol = require('../../config/secrets').socket_protocol;


var wsServer;

var connections = new Map();


/**
 * SocketObject{
 *  event,          // example: getUser, getAllReservation
 *  payload:{}          // Other params
 * }
 * @param server
 */
function setupSocketServer(server) {
    // server = http.createServer(function(request, response) {
    //     console.log((new Date()) + ' Received request for ' + request.url);
    //     response.writeHead(404);
    //     response.end();
    // });
    //
    // server.listen(port, function() {
    //     console.log((new Date()) + ' Server is listening on port 8080');
    // });

    wsServer = new WebSocketServer({
        httpServer: server,
        keepAlive: true,
        keepaliveInterval: 10000,
        autoAcceptConnections: false
    });



    wsServer.on('request',(request) =>{
        console.log('Trying to connect from ', request.remoteAddress);
        authorizeConnection(request.httpRequest.headers, request.resourceURL.query).
        then((acceptedHeader)=>{

            var restaurantId = acceptedHeader.id;
            var connection = request.accept(socket_protocol, request.origin);

            connection.restaurantId = restaurantId;


            connections.set(restaurantId, connection);

            messageQ.connectRestaurant(restaurantId, (allMessages)=>{
                var connection = connections.get(restaurantId);
                if (connection.connected){
                    connection.send(allMessages);
                    return true;
                }else{
                    console.log('Socket is not connected ...');
                    return false;
                }
            });

            console.log((new Date()) + ' Connection accepted.');

            connection.on('message', (message)=>{
                try{
                    var reqObj = JSON.parse(message.utf8Data);
                    if (!reqObj.event)
                        messageQ.sendError(connection.restaurantId, 'No event is specified.', reqObj.request_id, 403);
                    else{
                        if (reqObj.request_id !== undefined)
                        // routes.invoke(reqObj.event, {restaurantId: connection.restaurantId, requestId: reqObj.request_id, payload: reqObj.payload}, connection);
                            routes.call(reqObj.event, {restaurant_id: connection.restaurantId}, reqObj.request_id, reqObj.payload);
                        else if (reqObj.event === '/tools/connection/ping')
                            routes.call(reqObj.event, {restaurantId: connection.restaurantId, payload: reqObj.payload}, connection);
                    }

                }catch(err){
                    console.log(err);
                    connection.close();
                }

            });

            connection.on('close',(reasonCode, description)=>{
                console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
            });

        }).catch((error)=>{
            console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected. ' + error);
            request.reject();
        });
    });
}

function authorizeConnection(handShakeHeader, requestQuery) {

    return new Promise((resolve, error) => {

        let jwt_token = handShakeHeader.authorization || requestQuery.authorization;        // For obeying the standards of Authorization header

        console.log('Client version:');
        console.log(requestQuery.version);
        if (handShakeHeader['sec-websocket-protocol'] !== socket_protocol)
        {
            error('No appropriate protocol');
            return;
        }
        if (jwt_token === undefined){
            error('No appropriate authentication');
            return;
        }

        jwtAsync.verifyAsync(jwt_token, jwt_secret_key)
            .then((err, decode)=>{
                if (err){
                    error(err);
                }
                else{
                    let decodedJWTToken = jwt.decode(jwt_token, jwt_secret_key);
                    if (decodedJWTToken == undefined)
                        error('No appropriate header');
                    else
                        resolve(decodedJWTToken);
                }
            });
    });
}

function sendToAll(messageObject) {

}

module.exports.setupSocketServer = setupSocketServer;