const messageQ = require('./../messageQ/messageQ');

class RestaurantRequest{
    constructor(){

    }

    static get CANCEL_RESERVATION() { return '/events/reservations/cancel'; }

    cancelReservation(restaurantId, reservationId){
        messageQ.sendRequest(restaurantId, RestaurantRequest.CANCEL_RESERVATION, { reservation_id: reservationId});
    }
}

module.exports = new RestaurantRequest();