const HomeMobileResponse = require('../rest/responses/home-mobile');
const restaurantController = require('./../controllers/restaurant');
const zoneController = require('./zone');
const BaseController = require('./basecontroller');
const Promised = require('bluebird');

class HomeController extends BaseController {
    constructor() {
        super();
    }

    fillHome() {
        let banners = {
            "type": "banner",
            "banners": [
                {
                    "banner_url": "http://162.243.174.32/banners/banner1.jpg",
                    "title": "تخفیف های ویژه عید غدیر خم",
                    "sub_title": "بهترین ها تخفیف های عید غدیر خم را در اینجا مشاهده کنید!",
                    "type": 2,
                    "target_list_id": "1",
                },
                {
                    "banner_url": "http://162.243.174.32/banners/restaurant.jpeg",
                    "title": "جدیدترین های امروز",
                    "sub_title": "اگر اهل آزمایش کردن گزینه های جدید هستید حتما سری به این رستوران ها بزنید!",
                    "type": 2,
                    "target_list_id": "2",
                },
                {
                    "banner_url": "http://162.243.174.32/banners/super.jpeg",
                    "title": "گردشی در دنیای غذا",
                    "sub_title": "به دنیای غذا بروید و لذت ببرید!",
                    "type": 0,
                    "target_url": "https://www.reyhoon.com/restaurants?location=%D9%85%DB%8C%D8%AF%D8%A7%D9%86%20%D8%A8%D8%A7%D9%87%D9%86%D8%B1&area=%D9%86%DB%8C%D8%A7%D9%88%D8%B1%D8%A7%D9%86&city=%D8%AA%D9%87%D8%B1%D8%A7%D9%86",
                },
                {
                    "banner_url": "http://162.243.174.32/banners/banner1.jpg",
                    "title": "تخفیف ویژه رستوران اکبر جوجه",
                    "sub_title": "بهترین ها را در رستوران اکبر جوجه بخواهید!",
                    "type": 1,
                    "target_id": "1",
                    "restaurant": {
                        "_id": 1,
                        "name": "برگرلند مصطفی",
                        "branch": "مصطفی",
                        "address": "تهرانپارس نرسیده به فلکه اول",
                        "price_rate": 4,
                        "rating": 4.6,
                        "avatar": "http://162.243.174.32/restaurant/avatar/1665.jpg",
                        "zone_name": "چهارراه ولیعصر",
                        "zone_slug": "ValiasrCrossroads",
                    }
                },
                {
                    "banner_url": "http://162.243.174.32/banners/banner2.jpg",
                    "title": "کافه گریل",
                    "sub_title": "کافه گریل شما را به بهترین مزه ها دعوت میکند.",
                    "type": 1,
                    "target_id": "33",
                    "restaurant": {
                        "_id": 33,
                        "name": "کافه گریل 458",
                        "address": "خیابان شهید بهشتی، روبروی سینما آزادی",
                        "rating": 2.5,
                        "price_rate": 1,
                        "avatar": "http://162.243.174.32/restaurant/avatar/1635.jpg",
                        "zone_slug": "ValiasrCrossroads",
                        "zone_name": "چهارراه ولیعصر",
                    }
                },
                {
                    "banner_url": "http://162.243.174.32/banners/restaurant.jpeg",
                    "title": "جشنواره روزانه",
                    "sub_title": "روزانه سه وعده غذا بخورید",
                    "type": 1,
                    "target_id": "1",
                    "restaurant": {
                        "_id": 1,
                        "name": "برگرلند مصطفی",
                        "branch": "مصطفی",
                        "address": "تهرانپارس نرسیده به فلکه اول",
                        "price_rate": 4,
                        "rating": 4.6,
                        "avatar": "http://162.243.174.32/restaurant/avatar/1665.jpg",
                        "zone_name": "چهارراه ولیعصر",
                        "zone_slug": "ValiasrCrossroads",
                    }
                }
            ]
        };

        let simpleSection = {
            "type": "simple_section",
            "title": "بازدید های اخیر",
            "restaurants": [
                {
                    "_id": 87,
                    "name": "سلف سرویس سارینا",
                    "address": "پیچ شمیران، جنب بانک سپه، پلاک 4",
                    "price_rate": 2,
                    "rating": 4.9,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1480.jpg",
                    "zone_slug": "DarvazeDolat",
                    "zone_name": "دروازه دولت"
                },
                {
                    "_id": 78,
                    "name": "کبابخوان",
                    "address": "خیابان فاطمی، خیابان دائمی",
                    "price_rate": 3,
                    "rating": 3.5,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1764.jpg",
                    "zone_slug": "DrFatemi",
                    "zone_name": "دکتر فاطمی"
                },
                {
                    "_id": 79,
                    "name": "برگر استار",
                    "address": "گیشا، خیابان جوادی، نبش خیابان ۲۳",
                    "price_rate": 3,
                    "rating": 3.5,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1622.jpg",
                    "zone_slug": "DrFatemi",
                    "zone_name": "دکتر فاطمی"
                },
                {
                    "_id": 99,
                    "name": "برگر زغالی هیچ",
                    "address": "خیابان ولیعصر، بالاتر از چهارراه زرتشت",
                    "price_rate": 1,
                    "rating": 4.2,
                    "avatar": "http://162.243.174.32/restaurant/avatar/129.jpg",
                    "zone_slug": "Taleghani",
                    "zone_name": "طالقانی"
                },
            ]
        };

        let compactRecom = {
            "type": "compact_recom",
            "recoms": [
                {
                    "title": "بهترین قیمت ها",
                    "restaurants": [
                        {
                            "_id": 70,
                            "name": "ایران برگر",
                            "address": "خیابان کارگر شمالی، نبش کوچه طباطبائی",
                            "price_rate": 3,
                            "rating": 4.5,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1567.jpg",
                            "zone_slug": "EnghelabSq",
                            "zone_name": "انقلاب"
                        },
                        {
                            "_id": 71,
                            "name": "دیپلمات",
                            "address": "خیابان آزادی، بین جمالزاده جنوبی و انقلاب",
                            "price_rate": 3,
                            "rating": 4.5,
                            "avatar": "http://162.243.174.32/restaurant/avatar/445.jpg",
                            "zone_slug": "EnghelabSq",
                            "zone_name": "انقلاب"
                        },
                        {
                            "_id": 72,
                            "name": "رستوران سنتی گل",
                            "address": "خیابان ولیعصر، بعد از امیر اکرم، کوچه هاشمی",
                            "price_rate": 3,
                            "rating": 4.5,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1467.jpg",
                            "zone_slug": "EnghelabSq",
                            "zone_name": "انقلاب"
                        }
                    ]
                },
                {
                    "title": "بهترین مزه ها",
                    "restaurants": [
                        {
                            "_id": 74,
                            "name": "پیتزا تام",
                            "address": "خیابان جمهوری، خیابان شیخ هادی،",
                            "price_rate": 4,
                            "rating": 3.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/242.jpg",
                            "zone_slug": "Ferdosi",
                            "zone_name": "فردوسی"
                        },
                        {
                            "_id": 75,
                            "name": "غذاکده گندم",
                            "address": "خیابان شریعتی، بالاتر از سه راه طالقانی",
                            "price_rate": 4,
                            "rating": 3.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1727.jpg",
                            "zone_slug": "Ferdosi",
                            "zone_name": "فردوسی"
                        },
                        {
                            "_id": 76,
                            "name": "ایران ایتالیا",
                            "address": "خیابان ولی عصر، بالاتر از چهارراه ولی عصر",
                            "price_rate": 4,
                            "rating": 3.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/547.jpg",
                            "zone_slug": "ValiasrCrossroads",
                            "zone_name": "جهارراه ولیعصر"
                        },
                    ]
                },
                {
                    "title": "بیشترین بازدید",
                    "restaurants": [
                        {
                            "_id": 82,
                            "name": "ویلا بیکن",
                            "address": "خیابان انقلاب، خیابان ویلا، پلاک 49",
                            "price_rate": 3,
                            "rating": 2.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1470.jpg",
                            "zone_slug": "Jomhouri",
                            "zone_name": "جمهوری"
                        },
                        {
                            "_id": 84,
                            "name": "شماعیان",
                            "address": "خیابان جمهوری، نبش چهارراه استانبول",
                            "price_rate": 3,
                            "rating": 2.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1229.jpg",
                            "zone_slug": "Jomhouri",
                            "zone_name": "جمهوری"
                        },
                        {
                            "_id": 33,
                            "name": "کافه گریل 458",
                            "address": "خیابان شهید بهشتی، روبروی سینما آزادی",
                            "rating": 2.5,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1635.jpg",
                            "zone_slug": "ValiasrCrossroads",
                            "zone_name": "چهارراه ولیعصر"
                        }
                    ]
                },
                {
                    "title": "دنج ترین رستوران ها",
                    "restaurants": [
                        {
                            "_id": 74,
                            "name": "پیتزا تام",
                            "address": "خیابان جمهوری، خیابان شیخ هادی،",
                            "price_rate": 4,
                            "rating": 3.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/242.jpg",
                            "zone_slug": "Ferdosi",
                            "zone_name": "فردوسی"
                        },
                        {
                            "_id": 75,
                            "name": "غذاکده گندم",
                            "address": "خیابان شریعتی، بالاتر از سه راه طالقانی",
                            "price_rate": 4,
                            "rating": 3.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/1727.jpg",
                            "zone_slug": "Ferdosi",
                            "zone_name": "فردوسی"
                        },
                        {
                            "_id": 76,
                            "name": "ایران ایتالیا",
                            "address": "خیابان ولی عصر، بالاتر از چهارراه ولی عصر",
                            "price_rate": 4,
                            "rating": 3.9,
                            "avatar": "http://162.243.174.32/restaurant/avatar/547.jpg",
                            "zone_slug": "ValiasrCrossroads",
                            "zone_name": "جهارراه ولیعصر"
                        },
                    ]
                }
            ]
        };

        let recommendedZones = {
            "type": "zone_recom",
            "zones": [
                {
                    name: "چهارراه ولیعصر",
                    long: 35.706292,
                    lat: 51.406083,
                    slug: "ValiasrCrossroads"
                },
                {
                    name: "انقلاب",
                    lat: 51.397925,
                    slug: "EnghelabSq",
                    long: 35.701075
                },
                {
                    name: "دکتر فاطمی",
                    lat: 51.402269,
                    slug: "DrFatemi",
                    long: 35.718778
                },
                {
                    name: "طالقانی",
                    lat: 51.422015,
                    slug: "Taleghani",
                    long: 35.706857
                },
                {
                    name: "دروازه دولت",
                    lat: 51.426104,
                    slug: "DarvazeDolat",
                    long: 35.7018
                },
                {
                    name: "جمهوری",
                    lat: 51.403941,
                    slug: "Jomhouri",
                    long: 35.694398
                },
            ]
        };


        let simepleSection2 = {
            "type": "simple_section",
            "title": "بازدید های اخیر دوستان شما",
            "restaurants": [
                {
                    "_id": 87,
                    "name": "سلف سرویس سارینا",
                    "address": "پیچ شمیران، جنب بانک سپه، پلاک 4",
                    "price_rate": 2,
                    "rating": 4.9,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1480.jpg",
                    "zone_slug": "DarvazeDolat",
                    "zone_name": "دروازه دولت"
                },
                {
                    "_id": 79,
                    "name": "برگر استار",
                    "address": "گیشا، خیابان جوادی، نبش خیابان ۲۳",
                    "price_rate": 3,
                    "rating": 3.5,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1622.jpg",
                    "zone_slug": "DrFatemi",
                    "zone_name": "دکتر فاطمی"
                },
                {
                    "_id": 31,
                    "name": "ماسالا برگر",
                    "address": "کریم خان زند، خیابان حسینی",
                    "rating": 2.5,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1689.jpg",
                    "zone_slug": "ValiasrCrossroads",
                    "zone_name": "چهارراه ولیعصر"
                },
                {
                    "_id": 72,
                    "name": "رستوران سنتی گل",
                    "address": "خیابان ولیعصر، بعد از امیر اکرم، کوچه هاشمی",
                    "price_rate": 3,
                    "rating": 4.5,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1467.jpg",
                    "zone_slug": "EnghelabSq",
                    "zone_name": "انقلاب"
                }
            ]
        };

        let recommendedTags = {
            "type": "tag_recom",
            "tags": [
                {
                    "name": "چینی",
                    "slug": "chinees",
                    "avatar": "http://162.243.174.32/tags/sushi.jpg",
                },
                {
                    "name": "سالاد بار",
                    "slug": "salad-bar",
                    "avatar": "http://162.243.174.32/tags/saladb.jpg",
                },
                {
                    "name": "سنتی",
                    "slug": "iranian",
                    "avatar": "http://162.243.174.32/tags/persian-kebab-1.jpg"
                },
                {
                    "name": "نوشیدنی",
                    "slug": "drinks",
                    "avatar": "http://162.243.174.32/tags/drinks.jpg",
                },
                {
                    "name": "فست‌فود",
                    "slug": "fastfood",
                    "avatar": "http://162.243.174.32/tags/fast-food.jpg"
                },
                {
                    "name": "اتاق سیگار",
                    "slug": "smooking-room",
                    "avatar": "http://162.243.174.32/tags/rook-ruimte-smoking-room.jpg"
                }
            ]
        };

        let singleBanner = {
            "type": "single_banner",
            "banner": {
                "banner_url": "http://162.243.174.32/banners/mostafa-burgerland.jpg",
                "icon_url": "http://162.243.174.32/restaurant/avatar/1665.jpg",
                "title": "برگرلند مصطفی",
                "sub_title": "انواع برگر‌های مصطفی‌پز و چیپس و پنیر‌های احمد پسند",
                "type": 1, // valid values: 0 -> url, 1 -> restaurant_targer, 2 -> list_target
                "target_url": "http://www.google.com",
                "target_id": 1,
                "color_code": "#d8b700",
                "rating": 5,
                "restaurant": {
                    "_id": 1,
                    "name": "برگرلند مصطفی",
                    "branch": "مصطفی",
                    "address": "تهرانپارس نرسیده به فلکه اول",
                    "price_rate": 4,
                    "rating": 4.6,
                    "avatar": "http://162.243.174.32/restaurant/avatar/1665.jpg",
                    "zone_name": "انقلاب",
                    "zone_slug": "EnghelabSq"
                }
            }
        };

        let result = [];
        result.push(banners);
        result.push(simpleSection);
        result.push(compactRecom);
        result.push(singleBanner);
        result.push(recommendedZones);
        result.push(simepleSection2);
        result.push(compactRecom);
        result.push(recommendedTags);
        result.push(recommendedZones);
        result.push(compactRecom);

        return result;
    }

    homeMobile(input) {
        return new Promised((resolve, error) => {

            let result = this.fillHome();
            if (input.long && input.lat) {
                zoneController.nearestZone({long: parseFloat(input.long), lat: parseFloat(input.lat)}).then((r) => {
                    resolve(new HomeMobileResponse(result, r));
                });
            } else
                resolve(new HomeMobileResponse(result));

        });
    }
}

module.exports = new HomeController();