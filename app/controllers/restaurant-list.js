const BaseController = require('./basecontroller');
const Promise = require('bluebird');
const RestaurantList = require('./../models/restaurant-list');
const Restaurant = require('./../models/restaurant');
const Tag = require('./../models/tag');

class RestaurantListController extends BaseController{
    constructor(){
        super();
    }

    getListWithId(listId){
        return new Promise((resolve, reject) => {
            RestaurantList.getListWithId(listId).
            then(result => resolve(result)).
            catch(error => reject(error));
        });
    }

    getList(listId){
        return new Promise((resolve, reject) => {
            let res;
            RestaurantList.getListWithId(listId).
            then(result => { res = result; res.restaurants = []; return RestaurantList.getListMemberWithId(result._id); }).
            then(rawRestaurants => Promise.each(rawRestaurants, (rawRestaurant)=> {
                let restaurant = Restaurant.convertRawData(rawRestaurant);
                restaurant = Restaurant.convertPathToAbsolute(restaurant);
                let rawTags = restaurant.tags;
                if (rawTags){
                    let tags = [];
                    for (let rawTag of rawTags){
                        let tag = Tag.convertRawData(rawTag);
                        tag.convertPathToAbsolute();
                        tags.push(tag);
                    }
                    restaurant.tags = tags;
                }

                res.restaurants.push(restaurant);
            })).
            then(result => resolve(res)).
            catch(error => reject(error));
        });
    }
}

module.exports = new RestaurantListController();