const BaseController = require('./basecontroller');
const Promise = require('bluebird');
const pgErrors = require('pg-promise').errors;
const jwt = require('jsonwebtoken');
const db = require('../database');

const openHourController = require('./open-hour');
const smsController = require('./sms');

const secrets = require('../../config/secrets');

const RestaurantAuthResponse = require('../rest/responses/restaurant-auth');
const Restaurant = require('../models/restaurant');
const Tag = require('./../models/tag');

class RestaurantController extends BaseController {
    constructor() {
        super();

    }

    login(email, password) {
        let restaurant;
        return new Promise((resolve, reject) => {
            db.pg().
                one(Restaurant.query().findByEmail, [email]).
                then(data => Restaurant.convert(data)).
                then(_restaurant => restaurant = _restaurant).
                then(x => restaurant.comparePassword(password)).
                then(isSimilar => {
                    restaurant._password = undefined;
                    if (isSimilar)
                        resolve(new RestaurantAuthResponse(true, jwt.sign({id: restaurant._id}, secrets.jwt), restaurant));
                    else
                        resolve(new RestaurantAuthResponse(false));
                }).
                catch(error => {
                    if (!error instanceof pgErrors.QueryResultError)
                        throw error;

                    // if user doesn't exists in database
                    if (error.code === pgErrors.queryResultErrorCode.noData)
                        resolve(new RestaurantAuthResponse(false));
                    else
                        throw error;
                }).
                catch(error => {
                    reject(error);
                });
        });
    }

    localSignUp(restaurant){}

    loginSMSToken(cellPhone, smsToken){
        return Restaurant.findByCellphoneAndTokenRespectExpiration(cellPhone, smsToken).
        then(restaurant => new RestaurantAuthResponse(true, jwt.sign({id: restaurant._id}, secrets.jwt), restaurant));
    }

    findInfo(restaurantId){
        return new Promise((resolve, reject)=>{
            let restaurantObj;
            Restaurant.findById(db.pg(), restaurantId).
            then((restaurant)=>{
                restaurantObj = Restaurant.convertRawData(restaurant);
                // restaurantObj = Restaurant.convertPathToAbsolute(restaurantObj)
                return openHourController.openHoursOfARestaurant(restaurant._id);
            }).
            then((hours)=>{ restaurantObj.open_hours = hours; resolve(restaurantObj)}).
            catch(error=>reject(error));
        });
    }

    find(restaurantId){
        return new Promise((resolve, reject)=>{
            let restaurantObj;
            Restaurant.findById(db.pg(), restaurantId).
            then((restaurant)=>{
                restaurantObj = Restaurant.convertRawData(restaurant);
                resolve(restaurantObj)
            }).
            catch(error=>reject(error));
        });
    }

    searchByTag(tags, location){
        return new Promise((resolve, reject)=>{
            let restaurantObjs = [];
            let promise;
            if (location) promise = Restaurant.searchByTagDistanceSorted(tags, location);
            else promise = Restaurant.searchByTag(tags);

            promise.
            then(restaurants => Promise.each(restaurants, (restaurant) => {
                let restaurantObj = Restaurant.convertRawData(restaurant);
                let rest =Restaurant.convertPathToAbsolute(restaurantObj);
                let rawTags = rest.tags;
                if (rawTags){
                    let tags = [];
                    for (let rawTag of rawTags){
                        console.log(rawTag);
                        let tag = Tag.convertRawData(rawTag);
                        tag.convertPathToAbsolute();
                        tags.push(tag);
                    }
                    rest.tags = tags;
                }

                restaurantObjs.push(rest);
            })).
            then(()=> resolve(restaurantObjs)).
            catch( error => reject(error));
        });
    }

    isValid(restaurantId){
        return Restaurant.isValid(db.pg(), restaurantId);
    }

    searchByName(nameLike){

    }

    searchByNameOrBranch(name){
        let names = name.split(' ').filter(i => i);
        let restaurants = [];
        return new Promise((resolve, reject)=>{
           Restaurant.searchByNameOrBranch(names).
           then((rawRestaurants)=> Promise.each(rawRestaurants, (rawRestaurant)=> {

               let restaurant = Restaurant.convertRawData(rawRestaurant);
               restaurant = Restaurant.convertPathToAbsolute(restaurant);
               let rawTags = restaurant.tags;
               if (rawTags){
                   let tags = [];
                   for (let rawTag of rawTags){
                       let tag = Tag.convertRawData(rawTag);
                       tag.convertPathToAbsolute();
                       tags.push(tag);
                   }
                   restaurant.tags = tags;
               }

               restaurants.push(restaurant);
           })).then(()=>resolve(restaurants)).
           catch((error)=>reject(error));
        });
    }

    searchByZone(zoneSlug, location){
        return new Promise((resolve, reject)=>{
            let restaurants = [];
            Restaurant.searchByZone(zoneSlug).
            then((rawRestaurants)=> Promise.each(rawRestaurants, (rawRestaurant)=> {

                let restaurant = Restaurant.convertRawData(rawRestaurant);
                restaurant = Restaurant.convertPathToAbsolute(restaurant);
                let rawTags = restaurant.tags;
                if (rawTags){
                    let tags = [];
                    for (let rawTag of rawTags){
                        console.log(rawTag);
                        let tag = Tag.convertRawData(rawTag);
                        tag.convertPathToAbsolute();
                        tags.push(tag);
                    }
                    restaurant.tags = tags;
                }

                restaurants.push(restaurant);
            })).then(()=>resolve(restaurants)).
            catch((error)=>reject(error));
        });
    }

    _allRestaurants(){
        return Restaurant._allRestaurants();
    }

    generateSMSToken() {
        let min = 10000;
        let max = 99999;

        return Math.floor(Math.random() * (max - min) + min);
    }

    sendSMSToken(cellPhone){
        let token = this.generateSMSToken();
        return Restaurant.saveSMSToken(cellPhone, token).
        then(result => smsController.sendRestaurantAuthToken(cellPhone, token))
    }
}

module.exports = new RestaurantController();