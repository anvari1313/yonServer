const BaseController = require('./basecontroller');
const Tag = require('./../models/tag');
const db = require('./../database');
const Promise = require('bluebird');

class TagController extends BaseController{
    constructor(){
        super();
    }

    getRecommendedTags(userId){

    }

    getAllTags(nameLike){
        return new Promise((resolve, reject)=>{
            let tags = [];
            let nameLikes = nameLike.split(' ').filter(i => i);
            if (nameLikes.length === 0) nameLikes = [''];
            Tag.getAllTags(nameLikes).
            then(results=>Promise.each(results, (result)=> tags.push(Tag.convertRawData(result)))).
            then(()=>resolve(tags)).
            catch((error)=>reject(error));
        });
    }

    getAllTagsOf(restaurantId){
        return new Promise((resolve, reject)=>{
            let tags = [];
            Tag.getTagsOfRestaurant(db.pg(), restaurantId).
            then(results=>Promise.each(results, (result)=> tags.push(Tag.convertRawData(result)))).
            then(()=>resolve(tags)).
            catch((error)=>reject(error));
        });

    }
}

module.exports = new TagController();