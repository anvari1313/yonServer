const Promise = require('bluebird');
const BaseController = require('./basecontroller');
const FCMDevice = require('./../models/fcm-device');

class FCMDeviceController extends BaseController{
    constructor(){
        super();
    }

    newDevice(userId, fcmTokenId){
        let fcmDevice = new FCMDevice({ user_id: userId, fcm_token_id: fcmTokenId});
        let promise = (userId) ?
            FCMDevice.tokensOfUser(userId).
            then(result => {
                let fcm_token_id_index = result.fcm_token_ids.indexOf(fcmTokenId);
                if (fcm_token_id_index >= 0)
                    return Promise.resolve(fcmDevice);
                else {
                    return fcmDevice.save();
                }
            })
            :
            fcmDevice.save();

        return promise;

    }

    updateByLastToken(oldToken, newToken, userId){
        let promise = (userId) ?
            FCMDevice.tokensOfUser(userId).
            then(result => {
                if (result.fcm_token_ids.indexOf(oldToken) >= 0)
                    return Promise.resolve();
                else
                    return Promise.reject('unauthorized')
            })
            :
            Promise.resolve();

        return promise.then(() => FCMDevice.updateByLastToken(oldToken, newToken));
    }

    fcmTokensOfUser(userId){
        return FCMDevice.tokensOfUser(userId);
    }

}

module.exports = new FCMDeviceController();