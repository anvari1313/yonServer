const Promise = require('bluebird');
const BaseController = require('./basecontroller');
const mail_transporter = require('./../../config/email');

class EmailController extends BaseController{
    constructor(){
        super();
    }

    _sendEmail(address, email){
        return new Promise((resolve, reject)=>{
            try{
                let mailOptions = {
                    to: address,
                    subject: email.subject,
                    html: email.html
                };

                mail_transporter.sendMail(mailOptions,(error, info) => {
                    if (error !== null) {
                        reject(error);
                    } else {
                        resolve(info);
                    }
                });
            }catch (ex){
                console.log('exception thrown')
            }
        });
    }


}

module.exports = new EmailController();