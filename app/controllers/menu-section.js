const BaseController = require('./basecontroller');
const Promise = require('bluebird');
const db = require('./../database');
const eatableController = require('./eatable');
const MenuSection = require('./../models/menu-section');
const Eatable = require('./../models/eatable');

class MenuSectionController extends BaseController {
    constructor() {
        super();
    }

    getMenuSectionsOfRestaurant(restaurantId){
        return new Promise((resolve, reject)=>{
            let menuSections = [];
            MenuSection.getOfRestaurant(db.mongodb(), restaurantId).
            then((results)=> Promise.each(results, result=> {
                let menuSection = MenuSection.convertRawData(result);
                for(let i = 0; i < menuSection.eatables.length; i++){
                    menuSection.eatables[i] = Eatable.convertPathToAbsolute(menuSection.eatables[i]);
                }
                menuSections.push(menuSection);
            })).
            then(()=> resolve(menuSections)).
            catch((error)=> reject(error));
        });

    }

    add(restaurantId, properties){
        return new Promise((resolve, reject)=>{
            let menuSection = new MenuSection({
                name: properties.name,
                show_order: properties.show_order,
                restaurant_id: restaurantId
            });
            menuSection.save().
            then(attr => resolve(attr.ops[0])).
            catch(error => reject(error));
        });
    }

    addEatableToSection(restaurantId, sectionId, eatables){
        let menuSection;
        return MenuSection.findSection(sectionId).
        then(rawSection => { menuSection = MenuSection.convertRawData(rawSection); console.log(rawSection); return eatableController.addManyEatables(restaurantId, eatables); }).
        then(res =>  menuSection.addEatables(res.ops).save() )
    }

}

module.exports = new MenuSectionController();