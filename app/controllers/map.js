const BaseController = require('./basecontroller');
const Map = require('./../models/map');
const Promise = require('bluebird');

class MapController extends BaseController{
    constructor(){
        super();
    }

    addOneMap(restaurantId, properties){
        let map = new Map({ restaurant_id:restaurantId, name: properties.name, width: properties.width, height: properties.height});
        return map.add();
    }

    addManyMap(restaurantId, attributes){
        let maps = [];
        return Promise.each(attributes,(attribute)=>{
            maps.push(new Map({ restaurant_id:restaurantId, name: attribute.name, width: attribute.width, height: attribute.height}))
        }).
        then(()=>Map.addMany(maps));
    }

    getMap(restaurantId, mapId){
        return Map.getMap(restaurantId, mapId);
    }

    isValidMap(mapId){
        return Map.isValidMap(mapId);
    }

    isMapBelongToRestaurant(restaurantId, map){
        return new Promise((resolve, reject)=>{
            if (restaurantId == map.restaurant_id)
                resolve(map);
            else
                reject('Table not belonged to restaurant.');
        });
    }
}

module.exports = new MapController();