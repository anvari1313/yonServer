const BaseController = require('./basecontroller');
const Review = require('./../models/review');
const db = require('./../database');
const Promise = require('bluebird');
const reservationController = require('./../controllers/reservation');
const userController = require('./../controllers/user');


class ReviewController extends BaseController{
    constructor(){
        super();
    }

    isAllowToSendReview(userId){
        console.log(userId);
        return reservationController.hasReservation(userId);
    }

    addReview(restaurantId, user, reviewObj){

        let review = new Review({
            restaurant_id: restaurantId,
            user:
                {
                    _id: user._id,
                    fname: user.fname,
                    lname: user.lname
                },
            rate: reviewObj.rate,
            text: reviewObj.text
        });

        return review.save(db.mongodb());
    }

    getReviewsOfRestaurant(restaurantId){
        return Review.getReviewsOfRestaurant(restaurantId);
    }
}

module.exports = new ReviewController();