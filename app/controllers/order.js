const BaseController = require('./basecontroller');
const Order = require('./../models/order');
const Promise = require('bluebird');

class OrderController extends BaseController{
    constructor(){
        super();
    }

    add(restaurantId, orderParam){
        let order = new Order({
            restaurant_id: restaurantId,
            submit_datetime: orderParam.submit_datetime,
            eatables: orderParam.eatables,
            reservation_id: orderParam.reservation_id,
            table_id: orderParam.table_id,
            transaction_id: orderParam.transaction_id,
            preparation_status: orderParam.preparation_status,
            is_gone: orderParam.is_gone,
            order_pnum: orderParam.order_pnum,
            is_table_unknown: orderParam.is_table_unknown,
            is_takeaway: orderParam.is_takeaway,
            description: orderParam.description,
            createdAt: orderParam.createdAt,
            updatedAt: orderParam.updatedAt
        });

        return order.upsert();
    }

    listBetween(startTimestamp, endTimestamp){
        return new Promise((resolve, reject)=>{
            let orderObjects = [];
            Order.listBetween(startTimestamp, endTimestamp).
            then((results)=>Promise.each(results, result => orderObjects.push(Order.convertRawData(result)))).
            then(() => resolve(orderObjects)).
            catch((error)=> reject(error));
        });
    }

    edit(orderId, order){
        return Order.findById(orderId).
        then(rawOrder => Order.convertRawData(rawOrder))
    }
}

module.exports = new OrderController();