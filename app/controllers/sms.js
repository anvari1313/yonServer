const Promise = require('bluebird');
const BaseController = require('./basecontroller');
const request = require('request-promise');
const auth_token = require('./../../config/secrets').sms_provider_auth_token;

class SmsController extends BaseController{
    constructor(){
        super();
    }

    sendRestaurantAuthToken(cellPhone, token){
        return this._sendSms(cellPhone, 'میزیت\nکد تایید هویت شما : ' + token)
    }

    sendUserAuthToken(cellPhone, token){
        return this._sendSms(cellPhone, 'میزیت\nکد تایید هویت شما : ' + token)
    }

    sendUserReservationCancel(cellPhone, reservation){
        const moment = require('moment-jalaali');

        return this._sendSms(cellPhone, 'میزیت\nرزرو شما در ' +
            reservation.restaurant_name + ' برای روز ' +
            moment.unix(reservation.reservation_datetime).format('jYYYY/jMM/jDD') + ' توسط خود کافه/رستوران کنسل شده است.')
    }

    _sendSms(number, text){
        return new Promise((resolve, reject)=> {

            let options = {
                method: 'POST',
                uri: 'https://api.kavenegar.com/v1/' + auth_token + '/sms/send.json',
                headers:{
                    'Content-Type':'application/json'
                },
                body: {
                    "receptor": number,
                    "message" : text,
                    "sender": "10005550555500"
                },
                json: true // Automatically stringifies the body to JSON
            };

            let str = 'receptor=' + options.body.receptor + '&message=' + options.body.message + '&sender=' + options.body.sender;
            options.uri = options.uri + '?' + str;

            // console.log(options);
            request(options)
                .then(parsedBody => {
                    // POST succeeded...
                    console.log('this is herer');
                    console.log(parsedBody);
                    console.log('this is herer');
                    resolve(parsedBody);
                    console.log(parsedBody);
                })
                .catch(err => {
                    // POST failed...
                    reject(err);
                });

            console.log(number + ' -> ' + text);
            // resolve();
        });
    }


}

module.exports = new SmsController();