const BaseController = require('./basecontroller');
const Promise = require('bluebird');
const pgErrors = require('pg-promise').errors;
const jwt = require('jsonwebtoken');
const jwtAsync = require('./../utils/jwt-promise');
const db = require('../database');
const smsController = require('./sms');
const secrets = require('../../config/secrets');

const UserSignUpResponse = require('../rest/responses/user-signup');
const User = require('../models/user');
const dbErrorParser = require('./../utils/db-error-parser').parseError;


class UserController extends BaseController {
    constructor() {
        super();
    }

    tokenLogin(cellPhone, smsToken) {
        let user;
        let expiration = 15 * 60;

        return new Promise((resolve, reject) => {
            db.pg().one(User.query.findByCellPhone, [cellPhone]).
            then(data => User.convert(data)).
            then(_user => user = User.convertRawData(_user)).
            then(x => user.compareToken(smsToken, expiration)).
            then(x => user.verifyCellphone()).
            then(t => {
                console.log(t);
                user.password = undefined;
                resolve({token: jwt.sign({id: user._id}, secrets.jwt), user: user});
            }).catch(error => {
                console.log(user);
                if (!error instanceof pgErrors.QueryResultError) {
                    throw error;
                }

                // if user doesn't exists in database
                if (error.code === pgErrors.queryResultErrorCode.noData)
                    reject('user does\'t exist');
                else
                    throw error;
            }).catch(error => {
                reject(error);
            });
        });
    }

    login(email, password) {
        let user;


        return new Promise((resolve, reject) => {
            db.pg().one(User.query.findByEmail, [email]).then(data => User.convert(data)).then(_user => user = _user).then(x => user.comparePassword(password)).then(isSimilar => {
                if (isSimilar) {
                    user.password = undefined;
                    resolve({token: jwt.sign({id: user._id}, secrets.jwt), user: user});
                } else
                    reject('password doesn\'t match');
            }).catch(error => {
                if (!error instanceof pgErrors.QueryResultError) {
                    throw error;
                }

                // if user doesn't exists in database
                if (error.code === pgErrors.queryResultErrorCode.noData)
                    reject('user does\'t exist');
                else
                    throw error;
            }).catch(error => {
                reject(error);
            });
        });
    }

    localSignUp(user) {
        return new Promise((resolve, reject) => {
            let newUser = new User(
                {
                    fname: user.fname,
                    lname: user.lname,
                    birthdate: user.bithdate,
                    cell_phone: user.cell_phone,
                    password: user.password,
                    email: user.email
                });

            newUser.hashPassword().
            then((hash) => newUser.saveToDatabase(db)).
            then(addedUser => {
                this.sendSMSToken(user.cell_phone);
                reject({ cellPhoneNotVerifiedButUserCreated: true, m: 'cell_phone not verified', cell_phone: this.codeCellPhone(addedUser.cell_phone)})}).
            catch(error => {
                if (!(error instanceof Error))
                    throw error;

                reject(dbErrorParser(error));

            }).catch(error => resolve(error));
        });
    }

    decodeToken(jwt_token) {
        return new Promise((resolve, error) => {
            if (jwt_token === undefined) {
                resolve(null);
            }
            else {
                jwtAsync.verifyAsync(jwt_token, secrets.jwt)
                    .then((err, decode) => {
                        if (err) {
                            error(err);
                        }
                        else {
                            let decodedJWTToken = jwt.decode(jwt_token, secrets.jwt);
                            if (decodedJWTToken == undefined)
                                error('No appropriate token');
                            else
                                resolve(decodedJWTToken);
                        }
                    });
            }

        });
    }

    getUserById(userId) {
        return db.pg().one(User.query.findById, [userId]).then(result => User.convertRawData(result));
    }

    getUserByCellPhone(cellPhone) {
        return db.pg().one(User.query.findById, [cellPhone]).then(result => User.convertRawData(result));
    }
    // googleAuth(token){
    //     return new Promise((resolve, reject)=>{
    //         const GoogleAuth = require('google-auth-library');
    //         const auth = new GoogleAuth;
    //         const CLIENT_ID = require('./../../config/secrets').server_google_auth_client_id;
    //         const client = new auth.OAuth2(CLIENT_ID, '', '');
    //
    //
    //         client.verifyIdToken(
    //             token,
    //             CLIENT_ID,
    //             (e, login)=>{
    //                 if (e){
    //                     reject('google auth token is not valid');
    //                 }
    //                 else{
    //                     let payload = login.getPayload();
    //                     let sub = payload['sub'];
    //                     let addedUser;
    //
    //                     User.findUserIdBySub(sub).
    //                     then(userId => {
    //                         if (userId !== undefined){
    //                             User.findUserById(userId).
    //                             then(user => resolve({ token: jwt.sign({id: userId}, secrets.jwt), user: user}))
    //                         } else {
    //                             let newUser = new User({
    //                                 fname: payload['given_name'],
    //                                 lname: payload['family_name'],
    //                                 avatar: payload['picture'],
    //                                 email: payload['email']
    //                             });
    //
    //                             return newUser.saveToDatabase(db).
    //                             then(r => { addedUser = r; return User.addToGoogleAuth(addedUser._id, sub); }).
    //                             then(r => resolve({token: jwt.sign({id: addedUser._id}, secrets.jwt), user: addedUser})).
    //                             catch(error => {
    //                                 if (error.constraint === 'user_email_key') {
    //
    //                                     db.pg().
    //                                     one(User.query.findByEmail, newUser.email).
    //                                     then(foundedUser => { addedUser = foundedUser; return User.addToGoogleAuth(foundedUser._id, sub); }).
    //                                     then(r => resolve({token: jwt.sign({id: r.user_id}, secrets.jwt), user: addedUser}))
    //                                 } else
    //                                     throw error;
    //                                 }).
    //                             catch(error => reject(error));
    //                         }
    //                     })
    //                 }
    //             });
    //     });
    // }

    decodeGoogleTokenId(googleIdToken) {
        return new Promise((resolve, reject) => {
            const GoogleAuth = require('google-auth-library');
            const auth = new GoogleAuth;
            const CLIENT_ID = require('./../../config/secrets').server_google_auth_client_id;
            const client = new auth.OAuth2(CLIENT_ID, '', '');

            client.verifyIdToken(
                googleIdToken,
                CLIENT_ID,
                (e, login) => {
                    if (e) {
                        reject('invalid google_id_token');
                    }
                    else {
                        let payload = login.getPayload();
                        resolve(payload);
                    }
                });
        });
    }

    codeCellPhone(cellPhone) {
        return cellPhone;
        // return cellPhone.substr(0, 3) + "*****" + cellPhone.substr(8, cellPhone.length - 1);
    }

    googleAuth(googleIdToken) {
        return new Promise((resolve, reject) => {
            let googleUser;
            this.decodeGoogleTokenId(googleIdToken).
            then(_googleUser => {
                googleUser = _googleUser;
                return User.isSignUpWithGoogle(googleUser.sub)
            }).
            then(result => {
                if (result === false) {
                    reject('no cell_phone');
                } else {
                    return User.findUserByEmail(googleUser.email)
                }
            }).
            then(userObj => {
                let user = User.convertRawData(userObj);
                if (user.is_cell_phone_verified === true) {
                    user.removePrivateData();
                    resolve({token: jwt.sign({id: user._id}, secrets.jwt), user: user})
                } else {
                    this.sendSMSToken(user.cell_phone);
                    reject({ cellPhoneNotVerified: true, m: 'cell_phone not verified', cell_phone: this.codeCellPhone(user.cell_phone)})
                }
            }).
            catch(error => {
                if (error.code) {
                    reject(dbErrorParser(error));
                } else
                    reject(error)
            });
        });

    }

    googleAuthWithCellphone(googleIdToken, cellPhone) {
        return new Promise((resolve, reject) => {
            let googleUser;
            this.decodeGoogleTokenId(googleIdToken).
            then(_googleUser => {
                googleUser = _googleUser;
                return User.isSignUpWithGoogle(googleUser.sub)
            }).
            then(result => {
                if (result === false) {
                    let addedUser;
                    let newUser = new User({
                        fname: googleUser['given_name'],
                        lname: googleUser['family_name'],
                        avatar: googleUser['picture'],
                        email: googleUser['email'],
                        cell_phone: cellPhone
                    });

                    newUser.saveToDatabase(db).
                    then(r => {
                        addedUser = r;
                        return User.addToGoogleAuth(addedUser._id, googleUser.sub);
                    }).
                    then(r => {
                        this.sendSMSToken(cellPhone);
                        // resolve({
                        //     token: jwt.sign({id: addedUser._id}, secrets.jwt),
                        //     user: addedUser
                        // });
                        reject({ cellPhoneNotVerified: true, m: 'cell_phone not verified', cell_phone: this.codeCellPhone(addedUser.cell_phone)})
                    }).
                    catch(error => {
                        if (error.constraint === 'user_email_key') {
                            db.pg().one(User.query.findByEmail, newUser.email).
                            then(foundedUser => {
                                addedUser = foundedUser;
                                return User.addToGoogleAuth(foundedUser._id, googleUser.sub);
                            }).
                            then(r => resolve({token: jwt.sign({id: r.user_id}, secrets.jwt), user: addedUser}))
                        } else
                            throw error;
                    }).catch(error => {
                        if (error.code) {
                            reject(dbErrorParser(error));
                        } else
                            reject(error)
                    });
                } else {
                    User.findUserByEmail(googleUser.email).
                    then(userObj => {

                        let user = User.convertRawData(userObj);
                        if (user.is_cell_phone_verified === true) {
                            if (user.cell_phone !== cellPhone)
                                reject('unauthorized');
                            else
                                resolve({token: jwt.sign({id: user._id}, secrets.jwt), user: user})
                        } else {
                            this.sendSMSToken(user.cell_phone);
                            reject({ cellPhoneNotVerified: true, m: 'cell_phone not verified', cell_phone: this.codeCellPhone(user.cell_phone)})
                        }
                    })
                }
            }).catch(error => reject(error));
        });
    }

    generateSMSToken() {
        let min = 10000;
        let max = 99999;

        return Math.floor(Math.random() * (max - min) + min);
    }

    sendSMSToken(cellPhone) {
        let token = this.generateSMSToken();
        return User.saveSMSToken(cellPhone, token).then(result => smsController.sendUserAuthToken(cellPhone, token))
    }
}

module.exports = new UserController();