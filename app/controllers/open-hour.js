const BaseController = require('./basecontroller');
const OpenHour = require('./../models/open-hour');
const Promise = require('bluebird');

class OpenHourController extends BaseController{
    constructor(){
        super();
    }

    addRestaurant(restaurantId, param){
        let openHour = new OpenHour({restaurant_id: restaurantId, base: param.base, exceptions: param.exceptions});
        return openHour.saveToDb();
    }

    openHoursOfARestaurant(restaurantId, inputDate){
        return new Promise((resolve, reject)=>{
            OpenHour.openHoursOfARestaurant(restaurantId).
            then((result)=>{
                let openHour = OpenHour.convertRawData(result);
                openHour.processExceptions(inputDate);
                resolve(openHour.removeExtraFields());
            }).
            catch((error)=> reject(error));
        });
    }
}

module.exports = new OpenHourController();