const BaseController = require('./basecontroller');
const Banner = require('./../models/banner');
const db = require('./../database');

class BannerController extends BaseController{
    constructor(){
        super();
    }

    add(bannerObj){
        let banner = new Banner(bannerObj);
        return banner.add(db);
    }
}

module.exports = new BannerController();