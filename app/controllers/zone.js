const BaseController = require('./basecontroller');
const db = require('./../database');
const Zone = require('./../models/zone');
const Promise = require('bluebird');
const LocationResponse = require('../rest/responses/location');

class ZoneController extends BaseController{
    constructor(){
        super();
    }

    addZone(zoneObj){
        return new Promise((resolve, reject) =>{
            let zone = new Zone({name: zoneObj.name, slug: zoneObj.slug, long: zoneObj.long, lat: zoneObj.lat});
            let session = db.neo4jDriver().session();
            zone.addZone(session).
            then(result=>{
                session.close();
                resolve(result);
            }).
            catch((err)=>{
                session.close();
                reject(err);
            });
        });
    }

    connectZone(zoneSlug1, zoneSlug2){
        return new Promise((resolve, reject) =>{
            let session = db.neo4jDriver().session();
            Zone.connectZone(session, {slug:zoneSlug1}, {slug:zoneSlug2}).
            then(result=>{
                session.close();
                resolve(result);
            }).
            catch((err)=>{
                session.close();
                reject(err);
            });
        });

    }

    allZones(){
        let session = db.neo4jDriver().session();
        return new Promise((resolve, reject)=>{
            let zones = [];
            Zone.getAllZones(session).
            then((result)=> Promise.each(result.records, (record)=>zones.push(Zone.convertRawData(record)))).
            then(()=>{
                session.close();
                resolve(zones);
            }).
            catch((error)=>{
                session.close();
                reject(error);
            });
        });

    }

    getNeighbours(zoneSlug){
        return new Promise((resolve, reject)=>{
            let result = [];

            let session = db.neo4jDriver().session();
            Zone.getNeighbourZones(session, zoneSlug).
            then((output)=> {
                let flag = true;        // Used for one in between
                return Promise.each(output.records, ((record) => {
                    if (flag) result.push(Zone.convertRawData(record));
                    flag = !flag;
                }));
            }).then(()=>{
                resolve(result);
                session.close();
            }).catch((error)=>{
                reject(error);
                session.close();
            });
        });
    }

    /**
     * Get the nearest location to the given position
     * @param position an object containing long & lat field
     */
    nearestZone(position){
        // return new Promise((resolve, reject)=>{
        //     this.allZones().then((zones)=>{
        //         zones.sort((a, b)=>{
        //             let distance1 = Math.sqrt(Math.pow(position.long - a.long, 2) + Math.pow(position.lat - a.lat, 2));
        //             let distance2 = Math.sqrt(Math.pow(position.long - b.long, 2) + Math.pow(position.lat - b.lat, 2));
        //             if(distance1 > distance2) return 1;
        //             else if (distance2 > distance1) return -1;
        //             else return 0;
        //         });
        //         resolve(new LocationResponse(zones[0]))
        //     }).catch((error)=>reject(new LocationResponse(null, error)));
        // });

        let session = db.neo4jDriver().session();
        // let zone;
        let zone = [];
        return new Promise((resolve, reject)=>{
            Zone.nearestZone(session, position).
            then((result)=> Promise.each(result.records, (record)=>zone = Zone.convertRawData(record))).
            // then((result)=> Promise.each(result.records, (record)=>zone.push(record))).
            then(()=>{
                session.close();
                resolve(zone);
            }).
            catch((error)=>{
                session.close();
                reject(error);
            });
        });
    }

    searchByName(zoneName){
        return new Promise((resolve, reject)=>{
            let session = db.neo4jDriver().session();
            let zones = [];
            let zoneNames = zoneName.split(' ').filter(i => i);
            Zone.mostSimilarNamesZones(session, zoneNames).
            then((results)=>{
                session.close();
                return Promise.each(results.records, (record)=> zones.push(Zone.convertRawData(record)));
            }).
            then(()=>resolve(zones)).
            catch((error)=>{
                session.close();
                reject(error);
            });
        });
    }
}

module.exports = new ZoneController();