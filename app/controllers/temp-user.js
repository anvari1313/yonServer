const BaseController = require('./basecontroller');
const Promise = require('bluebird');
const TempUser = require('./../models/temp-user');

class TempUserController extends BaseController{
    constructor(){
        super();
    }

    'new'(userObj){
        let tempUser = new TempUser({ name: userObj.name, cell_phone: userObj.cell_phone});

        return tempUser.save();
    }

    findById(tempUserId){
        return TempUser.findById(tempUserId);
    }
}

module.exports = new TempUserController();