const BaseController = require('./basecontroller');
const Table = require('./../models/table');
const Map = require('./../models/map');

class TableController extends BaseController{
    constructor(){
        super();
    }

    addOne(restaurantId, mapId, properties){
        let table = new Table({
            restaurant_id: restaurantId,
            name: properties.name,
            capacity: properties.capacity,
            shape_type: properties.shape_type,
            x: properties.x,
            y: properties.y,
            angle: properties.angle
        });
        return table.addOne(mapId);
    }

    addMany(restaurantId, mapId, tablesProperties){
        return Table.addMany(restaurantId, mapId, tablesProperties);
    }

    getTable(tableId){
        return Table.getTable(tableId);
    }

    isValidTable(tableId){
        return Table.isValidTable(tableId);
    }

    getMap(tableId){
        return Map.searchForTableWithId(tableId);
    }
}

module.exports = new TableController();