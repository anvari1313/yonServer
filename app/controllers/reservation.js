const BaseController = require('./basecontroller');
const tableController = require('./table');
const smsController = require('./sms');
const restaurantController = require('./restaurant');
const userRequest = require('./../rest/fcm/user-request');
const tempUserController = require('./temp-user');
const Reservation = require('./../models/reservation');
const Restaurant = require('./../models/restaurant');
const db = require('./../database');
const errors = require('../rest/responses/codes');
const Promise = require('bluebird');
const moment = require('moment-jalaali');
const staticFilePath = require('./../../config/static-file').restaurantImages;
const RestaurantRequest = require('./../socket/request/restaurant-request');
const User = require('./../models/user');

class ReservationController extends BaseController {
    'new'(reservation) {
        let reservationObj;
        if (reservation.table_id) {
            reservationObj = new Reservation({
                user_id: reservation.userId,
                datetime: reservation.datetime,
                guest_count: reservation.guest_count,
                table_id: reservation.table_id,
                restaurant_id: reservation.restaurant_id,
                temp_user_id: reservation.temp_user_id,
                description: reservation.description
            });
        } else {
            reservationObj = new Reservation({
                user_id: reservation.userId,
                datetime: reservation.datetime,
                guest_count: reservation.guest_count,
                restaurant_id: reservation.restaurant_id,
                temp_user_id: reservation.temp_user_id,
                description: reservation.description
            });
        }

        return reservationObj.saveToDatabase(db);
    }

    newWithTempUser(reservationObj, userObj){
        let reservation = new Reservation(reservationObj);
        let _userObj;
        
        return tempUserController.new(userObj).
        then(user => {
            _userObj = user;
            reservationObj.temp_user_id = user._id;
            let reservation = new Reservation(reservationObj);
            return reservation.saveToDatabase(db)
        }).
        then(registeredReservation => {
            _userObj.lname = _userObj.name;
            registeredReservation.user = _userObj;
            return Promise.resolve(registeredReservation)
        });
    }

    isReservationValidAfterMax(restaurantId){
        return Reservation.isValidReservationAfterMax(db.pg(), restaurantId);
    }

    isReservationValid() {
        // TODO Check the reservation
    }

    reservationsOfADay(restaurantId, dateTimestamp){
        return Reservation.reservationsOfADay(db.pg(), restaurantId, dateTimestamp);
    }

    reservationsOfATime(restaurantId, dateTimestamp){
        return new Promise((resolve, reject)=>{
            let isValidResAfterMax;
            let maxResTime;
            this.isReservationValidAfterMax(restaurantId).
            then((result)=>{
                isValidResAfterMax = result.is_valid_res_after_max;
                console.log('IS RESERVATION VALID AFTER MAX: ' + isValidResAfterMax);
                maxResTime = result.max_table;
                let tableReservedPromise;
                if (isValidResAfterMax === true){
                    console.log('Getting allReservationsIntersectsWithTime');
                    tableReservedPromise = Reservation.allReservationsIntersectsWithTime(db.pg(), restaurantId, dateTimestamp, maxResTime);
                }else {
                    console.log('Getting reservationsOfADay');
                    tableReservedPromise = Reservation.reservationsOfADay(db.pg(), restaurantId, dateTimestamp);
                }
                return tableReservedPromise;
            }).
            then( result => resolve(result)).
            catch( error =>reject(error));
        });
    }

    hasReservation(userId){
        return new Promise((resolve, reject)=>{
            db.pg().one(Reservation.query.numberOfReservationsOfUser, [userId]).
            then((result)=>{
                if (result.count != 0)
                    resolve(result);
                else
                    reject('user has no reservation');
            }).catch((error)=>reject(error));
        });
    }

    getAllReservations(restaurantId, timeObj) {
        return new Promise((resolve, reject) => {
            let reservations = [];
            let queryForSignedUser;
            let queryForAnonymousUser;
            if (timeObj.start && timeObj.end){
                queryForSignedUser = db.pg().any(Reservation.query.reservationsOfRestaurantBetweenTwoTimestamp, [restaurantId, timeObj.start, timeObj.end]);
                queryForAnonymousUser = db.pg().any(Reservation.query.reservationsOfRestaurantBetweenTwoTimestampAnonymousUser, [restaurantId, timeObj.start, timeObj.end]);
            }
            else {
                queryForSignedUser = db.pg().any(Reservation.query.reservationsOfRestaurant, [restaurantId]);
                queryForAnonymousUser = db.pg().any(Reservation.query.reservationsOfRestaurantAnonymousUser, [restaurantId]);
            }
            queryForSignedUser.
            then((results) => Promise.each(results, (result) => reservations.push(Reservation.convertRawDataWithUser(result)))).
            then(() => queryForAnonymousUser).
            then((results) => Promise.each(results, (result) => reservations.push(Reservation.convertRawDataWithUser(result)))).
            then(() => resolve(reservations)).
            catch((error) => reject(error));
        });
    }

    getAllReservationsOfAUser(userId){
        return new Promise((resolve, reject)=>{
            let reservationsWithRestaurant = [];

            db.pg().any(Reservation.query.pendingReservationsOfUser, [userId]).
            then(results => Promise.each(results, (result)=> {
                let reservation = Reservation.convertRawData(result);
                reservation.restaurant = Restaurant.convertPathToAbsolute(Restaurant.convertRawData(result));
                reservationsWithRestaurant.push(reservation)
            })).
            then(() => resolve(reservationsWithRestaurant)).
            catch(error => reject(error));
        });
    }

    getReservationsOfAUserOfRestaurant(userId, restaurantId){
        return new Promise((resolve, reject)=>{
            let reservationsWithRestaurant = [];

            db.pg().any(Reservation.query.pendingReservationsOfUserOfRestaurant, [userId, restaurantId]).
            then(results => Promise.each(results, (result)=> {
                let reservation = Reservation.convertRawData(result);
                reservation.restaurant = Restaurant.convertRawData(result);
                reservationsWithRestaurant.push(reservation)
            })).
            then(() => resolve(reservationsWithRestaurant)).
            catch(error => reject(error));
        });
    }

    checkIfReservable(params) {
        return new Promise((resolve, reject)=>{
            Reservation.checkIfReservable(db.pg(), params).
            then((result)=> {console.log(result); if (result.isReservationValid) resolve(); else reject(errors.reservationNotAllowed); }).catch((error)=>reject(error));
        });
    }

    cancelReservationByUser(reservationId, userId){
        return new Promise((resolve, reject)=>{
            Reservation.isReservationOwnToUser(reservationId, userId).
            then(result => Reservation.cancelReservation(reservationId)).
            then((reservation) => { RestaurantRequest.cancelReservation(reservation.restaurant_id, reservationId); resolve(); }).
            catch(error => { console.log(error); reject('reservation not owned to user'); });
        });
    }

    makeupObjectForReservationCancelForFCM(reservation){
        let resultObj = {};
        resultObj.reservation_id = reservation._id;
        resultObj.reservation_user_id = reservation.user_id;
        resultObj.reservation_datetime = /* moment(new Date(reservation.datetime)).unix()*/ reservation.datetime;
        resultObj.reservation_guest_count = reservation.guest_count;
        return restaurantController.find(reservation.restaurant_id).
        then(restaurant => {
            resultObj.restaurant_name = restaurant.name;
            resultObj.restaurant_avatar = restaurant.avatar;
            return Promise.resolve(resultObj);
        });
    }

    cancelReservationByRestaurant(reservationId, restaurantId){
        let _preparedReservationObjForFCM;
        let _reservation;

        return new Promise((resolve, reject) => {
            let isTempUserReservation;

            Reservation.isReservationOwnToRestaurant(reservationId, restaurantId).
            then(result => Reservation.cancelReservation(reservationId)).
            then(reservation => {
                _reservation = reservation;
                isTempUserReservation = !_reservation.user_id;
                return this.makeupObjectForReservationCancelForFCM(_reservation)}).
            then(preparedReservationObjForFCM => {
                _preparedReservationObjForFCM = preparedReservationObjForFCM;
                if (isTempUserReservation) {
                    return tempUserController.findById(_reservation.temp_user_id).
                    then(userObj => smsController.sendUserReservationCancel(userObj.cell_phone, _preparedReservationObjForFCM)).
                    then(() => resolve()).
                    catch(error => reject(error))
                } else {
                    return User.findUserById(_reservation.user_id).
                    then(userObj =>
                        Promise.all([
                            userRequest.sendCancelReservationToUser(_preparedReservationObjForFCM.reservation_user_id, _preparedReservationObjForFCM),
                            smsController.sendUserReservationCancel(userObj.cell_phone, _preparedReservationObjForFCM)
                        ])).
                    then(() => resolve()).
                    catch(error => reject(error))
                }
            })

        });
    }

    assignTable(reservationId, tableId){
        return Reservation.assignTable(reservationId, tableId);
    }
}

module.exports = new ReservationController();