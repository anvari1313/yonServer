const BaseController = require('./basecontroller');
const Promise = require('bluebird');
const Eatable = require('./../models/eatable');

class EatableController extends BaseController {
    constructor() {
        super();
    }

    addEatable(restaurantId, properties){
        let eatable = new Eatable({
            name: properties.name,
            price: properties.price,
            restaurant_id: restaurantId,
            tags: properties.tags,
            picture_album: properties.picture_album,
            ingredients: properties.ingredients,
            user_review: properties.user_review,
            featured_picture: properties.featured_picture
        });

        return eatable.save();
    }

    addManyEatables(restaurantId, eatables){
        let modifiedEatables = [];
        return Promise.each(eatables, eatable => { eatable.restaurant_id = restaurantId; modifiedEatables.push(eatable); }).
        then(() => Eatable.addMany(modifiedEatables))
    }
}

module.exports = new EatableController();