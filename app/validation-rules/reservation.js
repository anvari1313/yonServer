const moment = require('moment');
const validate = require('validate.js');
const validationFailCodes = require('../rest/responses/codes').validationFailCodes;

let cellPhoneValidation = {
    numericality: {
        onlyInteger: true,
        greaterThan: 0,
        message: {
            code: validationFailCodes.notValidCellphone.code,
            message: validationFailCodes.notValidCellphone.message
        },
    },
    length: {
        minimum: 11,
        tooShort: {
            code: validationFailCodes.tooShort.code,
            message: validate.format(validationFailCodes.tooShort.message, {count: 11}),
            minimum: 11
        }
    }
};

let requiredCellPhoneValidation = JSON.parse(JSON.stringify(cellPhoneValidation));


requiredCellPhoneValidation.presence = {
    message: {
        code: validationFailCodes.blankField.code,
        message: validationFailCodes.blankField.message,
    }
};

module.exports.getAllOfDay = {
    date:{
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    }
};

module.exports.getAllOfDayInATime = {
    datetime:{
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    }
};

module.exports.newWithTableFromUser = {
    datetime: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    guest_count: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            onlyInteger:true,
            greaterThan: 0,
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    table_id:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    description:{}
};

module.exports.newWithoutTableFromUser = {
    datetime: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    guest_count: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            onlyInteger:true,
            greaterThan: 0,
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    description:{}
};

module.exports.newFromRestaurant = {
    restaurant_id: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    datetime: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    guest_count: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            onlyInteger:true,
            greaterThan: 0,
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    table_id:{},
    user_name: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
    },
    user_cell_phone: requiredCellPhoneValidation,
    description:{}
};

module.exports.cancel = {
    reservation_id: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    }
};