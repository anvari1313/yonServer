const validationFailCodes = require('../rest/responses/codes').validationFailCodes;
const validate = require('validate.js');

validate.validators.eatableValidator = value => {
    return new validate.Promise((resolve, reject) => {
        if (Array.isArray(value)) {
            for(let eatable in value){
                // Todo check the owning each eatable by restaurant
            }
            resolve();
        } else
            reject({eatables: validationFailCodes.notArrayType});
    });
};

module.exports.new = {
    submit_datetime: {
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    user_id:{

    },
    eatables: {
        eatableValidator: true
    },
    reservation_id:{},
    table_id:{},
    transaction_id:{},
    preparation_status:{},
    is_gone:{},
    order_pnum: {},
    is_table_unknown: {},
    is_takeaway: {},
    description:{},
};

module.exports.validate = validate;