const validate = require('validate.js');
const validationFailCodes = require('../rest/responses/codes').validationFailCodes;

module.exports.add = {
    rate: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            greaterThanOrEqualTo: 0,
            lessThanOrEqualTo: 5,
            message: {
                message: validate.format(validationFailCodes.notValidOrNotInRangeNumber.message, {min: 0, max: 5}),
                code: validationFailCodes.notValidOrNotInRangeNumber.code
            },
        }
    },

    text:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    }
};