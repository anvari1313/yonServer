/**
 * Created by ahmad on 6/1/17.
 */
const validate = require('validate.js');
const validationFailCodes = require('../rest/responses/codes').validationFailCodes;

let cellPhoneValidation = {
    numericality: {
        onlyInteger: true,
        greaterThan: 0,
        message: {
            code: validationFailCodes.notValidCellphone.code,
            message: validationFailCodes.notValidCellphone.message
        },
    },
    length: {
        minimum: 11,
        tooShort: {
            code: validationFailCodes.tooShort.code,
            message: validate.format(validationFailCodes.tooShort.message, {count: 11}),
            minimum: 11
        }
    }
};

let requiredCellPhoneValidation = JSON.parse(JSON.stringify(cellPhoneValidation));


requiredCellPhoneValidation.presence = {
    message: {
        code: validationFailCodes.blankField.code,
        message: validationFailCodes.blankField.message,
    }
};

module.exports.login = {
    email: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        email: {
            message: {
                code: validationFailCodes.invalidEmail.code,
                message: validationFailCodes.invalidEmail.message
            }
        },
    },
    password: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        length: {
            minimum: 6,
            tooShort: {
                code: validationFailCodes.tooShort.code,
                message: validate.format(validationFailCodes.tooShort.message, {count: 6}),
                minimum: 6
            }
        },
    }
};

module.exports.tokenLogin = {
    sms_token: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    cell_phone: requiredCellPhoneValidation
};

module.exports.googleAuth = {
    id_token: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    cell_phone: cellPhoneValidation
};

module.exports.signup = {
    fname: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    lname: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    birthdate: {
        datetime: {
            dateOnly: true,
            message: {
                code: validationFailCodes.notDate.code,
                message: validationFailCodes.notDate.message,
            }
        }
    },
    email: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        email: {
            message: {
                code: validationFailCodes.invalidEmail.code,
                message: validationFailCodes.invalidEmail.message
            }
        },
    },
    cell_phone: requiredCellPhoneValidation,
    password: {
        length: {
            minimum: 6,
            tooShort: {
                code: validationFailCodes.tooShort.code,
                message: validate.format(validationFailCodes.tooShort.message, {count: 6}),
                minimum: 6
            }
        },
    }
};

module.exports.device = {
    new: {
        new_fcm_token: {
            presence: {
                message: {
                    code: validationFailCodes.blankField.code,
                    message: validationFailCodes.blankField.message,
                }
            }
        }
    },
    update: {
        old_fcm_token: {
            presence: {
                message: {
                    code: validationFailCodes.blankField.code,
                    message: validationFailCodes.blankField.message,
                }
            }
        },
        new_fcm_token: {
            presence: {
                message: {
                    code: validationFailCodes.blankField.code,
                    message: validationFailCodes.blankField.message,
                }
            }
        }
    }
};