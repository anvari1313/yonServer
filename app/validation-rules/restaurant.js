/**
 * Created by amirhosein on 5/18/17.
 */
const validate = require('validate.js');
const validationFailCodes = require('../rest/responses/codes').validationFailCodes;

module.exports.login = {
    email: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        email: {
            message: {
                code: validationFailCodes.invalidEmail.code,
                message: validationFailCodes.invalidEmail.message
            }
        },
    },
    password: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        length: {
            minimum: 6,
            tooShort: {
                code: validationFailCodes.tooShort.code,
                message: validate.format(validationFailCodes.tooShort.message, {count: 6}),
                minimum: 6
            }
        },
    }
};

module.exports.localSignUp = {
    email: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        email: {
            message: {
                code: validationFailCodes.invalidEmail.code,
                message: validationFailCodes.invalidEmail.message
            }
        },
    },
    password: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        length: {
            minimum: 6,
            tooShort: {
                code: validationFailCodes.tooShort.code,
                message: validate.format(validationFailCodes.tooShort.message, {count: 6}),
                minimum: 6
            }
        },
    }
};