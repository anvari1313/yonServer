const validationFailCodes = require('../rest/responses/codes').validationFailCodes;

module.exports.addMap = {
    name:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    width:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    height:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    }
};

module.exports.removeMap = {
    _id:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    }
};


module.exports.addTable = {
    name:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    capacity:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    x:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    y:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    shape_type:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
    },
    angle:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },

    map_id:{
        presence:{
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    }

};

module.exports.addManyTable = {
    name:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    },
    capacity:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    x:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    y:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
    shape_type:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
    },
    angle:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality:{
            message:{
                code:validationFailCodes.notNumber.code,
                message:validationFailCodes.notNumber.message
            }
        }
    },
};


module.exports.getTable = {
    table_id:{
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        }
    }
};
