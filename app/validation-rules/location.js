const validationFailCodes = require('../rest/responses/codes').validationFailCodes;

module.exports.searchByLongLat = {
    long: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    },
    lat: {
        presence: {
            message: {
                code: validationFailCodes.blankField.code,
                message: validationFailCodes.blankField.message,
            }
        },
        numericality: {
            message: {
                code: validationFailCodes.notNumber.code,
                message: validationFailCodes.notNumber.message
            }
        }
    }
};