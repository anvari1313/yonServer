const Model = require('./basemodel');

class Banner extends Model{
    constructor(banner){
        super();
        this.title = banner.title;
        this.subtitle = banner.subtitle;
        this.image = banner.image;
        this.owner = banner.owner;
        this.type = -1;
        if(banner.taget_url) {
            this.type = 0;
            this.taget_url = banner.taget_url;
        }
        if(banner.target_id){
            this.type = 1;
            this.target_id = banner.target_id;
        }
        if(banner.target_list_id){
            this.type = 2;
            this.target_list_id = banner.target_list_id;
        }
    }

    add(db){
        return db.mongodb().collection('banners').insertOne(this);
    }
}

module.exports = Banner;