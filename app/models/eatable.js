const Model = require('./basemodel');
const staticFilePath = require('./../../config/static-file').eatbleImages;
const mongo_find_promise = require('./../utils/mongo-find-promise');
const Promise = require('bluebird');
const db = require('./../database');
const ObjectId = require('mongodb').ObjectId;

class Eatable extends Model{
    constructor(modelObj){
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.name) this.name = modelObj.name;
        if (modelObj.price) this.price = modelObj.price;
        if (modelObj.restaurant_id) this.restaurant_id = modelObj.restaurant_id;
        this.tags = modelObj.tags || [];
        this.picture_album = modelObj.picture_album || [];
        this.ingredients = modelObj.ingredients || [];
        this.user_review = modelObj.user_review || [];
        if (modelObj.featured_picture ) this.featured_picture  = modelObj.featured_picture;
    }

    static get COLLECTION_NAME(){ return 'eatables'; }

    static convertPathToAbsolute(that){
        let newPictureAlbum = [];

        if (that.featured_picture) that.featured_picture = staticFilePath.featureImage.url + that.featured_picture;
        for (let picture_album_item of that.picture_album){
            newPictureAlbum.push(staticFilePath.album.url + picture_album_item);
        }
        that.picture_album = newPictureAlbum;
        return that;
    }

    static convertRawData(rawData){
        let eatable = new Eatable({
            _id: rawData._id,
            name: rawData.name,
            price: rawData.price,
            restaurant_id: rawData.restaurant_id,
            tags: rawData.tags,
            picture_album: rawData.picture_album,
            ingredients: rawData.ingredients,
            user_review: rawData.user_review,
            featured_picture: rawData.featured_picture
        });

        return eatable;
    }

    static addMany(eatables){
        return db.mongodb().collection(Eatable.COLLECTION_NAME).insertMany(eatables);
    }

    save(){
        if (this._id){

        } else {
            return db.mongodb().collection(Eatable.COLLECTION_NAME).insertOne(this);
        }
    }
}

module.exports = Eatable;