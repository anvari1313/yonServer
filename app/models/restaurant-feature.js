/**
 * Created by amirhosein on 4/12/17.
 */
const Model = require('./basemodel');

class RestaurantFeature extends Model {
    constructor(id, name, slug, valueType, validValues, description) {
        super();

        this.id = id || -1;
        this.name = name || '';
        this.slug = slug || '';
        this.valueType = valueType || '';
        this.validValues = validValues || {};
        this.description = description || '';

        this.value = null;
    }

    static convertRawData(obj) {

    }
}

module.exports = RestaurantFeature;