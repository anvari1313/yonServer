const Model = require('./basemodel');
const db = require('./../database');
const Restaurant = require('./restaurant');

class RestaurantList extends Model{
    constructor(modelObj){
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.name) this.name = modelObj.name;
        if (modelObj.description) this.description = modelObj.description;
        if (modelObj.list) this.list = modelObj.list;
    }


    static convertRawData(rawObj){

    }

    static getListWithId(listId){
        return db.pg().one(RestaurantList.query.findListById, [listId]);
    }

    static getListMemberWithId(listId){
        return db.pg().any(RestaurantList.query.findRestaurantListById, [listId]);
    }

    static get query(){
        let fields = Restaurant.FULL_RESTAURANT_FIELDS;
        return {
            findListById: 'SELECT * FROM restaurants_lists WHERE _id = $1',
            findRestaurantListById: 'SELECT ' + fields + ' FROM restaurants_lists_relations INNER JOIN restaurants ON (restaurants_lists_relations.restaurant_id = restaurants._id) WHERE restaurants_lists_relations.list_id = $1',
        };
    }
}

module.exports = RestaurantList;