const Promise = require('bluebird');
const bcrypt = require('bcrypt');
const staticFilePath = require('./../../config/static-file').restaurantImages;
const db = require('./../database');
const util = require('util');
const moment = require('moment');
const Model = require('./basemodel');

/**
 * _id          -> integer
 * name         -> varchar(120)
 * branch       -> varchar(120)
 * address      -> text
 * location     -> point
 * point        -> integer
 * cash         -> bigint
 * email        -> citext
 * password     -> text
 * info         -> json
 * max_table    -> integer
 * price        -> smallint
 * rating       -> double
 * description  -> text
 */
class Restaurant extends Model {

    constructor(modelObj) {
        super();

        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.name) this.name = modelObj.name;
        if (modelObj.branch) this.branch = modelObj.branch;
        if (modelObj.address) this.address = modelObj.address;
        if (modelObj.location) this.location = modelObj.location;
        if (modelObj.point) this.point = modelObj.point;
        if (modelObj.cash) this.cash = Number(modelObj.cash);
        if (modelObj.email) this.email = modelObj.email;
        if (modelObj.info) this.info = modelObj.info || {};
        if (modelObj.max_table) this.max_table = modelObj.max_table;
        if (modelObj.price_rate) this.price_rate = parseInt(modelObj.price_rate) || 0;
        if (modelObj.rating) this.rating = modelObj.rating || 0.0;
        if (modelObj.avatar) this.avatar = modelObj.avatar;
        if (modelObj.password) this._password = modelObj.password;
        if (modelObj.tags) this.tags = modelObj.tags;
        if (modelObj.distance) this.distance = modelObj.distance;
        if (modelObj.zone_slug) this.zone_slug = modelObj.zone_slug;
        if (modelObj.zone_name) this.zone_name = modelObj.zone_name;
    }

    getTags(pg) {
        const Tag = require('./tag');

        let self = this;
        return new Promise(function (resolve, reject) {
            try {
                if (self._tags) {
                    resolve(self._tags);
                } else {
                    pg.any(Restaurant.query().getTags, [self.id]).then(data => Tag.convert(data)).then(tags => self._tags = tags).then(x => resolve(self._tags)).catch(reject);
                }
            } catch (exp) {
                reject(exp);
            }
        });
    }

    static findByCellphoneAndTokenRespectExpiration(cellPhone, smsToken){
        let now = moment().unix();
        let expiration = 15 * 60;
        return db.pg().one(Restaurant.query().findRestaurantByCellPhoneAndSMSToken, [cellPhone, smsToken, now, expiration])
    }

    comparePassword(_password) {
        let self = this;
        return Promise.resolve(_password).then(password => bcrypt.compare(password, self._password));
    }

    static convertRawData(obj) {
        return new Restaurant({
            _id: obj.restaurant_id || obj._id,
            name: obj.name,
            branch: obj.branch,
            address: obj.address,
            location: obj.location,
            point: obj.point,
            cash: obj.cash,
            email: obj.email,
            password: obj.password,
            info: obj.info,
            price_rate: obj.price_rate,
            tags: obj.tags,
            avatar: obj.avatar,
            max_table: obj.max_table,
            rating: obj.rating,
            zone_slug: obj.zone_slug,
            zone_name: obj.zone_name,
            distance: obj.distance
        });
    }

    static convertPathToAbsolute(obj){

        if (obj.avatar)
        {
            obj.avatar = staticFilePath.avatar.url + obj.avatar;
            console.log('Log start');
            console.log(obj.avatar);
            console.log('Log start');
        }

        // console.log('This is logged');
        // console.log(obj.avatar);
        // console.log('That is logged');
        // if (obj.avatar) {
        //     obj.avatar = "http://159.89.81.146/restaurant/avatar/1665.jpg";
        //     console.log(obj.avatar);
        // }
        return obj;
    }

    static hashPassword(password) {
        const saltRounds = 10;
        return bcrypt.hash(password, saltRounds)
    }

    static get FULL_RESTAURANT_FIELDS(){
        return ' restaurants._id, name, branch, address, price AS price_rate, rating, cached_tags AS tags, avatar, zone_slug, zone_name';
    }

    static get MID_RESTAURANT_FIELDS(){
        return ' restaurants._id, name, branch, address, price AS price_rate, rating, avatar, zone_slug, zone_name';
    }

    static query(isMinifiedFeilds) {
        let fieldsOfRestaurant;
        if (isMinifiedFeilds === true)
            fieldsOfRestaurant = Restaurant.MID_RESTAURANT_FIELDS;
        else
            fieldsOfRestaurant = Restaurant.FULL_RESTAURANT_FIELDS;
        return {
            emails: 'SELECT email from restaurants;',
            all: 'select * from restaurants',
            getAll: 'SELECT ' + fieldsOfRestaurant + ' FROM restaurants',
            getAllOffseted : 'SELECT ' + fieldsOfRestaurant + ' FROM restaurants',
            nearestZones: 'SELECT * FROM restaurants',
            findById: 'select * from restaurants where _id = $1',
            findByIdInformation: 'SELECT _id, name, branch, avatar, rating, address, price AS price_rate, location, info, description, zone_slug, zone_name FROM restaurants WHERE _id = $1',
            findByEmail: 'select * from restaurants where email = $1',
            getTags: 'select tags.* \n'
            + 'from restaurants \n'
            + 'join tags_exists on (restaurants._id = tags_exists.restaurant_id) \n'
            + 'join tags on (tags_exists.tag_id = tags._id) \n'
            + 'where (restaurants._id = $1)',
            findInfoByZoneSlug: 'SELECT DISTINCT ON (_id)' + fieldsOfRestaurant + ' FROM restaurants WHERE zone_slug = $1',
            findInfoByZoneSlugWithDistance: 'SELECT ' + fieldsOfRestaurant + ', ',
            findInfoByTag: 'SELECT DISTINCT ON (_id) ' + fieldsOfRestaurant + ' FROM (\n' +
            '\tSELECT selected_tags.tag_id, restaurant_id FROM \n' +
            '\t\t(SELECT _id AS tag_id FROM tags WHERE slug = ALL ($1::character varying[])) AS selected_tags\n' +
            '\t\t\tINNER JOIN tags_exists ON (selected_tags.tag_id = tags_exists.tag_id)) AS restaurants_tags INNER JOIN restaurants ON (restaurants._id = restaurants_tags.restaurant_id);',
            findInfoByTagWithDistance: 'SELECT * FROM (SELECT DISTINCT ON (_id) ' + fieldsOfRestaurant + ',location<@>\'($2,$3)\'::point as distance FROM (\n' +
            '\tSELECT selected_tags.tag_id, restaurant_id FROM \n' +
            '\t\t(SELECT _id AS tag_id FROM tags WHERE slug = ALL ($1::character varying[])) AS selected_tags\n' +
            '\t\t\tINNER JOIN tags_exists ON (selected_tags.tag_id = tags_exists.tag_id)) AS restaurants_tags INNER JOIN restaurants ON (restaurants._id = restaurants_tags.restaurant_id)) AS results ORDER BY results.distance;',
            saveSMSAuthToken:'UPDATE restaurants SET (auth_sms_token, auth_sms_token_generated_at) = ($2, to_timestamp($3)::timestamp without time zone) WHERE cell_phone = $1 RETURNING *;',
            findRestaurantByCellPhoneAndSMSToken: 'SELECT ' + fieldsOfRestaurant + ' FROM restaurants WHERE $1 = cell_phone AND $2 = auth_sms_token AND $3 - EXTRACT(EPOCH FROM auth_sms_token_generated_at) < $4;'
        };
    }

    static searchByNameOrBranch(nameLikes){
        let query = util.format(' WHERE (name LIKE %s OR branch LIKE %s)', '\'%' + nameLikes[0] + '%\'', '\'%' + nameLikes[0] + '%\'');
        for (let i = 1; i < nameLikes.length; i++){
            let ro = util.format(' AND (name LIKE %s OR branch LIKE %s)', '\'%' + nameLikes[i] + '%\'', '\'%' + nameLikes[i] + '%\'');
            query += ro;
        }
        return db.pg().any(Restaurant.query(true).getAll + query);
    }

    static findById(pg, restaurantId) {
        return pg.one(Restaurant.query().findByIdInformation, [restaurantId]).
        then(restaurantObj => Promise.resolve(this.convertPathToAbsolute(restaurantObj)));
    }

    static hashPassword(password) {
        const saltRounds = 10;
        return bcrypt.hash(password, saltRounds)
    }

    static isValid(pg, restaurantId) {
        return new Promise((resolve, reject) => {
            pg.one(Restaurant.query().findById, [restaurantId]).then(result => resolve(result)).catch((error) => reject('Not a valid restaurant'));
        });
    }

    static searchByZone(zoneSlug){
        return db.pg().any(Restaurant.query().findInfoByZoneSlug, [zoneSlug])
    }

    static searchByZoneDistanceSorted(zoneSlug, location){
        return db.pg().any(Restaurant.query().findInfoByZoneSlugWithDistance, [zoneSlug, location])
    }

    static searchByTag(tags){
        return db.pg().any(Restaurant.query().findInfoByTag, [tags]);
    }

    static _allRestaurants(){
        return db.pg().any(Restaurant.query().getAllOffseted);
    }

    static searchByTagDistanceSorted(tags, location){
        return db.pg().any(Restaurant.query().findInfoByTagWithDistance, [tags, parseFloat(location.x), parseFloat(location.y)]);
    }

    static getEmail(){
        return db.pg().any(Restaurant.query().emails);
    }

    static saveSMSToken(cellPhone, token){
        let now = moment().unix();
        return db.pg().one(Restaurant.query().saveSMSAuthToken, [cellPhone, token, now]);
    }
}

module.exports = Restaurant;