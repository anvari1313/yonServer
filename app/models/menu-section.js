const Model = require('./basemodel');
const mongo_find_promise = require('./../utils/mongo-find-promise');
const Promise = require('bluebird');
const Eatable = require('./eatable');
const db = require('./../database');
const ObjectId = require('mongodb').ObjectId;

class MenuSection extends Model{
    constructor(modelObj){
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.name) this.name = modelObj.name;
        if (modelObj.restaurant_id) this.restaurant_id = modelObj.restaurant_id;
        if (modelObj.show_order) this.show_order = modelObj.show_order;
        if (modelObj.eatables) this.eatables = modelObj.eatables; else this.eatables = [];
    }

    static get COLLECTION_NAME(){ return 'menu_sections'; }

    static convertRawData(rawData){
        let menuSection = new MenuSection({
            _id: rawData._id,
            name: rawData.name,
            restaurant_id: rawData.restaurant_id,
            show_order: rawData.show_order,
            eatables: rawData.eatables
        });


        let convertedEatables = [];

        for (let i = 0; i <  menuSection.eatables.length; i++){
            let convertedEatable = Eatable.convertRawData(menuSection.eatables[i]);
            convertedEatables.push(convertedEatable);
        }

        menuSection.eatables = convertedEatables;


        return menuSection;
    }

    static getOfRestaurant(mongodb, restaurantId){
        return mongo_find_promise.sortedFind(mongodb.collection(MenuSection.COLLECTION_NAME), {restaurant_id: restaurantId}, {show_order: 1});
    }

    static findSection(sectionId){
        return mongo_find_promise.findOne(db.mongodb().collection(MenuSection.COLLECTION_NAME), {_id: ObjectId(sectionId)});
    }

    save(){
        if (this._id){
            this._id = ObjectId(this._id);
            return db.mongodb().collection(MenuSection.COLLECTION_NAME).updateOne({_id: this._id}, {$set: this})
        }else{
            return db.mongodb().collection(MenuSection.COLLECTION_NAME).insertOne({
                name: this.name,
                restaurant_id: this.restaurant_id,
                show_order: this.show_order,
                eatables: this.eatables
            });
        }

    }

    addEatables(eatables){
        for(let i = 0; i < eatables.length; i ++) this.eatables.push(eatables[i]);
        return this;
    }
}

module.exports = MenuSection;