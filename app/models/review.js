const Model = require('./basemodel');
const Promise = require('bluebird');
const moment = require('moment');
const db = require('./../database');
const mongo_find = require('./../utils/mongo-find-promise');

class Review extends Model {
    constructor(modelObj) {
        super();

        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.restaurant_id) this.restaurant_id = modelObj.restaurant_id;
        if (modelObj.datetime) this.datetime = modelObj.datetime;
        if (modelObj.user) this.user = modelObj.user;
        if (modelObj.rate) this.rate = Number(modelObj.rate);
        if (modelObj.text) this.text = modelObj.text;
    }

    getRestaurants(pg) {
    }

    static convertRawData(data) {
        return new Review({
            _id: data._id,
            name: data.name,
            slug: data.slug,
            image: data.image,
            description: data.description
        });
    }

    save(mongodb){
        let nowTimestamp = moment().unix();
        this.datetime = nowTimestamp;
        return mongodb.collection('reviews').insertOne(
            {
                restaurant_id: this.restaurant_id,
                datetime: this.datetime,
                user: this.user,
                rate: this.rate,
                text: this.text
            });
    }

    static getReviewsOfRestaurant(restaurantId){
        return mongo_find.find(db.mongodb().collection('reviews'), {restaurant_id: restaurantId});
    }
}

module.exports = Review;