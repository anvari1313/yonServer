const Model = require('./basemodel');
const Promise = require('bluebird');

const db = require('./../database');
const util = require('util');

const staticFilePath = require('./../../config/static-file').tagImages;

class Tag extends Model {
    constructor(modelObj) {
        super();

        if (modelObj._id) this._id = modelObj.id;
        if (modelObj.name) this.name = modelObj.name;
        if (modelObj.slug) this.slug = modelObj.slug;
        if (modelObj.image) this.image = modelObj.image;
        if (modelObj.description) this.description = modelObj.description;

        if (modelObj.restaurants) this.restaurants = modelObj.restaurants;
    }

    getRestaurants(pg) {
        const Restaurant = require('./restaurant');

        let self = this;
        return new Promise(function (resolve, reject) {
            try {
                if (self._restaurants) {
                    resolve(self._restaurants)
                } else {
                    pg.any(Tag.query.getRestaurants, [self.id])
                        .then(data => Restaurant.convert(data))
                        .then(restaurants => self._restaurants = restaurants)
                        .then(x => resolve(self._restaurants))
                        .catch(reject)
                }
            } catch (exp) {
                reject(exp);
            }
        });
    }

    static getTagsOfRestaurant(pg, restaurantId) {
        return pg.any(Tag.query.getTagsOfRestaurant, [restaurantId]);
    }

    static convertRawData(data) {
        return new Tag({
            _id: data._id,
            name: data.name,
            slug: data.slug,
            image: data.image,
            description: data.description
        });
    }

    convertPathToAbsolute(){
        if (this.image) this.image = staticFilePath.image.url + this.image;
    }

    static getAllTags(nameLikes){
        let query = util.format(' WHERE (name LIKE %s)', '\'%' + nameLikes[0] + '%\'');
        for (let i = 1; i < nameLikes.length; i++){
            let ro = util.format(' OR (name LIKE %s)', '\'%' + nameLikes[i] + '%\'');
            query += ro;
        }

        return db.pg().any(Tag.query.getAllLikeName + query);
    }

    static get query() {
        return {
            getAllLikeName: 'SELECT * FROM tags',
            findById: 'select * from tags where _id = $1',
            getRestaurants: 'select restaurants.* \n'
            + 'from tags \n'
            + 'join tags_exists on (tags._id = tags_exists.tag_id) \n'
            + 'join restaurants on (tags_exists.restaurant_id = restaurants._id) \n'
            + 'where (tags._id = $1)',
            getNFirstTags: 'SELECT * FROM tags LIMIT $1',
            getTagsOfRestaurant: 'SELECT tags.* FROM tags_exists INNER JOIN tags ON tags_exists.tag_id = tags._id WHERE restaurant_id = $1;'
        }
    }
}

module.exports = Tag;