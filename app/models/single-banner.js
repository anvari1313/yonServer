const Banner = require('./banner');

class SingleBanner extends Banner{
    constructor(sbanner){
        super(sbanner);
        this.is_single_banner = true;
        this.color_code = sbanner.color_code;
        this.rate = sbanner.rate;
    }
}

module.exports = SingleBanner;