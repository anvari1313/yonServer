const Promise = require('bluebird');
const bcrypt = require('bcrypt');
const db = require('../database');
const Model = require('./basemodel');
const moment = require('moment-jalaali');

/**
 * _id          -> integer
 * fname        -> varchar(25)
 * lname        -> varchar(25)
 * birth        -> timestamp without timezone
 * point        -> integer
 * cash         -> bigint
 * cell_phone   -> varchar(15)
 * password     -> varchar(120)
 * avatar       -> varchar(100)
 */
class User extends Model {
    constructor(userObj) {
        super();
        if (userObj._id) this._id = userObj._id;
        if (userObj.fname) this.fname = userObj.fname;
        if (userObj.lname) this.lname = userObj.lname;
        if (userObj.birthdate) this._birthdate = userObj.birthdate; else this._birthdate = 0;
        if (userObj.point) this.point = userObj.point;
        if (userObj.cash) this.cash = userObj.cash;
        if (userObj.cell_phone) this.cell_phone = userObj.cell_phone;
        if (userObj.avatar) this.avatar = userObj.avatar;
        if (userObj.password) this.password = userObj.password;
        if (userObj.email) this.email = userObj.email;
        if (userObj.auth_sms_token) this.auth_sms_token = userObj.auth_sms_token;
        if (userObj.auth_sms_token_generated_at) this.auth_sms_token_generated_at = userObj.auth_sms_token_generated_at;
        if (userObj.is_cell_phone_verified) this.is_cell_phone_verified = userObj.is_cell_phone_verified;
    }

    get birthdate() {
        return moment.unix(this._birthdate);
    }

    set birthdate(bd) {
        if (typeof bd === 'number')
            this._birthdate = bd;
    }

    static convertRawData(obj) {
        return new User({
            _id: obj._id,
            fname: obj.fname,
            lname: obj.lname,
            birthdate: obj.birthdate,
            point: obj.point,
            cash: obj.cash,
            cell_phone: obj.cell_phone,
            avatar: obj.avatar,
            password: obj.password,
            email: obj.email,
            auth_sms_token: obj.auth_sms_token,
            auth_sms_token_generated_at: obj.auth_sms_token_generated_at,
            is_cell_phone_verified: obj.is_cell_phone_verified
        });
    }

    convertAvatarUrl(){

    }

    comparePassword(_password) {
        let self = this;
        return Promise.resolve(_password).then(password => bcrypt.compare(password, self.password));
    }

    compareToken(_token, validInterval) {
        let now = moment().unix();
        return new Promise((resolve, reject) => {
            if (_token === this.auth_sms_token){
                console.log(now);
                console.log(this.auth_sms_token_generated_at);
                console.log(validInterval);
                if ((now - this.auth_sms_token_generated_at) < validInterval)
                    resolve();
                else
                    reject('token is expired');
            }
            else
                reject('token is not correct')
        });
    }

    hashPassword() {
        let self = this;
        return new Promise((resolve, error)=>{
            if (this.password)
            bcrypt.genSalt(10, (err, salt)=>{
                if (err)
                    error(err);
                else {
                    bcrypt.hash(self.password, salt, (err, hash)=>{
                        if (err)
                            error(err);
                        else {
                            self.password = hash;
                            resolve();
                        }
                    });
                }
            })
            else
                resolve();
        });
    }

    saveToDatabase() {
        let me = this;

        let query = (this.password)?
            'INSERT INTO users (fname, lname, birthdate, cell_phone, email, password, avatar, is_verified) ' +
            'VALUES (${fname}, ${lname}, ${birthdate}, ${cell_phone}, ${email}, ${password}, ${avatar}, ${is_verified}) RETURNING *'
            :
            'INSERT INTO users (fname, lname, birthdate, cell_phone, email, avatar, is_verified) ' +
            'VALUES (${fname}, ${lname}, ${birthdate}, ${cell_phone}, ${email}, ${avatar}, ${is_verified}) RETURNING *';
        return db.pg().one(query,
            {
                fname: me.fname,
                lname: me.lname, birthdate: me.birthdate,
                cell_phone: me.cell_phone,
                email: me.email,
                password: me.password,
                is_verified: false,
                avatar: me.avatar
            });
    }

    static get query() {
        return {
            all: 'select *, extract(epoch from birthdate) as birthdate from users',
            findById: 'select *, extract(epoch from birthdate) as birthdate from users where _id = $1',
            findByCellPhone: 'SELECT *, EXTRACT(EPOCH from birthdate) as birthdate from users where _id = $1',
            findInfoById: 'select _id, fname, lname, email, extract(epoch from birthdate) as birthdate, avatar, cell_phone from users where _id = $1',
            findByEmail: 'select * from users where email = $1',
            findBySub: 'SELECT * FROM google_auth WHERE sub = $1;',
            saveSMSAuthToken: 'UPDATE users SET (auth_sms_token, auth_sms_token_generated_at) = ($2, to_timestamp($3)::timestamp without time zone) WHERE cell_phone = $1 RETURNING *;',
            findByCellPhone: 'SELECT *,EXTRACT(EPOCH FROM auth_sms_token_generated_at) FROM users WHERE cell_phone = $1;',
            findUserIdByGoogleSUB: 'SELECT * FROM google_auth WHERE sub = $1;',
            verifyCellPhone: 'UPDATE users SET is_cell_phone_verified = TRUE WHERE _id = $1 RETURNING *'
        }
    }

    static addToGoogleAuth(userId, sub) {
        return db.pg().one('INSERT INTO google_auth (user_id, sub) VALUES ($1, $2) RETURNING *;', [userId, sub])
    }

    static findUserIdBySub(sub) {
        return new Promise((resolve, reject) => {
            db.pg().one(User.query.findBySub, [sub]).then(result => resolve(result.user_id)).catch(error => {
                console.log(error);
                resolve()
            });
        });
    }

    static findUserById(id) {
        return db.pg().one(User.query.findInfoById, [id]);
    }

    static saveSMSToken(cellPhone, token){
        let now = moment().unix();
        return db.pg().one(User.query.saveSMSAuthToken, [cellPhone, token, now]).
        catch(error => Promise.reject('cell_phone not found'));
    }

    static hashPassword(password) {
        const saltRounds = 10;
        return bcrypt.hash(password, saltRounds)
    }

    static isSignUpWithGoogle(sub){
        return new Promise((resolve)=> {
            db.pg().one(User.query.findUserIdByGoogleSUB, [sub]).
            then(res => resolve(res)).
            catch(error => resolve(false))
        });
    }

    static findUserByEmail(email){
        return new Promise((resolve)=> {
            db.pg().one(User.query.findByEmail, [email]).
            then(res => resolve(res)).
            catch(error => {
                resolve(false)
                console.log(error);
            } )
        });
    }

    verifyCellphone(){
        return db.pg().one(User.query.verifyCellPhone, [this._id]);
    }

    removePrivateData(){
        delete this.password;
        delete this.auth_sms_token_generated_at;
        delete this.auth_sms_token;
        delete this.is_cell_phone_verified;
    }
}

module.exports = User;