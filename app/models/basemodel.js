const Promise = require('bluebird');
const bcrypt = require('bcrypt');

/**
 * Created by amirhosein on 3/8/17.
 */
class Model {
    constructor() {
        if (this.constructor === Model) {
            throw new TypeError("Can not construct abstract class.");
        }
    }

    save() {
        throw new TypeError("Do not call abstract method foo from child.");
    }

    static convert(data) {
        let self = this;
        return new Promise(function (resolve, reject) {
            try {
                let result = Array.isArray(data) ?
                    self.convertList(data) :
                    self.convertRawData(data);
                resolve(result);
            } catch (error) {
                reject(error);
            }
        });
    }

    static convertRawData(data) {
        throw new TypeError('Not Implemented');
    }

    static *convertList(lst) {
        if (!Array.isArray(lst)) {
            throw new Error('Not a list');
        }

        for (let item of lst) {
            yield this.convertRawData(item);
        }
    }


}

module.exports = Model;