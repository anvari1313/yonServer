const Model = require('./basemodel');

class UserTransaction extends Model{
    constructor(modelObj){
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.user_id) this.user_id = modelObj.user_id;
        if (modelObj.amount) this.amount = modelObj.amount;
        if (modelObj.type) this.type = modelObj.type;
    }


    static convertRawData(rawObj){
    }

}

module.exports = UserTransaction;