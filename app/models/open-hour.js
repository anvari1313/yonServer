const Model = require('./basemodel');
const ObjectId = require('mongodb').ObjectId;
const mongo_find_promise = require('./../utils/mongo-find-promise');
const db = require('./../database');
const commonUtil = require('./../utils/common');
const Promise = require('bluebird');

const jMoment = require('moment-jalaali');

/**
 * The formation of this model is non relational and has the following prototype
 * OpenHour{
 *  base: [
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}],
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}],
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}],
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}],
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}],
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}],
 *      [{start: 39600, end: 59400}, {start: 70200, end: 86400}]
 *  ],
 *  restaurant_id: 1,
 *  exceptions: [{date: 1503552903, open: [{start: 43200, end: 54000}]}]
 *
 *
 * }
 */
class OpenHour extends Model {
    constructor(modelObj) {
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.restaurant_id) this.restaurant_id = modelObj.restaurant_id;
        this.days = modelObj.days || [[], [], [], [], [], [], []];
        if (modelObj.base) this.base = modelObj.base;
        if (modelObj.exceptions) this.exceptions = modelObj.exceptions;
    }

    static convertRawData(data) {
        let modelObj;
        if (data){
            modelObj = {
                _id : data._id,
                restaurant_id: data.restaurant_id,
                base: data.base,
                exceptions: data.exceptions,
                days: data.days
            };
        }else
            modelObj = {};

        let openHour = new OpenHour(modelObj);

        return openHour;
    }

    removeExtraFields(){
        delete this._id;
        delete this.restaurant_id;
        delete this.exceptions;
        delete this.base;
        return this.days;
    }

    processExceptions(inputDate){
        let date;
        if (inputDate) date = jMoment(parseInt(inputDate) * 1000); else date = jMoment();

        if (inputDate){
            let dayOfWeek = date.day();

            this.days = this.base[dayOfWeek % 7];
            for (let exception of this.exceptions){
                let s1 = commonUtil.convertTimestampToDaystamp(exception.date);
                let s2 = commonUtil.convertTimestampToDaystamp(date.unix());
                console.log('s1 : ' + s1 + ', s2 ' + s2);
                if (s1 === s2)
                    this.days = exception.open;
            }
        }else{
            for (let i = 0; i < 7; i++){
                let dayOfWeek = date.day();

                for (let exception of this.exceptions){
                    let s1 = commonUtil.convertTimestampToDaystamp(exception.date);
                    let s2 = commonUtil.convertTimestampToDaystamp(date.unix());
                    if (s1 === s2)
                        this.days[dayOfWeek % 7] = exception.open;
                    else
                        this.days[dayOfWeek % 7] = this.base[dayOfWeek % 7];
                }
                date = date.add('days', 1);
            }
        }

    }

    saveToDb(){
        db.mongodb().collection('open_hours').insertOne({restaurant_id: this.restaurant_id, base: this.base, exceptions:this.exceptions})
    }

    static openHoursOfARestaurant(restaurantId){
        return db.mongodb().collection('open_hours').findOne({restaurant_id: restaurantId});
    }

    check(){
        db.mongodb().collection('open_hours').find({$where: function () { return this.days[0].start > 3; }})
    }
}

module.exports = OpenHour;