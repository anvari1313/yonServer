/**
 * Created by amirhosein on 3/11/17.
 */
const moment = require('moment-jalaali');

const Model = require('./basemodel');
const User = require('./user');
const Restaurant = require('./restaurant');
const db = require('./../database');
const Promise = require('bluebird');

class Reservation extends Model {
    constructor(objectModel) {
        super();

        this._id = objectModel._id;
        this.datetime = objectModel.datetime || 0;
        this.guest_count = objectModel.guest_count || 1;

        if (objectModel.user_id) this.user_id = objectModel.user_id;
        this.table_id = objectModel.table_id || null;

        if (objectModel.restaurant_id) this.restaurant_id = objectModel.restaurant_id || null;

        if (objectModel.createdAt) this._createdAt = objectModel.createdAt;
        if (objectModel.updatedAt) this._updatedAt = objectModel.updatedAt;
        if (objectModel.description) this.description = objectModel.description;
        if (objectModel.user) this.user = objectModel.user;
        if (objectModel.temp_user_id) this.temp_user_id = objectModel.temp_user_id;
        if (objectModel.is_canceled || objectModel.is_canceled === false) this.is_canceled = objectModel.is_canceled;
    }

    static get RESERVATION_FIELDS(){
        return ' reservations._id AS reservation_id, EXTRACT(EPOCH FROM reservations.datetime) AS datetime, reservations.guest_count, reservations.table_id, restaurant_id, reservations.is_canceled ';
    }

    get createdAt() {
        return moment.unix(this.createdAt);
    }

    get updatedAt() {
        return moment.unix(this.updatedAt);
    }

    static findById(id, mongodb, pg) {
        return new Promise((resolve, reject) => {
            const ObjectID = require('mongodb').ObjectID;

            const Table = require('../models/table');

            let rawReservation = null;
            let rawTable = null;

            pg.one(Reservation.query.findByIdWithUser, [id])
                .then(data => rawReservation = data)
                .then(x => mongodb.collection('tables').findOne({_id: ObjectID.createFromHexString(rawReservation.table_id)}))
                .then(data => rawTable = data)
                .then(x => pg.one(Restaurant.query.findById, [rawTable.restaurant_id]))
                .then(data => Restaurant.convert(data))
                .then(restaurant => Table.convert({obj: rawTable, restaurant: restaurant}))
                .then(table => Reservation.convert({obj: rawReservation, table: table}))
                .then(resolve)
                .catch(reject);
        })
    }

    static isValidReservationAfterMax(pg, restaurantId) {
        return pg.one(Reservation.query.isReservationValidAfterMax, [restaurantId]);
    }

    static reservationsOfADay(pg, restaurantId, dayTimestamp) {
        return pg.any(Reservation.query.allReservationsOfADay, [restaurantId, dayTimestamp]);
    }

    static allReservationsIntersectsWithTime(pg, restaurantId, timestamp, maxTime) {
        let arr = [restaurantId, timestamp, maxTime];
        return pg.any(Reservation.query.allReservationsIntersectsWithTime, arr);
    }

    static isReservationOwnToUser(reservationId, userId) {
        return db.pg().one(Reservation.query.reservationOwnToUser, [reservationId, userId])
    }

    static isReservationOwnToRestaurant(reservationId, restaurantId){
        return db.pg().one(Reservation.query.reservationOwnToRestaurant, [reservationId, restaurantId]);
    }

    /**
     * This function checks whether the reservation with the given table is allowed or not
     * First check if the table is not reserved. If it is not reserved then true is returned
     * If the table is reserved then check the restaurant if it allow reservation after max_time then check the time of the reservation and the time of
     * last reservation
     * @param params
     */
    static checkIfReservable(pg, params) {
        return pg.one(Reservation.query.isReservationValidQuery, [params.restaurant_id, params.table_id, params.datetime]);
    }

    static convertRawData(data) {
        let obj = data.obj || data;
        let table = data.table;

        console.log(obj);
        if (!obj) {
            throw new Error('obj is required');
        }

        let reservation = new Reservation({
            _id: obj.reservation_id || obj._id,
            user_id: obj.user_id,
            temp_user_id: obj.temp_user_id,
            datetime: obj.datetime,
            guest_count: obj.guest_count,
            table_id: obj.table_id,
            is_canceled: obj.is_canceled,
            restaurant_id: obj.restaurant_id,
            createdAt: obj.createdAt,
            updatedAt: obj.updatedAt,
            description: obj.description
        });

        reservation.table = table || null;
        reservation.user = obj.user ? User.convertRawData(obj.user) : null;

        return reservation;
    }

    static convertRawDataWithUser(data) {

        let reservation = new Reservation(
            {
                _id: data.reservation_id || data._id,
                datetime: data.datetime,
                guest_count: data.guest_count,
                table_id: data.table_id,
                restaurant_id: data.restaurant_id,
                user: {
                    _id: data.user_id,
                    fname: data.user_fname,
                    lname: data.user_lname
                },
                is_canceled: data.is_canceled,
                description: data.description
            });

        return reservation;

    }

    saveToDatabase(db) {
        return db.pg().one(Reservation.query.insert, {
            user_id: this.user_id,
            datetime: this.datetime,
            guest_count: this.guest_count,
            table_id: this.table_id,
            temp_user_id: this.temp_user_id,
            restaurant_id: this.restaurant_id,
            description: this.description
        });
    }

    static get query() {
        return {
            findById: 'select * from reservations where _id = $1',
            findByIdWithUser: 'select R.*, row_to_json(U.*) as user \n'
            + 'from reservations as R \n'
            + 'inner join users as U on (R.user_id = U._id) \n'
            + 'where (R._id = $1)',
            insert: 'INSERT INTO reservations(user_id, temp_user_id, datetime, guest_count, table_id, "createdAt", "updatedAt", restaurant_id, description)' +
            'VALUES (${user_id}, ${temp_user_id}, to_timestamp(${datetime}) AT TIME ZONE \'UTC\', ${guest_count}, ${table_id}, now(), now(), ${restaurant_id}, ${description}) RETURNING *,EXTRACT (EPOCH FROM datetime) AS datetime',
            reservationsOfRestaurant: 'SELECT reservations._id,extract(epoch from reservations.datetime) as datetime, guest_count, table_id, user_id, users.fname AS user_fname, users.lname AS user_lname, is_canceled, description\n' +
            'FROM reservations INNER JOIN users ON reservations.user_id = users._id WHERE restaurant_id = $1;',
            reservationsOfRestaurantAnonymousUser: 'SELECT reservations._id,extract(epoch from reservations.datetime) as datetime, guest_count, table_id, temp_user_id, temp_users.name AS user_lname, is_canceled, description\n' +
            'FROM reservations INNER JOIN temp_users ON reservations.temp_user_id = temp_users._id WHERE restaurant_id = $1;',
            reservationsOfRestaurantBetweenTwoTimestamp: 'SELECT reservations._id,extract(epoch from reservations.datetime) as datetime, guest_count, table_id, user_id, users.fname AS user_fname, users.lname AS user_lname, is_canceled, description\n' +
            'FROM reservations INNER JOIN users ON reservations.user_id = users._id WHERE (restaurant_id = $1) AND (extract(epoch from reservations.datetime) between $2 AND $3);',
            reservationsOfRestaurantBetweenTwoTimestampAnonymousUser: 'SELECT reservations._id,extract(epoch from reservations.datetime) as datetime, guest_count, table_id, temp_user_id, temp_users.name AS user_lname, is_canceled, description\n' +
            'FROM reservations INNER JOIN temp_users ON reservations.user_id = temp_users._id WHERE (restaurant_id = $1) AND (extract(epoch from reservations.datetime) between $2 AND $3);',
            numberOfReservationsOfUser: 'SELECT COUNT(_id) FROM reservations WHERE user_id = $1;',
            allReservationsOfADay: 'SELECT extract(epoch from datetime) AS datetime, table_id FROM reservations WHERE ' +
            'restaurant_id = $1 AND (to_timestamp($2) AT TIME ZONE \'UTC\')::date = datetime::date AND table_id IS NOT NULL;',
            isReservationValidAfterMax: 'SELECT is_valid_res_after_max, max_table FROM restaurants WHERE _id = $1',
            // allReservationsIntersectsWithTime:'SELECT table_id FROM reservations WHERE restaurant_id = $1 AND ($2 BETWEEN (EXTRACT (EPOCH FROM datetime) AND EXTRACT (EPOCH FROM datetime) + $3);'
            allReservationsIntersectsWithTime: 'SELECT table_id FROM reservations WHERE (restaurant_id = $1) AND (is_canceled = FALSE) AND (table_id IS NOT NULL) AND ' +
            '(($2 BETWEEN (EXTRACT (EPOCH FROM datetime)) AND ((EXTRACT (EPOCH FROM datetime)) + $3)) ' +
            // 'OR ($2 + $3 > (EXTRACT (EPOCH FROM datetime))) ' +
            ');',
            isReservationValidQuery: 'SELECT public."isReservationValid"($1, $2::text, (to_timestamp($3) AT TIME ZONE \'UTC\')::timestamp without time zone);',
            pendingReservationsOfUser: 'SELECT ' + Reservation.RESERVATION_FIELDS + ', ' + Restaurant.MID_RESTAURANT_FIELDS + ' FROM reservations INNER JOIN restaurants ON reservations.restaurant_id = restaurants._id WHERE reservations.user_id = $1 AND reservations.datetime > now();',
            pendingReservationsOfUserOfRestaurant: 'SELECT ' + Reservation.RESERVATION_FIELDS + ' FROM reservations WHERE reservations.user_id = $1 AND reservations.datetime > now() AND restaurant_id = $2;',
            reservationOwnToUser: 'SELECT _id FROM reservations WHERE _id = $1 AND user_id = $2;',
            reservationOwnToRestaurant: 'SELECT _id FROM reservations WHERE _id = $1 AND restaurant_id = $2;',
            cancelReservation: 'UPDATE reservations SET is_canceled = true WHERE _id = $1 RETURNING *, EXTRACT (EPOCH FROM datetime) AS datetime;',
            assignTable: 'UPDATE reservations SET table_id = $2 WHERE _id = $1 RETURNING *;'
        };
    }

    static cancelReservation(reservationId) {
        return db.pg().one(Reservation.query.cancelReservation, [reservationId])
    }

    static assignTable(reservationId, tableId){
        return db.pg().one(Reservation.query.assignTable, [reservationId, tableId]);
    }
}

module.exports = Reservation;