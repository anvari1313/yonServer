const Model = require('./basemodel');
const util = require('util');

class Zone extends Model{
    constructor(zoneObj){
        super();

        this.name = zoneObj.name;
        this.slug = zoneObj.slug;
        this.long = zoneObj.long;
        this.lat = zoneObj.lat;

        if (zoneObj.distance) this.distance = zoneObj.distance;
    }

    addZone(dbSession){
        return dbSession.run(Zone.getCypher().newZone,
            {
                nameParameter: this.name,
                slugParameter: this.slug,
                longParameter: this.long,
                latParameter: this.lat
            });
    }

    static convertRawData(rawObj){
        let propertyObj = rawObj._fields[0].properties;
        let distance = rawObj._fields[1];

        return new Zone({
            name: propertyObj.name,
            slug: propertyObj.slug,
            long: propertyObj.long,
            lat: propertyObj.lat,
            distance: distance
        });
    }
    static connectZone(dbSession, zone1, zone2){
        return dbSession.run(Zone.getCypher().connectZone,
            {slugParameter1: zone1.slug, slugParameter2: zone2.slug});
    }

    static getNeighbourZones(dbSession, zoneSlug){
        return dbSession.run(Zone.getCypher().findNeighbourZones, {slugParameter:zoneSlug});
    }

    static getAllZones(dbSession){
        return dbSession.run(Zone.getCypher().allZones);
    }

    static getCypher(){
        return {
            newZone: 'CREATE (newZone:Zone { name: {nameParameter}, slug: {slugParameter}, long: {longParameter}, lat: {latParameter} }) RETURN newZone;',
            connectZone: 'MATCH (zone1:Zone), (zone2:Zone) ' +
            'WHERE zone1.slug = {slugParameter1} AND zone2.slug = {slugParameter2} ' +
            'CREATE (zone1)-[:Neighbour]->(zone2), (zone2)-[:Neighbour]->(zone1);',
            findNeighbourZones:'MATCH (zone:Zone)-[:Neighbour]-(neighbourhoods) ' +
            'WHERE zone.slug = {slugParameter} RETURN neighbourhoods;',
            allZones: 'MATCH (zone:Zone) RETURN zone;',
            similarNames: 'MATCH (zone:Zone)',
            nearestZoneToLocation: 'MATCH (z:Zone) RETURN z,' +
            'sqrt((z.long - {longParameter}) ^ 2 + (z.lat - {latParameter}) ^ 2) AS distance ORDER BY distance ASC LIMIT 1;'
        };
    }

    static mostSimilarNamesZones(dbSession, zoneNames){
        if (Array.isArray(zoneNames)){
            let query = util.format(Zone.getCypher().similarNames + ' WHERE (zone.name CONTAINS %s)', '\'' + zoneNames[0] + '\'');
            for (let i = 1; i < zoneNames.length; i++){
                let ro = util.format(' OR (zone.name CONTAINS %s)', '\'' + zoneNames[i] + '\'');
                query += ro;
            }
            query += ' RETURN zone;';
            console.log(query);
            return dbSession.run(query);
        }else{
            return dbSession.run(Zone.getCypher().similarNames + ' WHERE zone.name CONTAINS {nameParameter} RETURN zone;', {nameParameter:zoneNames});
        }
    }

    static nearestZone(dbSession, location){
        console.log(location);
        return dbSession.run(Zone.getCypher().nearestZoneToLocation, {longParameter:location.long, latParameter: location.lat});
    }

}

module.exports = Zone;