const Model = require('./basemodel');
const db = require('../database');
const ObjectId = require('mongodb').ObjectId;
const mongo_find_promise = require('./../utils/mongo-find-promise');

class Map extends Model {
    constructor(modelObj) {
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.restaurant_id) this.restaurant_id = modelObj.restaurant_id;
        if (modelObj.name) this.name = modelObj.name;
        if (modelObj.width) this.width = modelObj.width;
        if (modelObj.height) this.height = modelObj.height;
        this.tables = modelObj.tables || [];
    }

    static convertRawData(data) {
        let obj = data.obj;
        let restaurant = data.map;

        if (!obj) {
            throw new Error('obj is required');
        }

        let map = new Map({
            _id: obj._id,
            restaurant_id: obj.restaurant_id,
            name: obj.name,
            width: obj.width,
            height: obj.height,
            tables: obj.tables
        });


        return map;
    }


    add() {
        let insertedObject = {};
        insertedObject.restaurant_id = this.restaurant_id;
        insertedObject.name = this.name;
        insertedObject.width = this.width;
        insertedObject.height = this.height;
        if (this.tables) insertedObject.tables = this.tables;
        else insertedObject.tables = [];


        return db.mongodb().collection('maps').insertOne(insertedObject);
    }

    static addMany(maps) {
        return db.mongodb().collection('maps').insertMany(maps);
    }

    static getMap(restaurantId, mapId) {
        return (mapId) ?
            mongo_find_promise.find(db.mongodb().collection('maps'), {
                _id: new ObjectId(mapId),
                restaurant_id: restaurantId
            }) :
            mongo_find_promise.find(db.mongodb().collection('maps'), {restaurant_id: restaurantId});
    }

    static addTableToMap(mapId, table) {
        mongo_find_promise.find(db.mongodb().collection('maps'), {_id: new ObjectId(mapId)}).then((map) => {
            let tables = map[0].tables;
            if (tables === 'undefined' || !Array.isArray(tables)) {
                let tmeptables = [];

                tmeptables.push(table);
                map.tables = tmeptables;
                return db.mongodb().collection('maps').updateOne({_id: new ObjectId(mapId)}, {$push: {tables: tmeptables}})
            } else {
                if (!Array.isArray((table))) table = [table];
                return db.mongodb().collection('maps').updateOne({_id: new ObjectId(mapId)}, {$push: {tables: {$each: table}}})
            }

        });
    }

    static searchForTableWithId(tableId) {
        try {
            tableId = ObjectId(tableId);
            return db.mongodb().collection('maps').findOne({tables: {$elemMatch: {_id: tableId}}});
        } catch (error) {
            return new Promise((resolve, reject) => reject(error));
        }
    }

    static isValidMap(mapId) {
        return new Promise((resolve, reject) => {
            try {
                let objID = ObjectId(mapId);
                db.mongodb().collection('maps').findOne({_id: objID}).then((map) => {
                    if (map) resolve(map);
                    else reject('Not a map');
                }).catch((error) => {
                    reject('Not a map');
                });
            } catch (error) {
                reject('Not a map');
            }
        });
    }

    getData() {
        console.log(this);
    }

}

module.exports = Map;