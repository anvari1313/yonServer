const Model = require('./basemodel');
const db = require('../database');
const ObjectId = require('mongodb').ObjectId;
const mongo_find_promise = require('./../utils/mongo-find-promise');
const moment = require('moment');


class Order extends Model {

    /**
     * @param modelObj{
     *  _id
     *  restaurant_id
     *  submit_datetime
     *  user_id
     *  eatables {
     *          _id
     *          name
     *          price
     *          num
     *      }
     * }
     */
    constructor(modelObj) {
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.restaurant_id) this.restaurant_id = modelObj.restaurant_id;
        if (modelObj.submit_datetime) this.submit_datetime = modelObj.submit_datetime;
        if (modelObj.user_id) this.user_id = modelObj.user_id;
        this.eatables = modelObj.eatables || [];
        if (modelObj.reservation_id) this.reservation_id = modelObj.reservation_id;
        if (modelObj.table_id) this.table_id = modelObj.table_id;
        if (modelObj.transaction_id) this.transaction_id = modelObj.transaction_id;
        if (modelObj.preparation_status) this.preparation_status = modelObj.preparation_status;
        if (modelObj.is_gone) this.is_gone = modelObj.is_gone;
        if (modelObj.is_table_unknown) this.is_table_unknown = modelObj.is_table_unknown;
        if (modelObj.order_pnum) this.order_pnum = modelObj.order_pnum;
        if (modelObj.is_takeaway) this.is_takeaway = modelObj.is_takeaway;
        if (modelObj.description) this.description = modelObj.description;
        this.createdAt = modelObj.createdAt || moment().unix();
        this.updatedAt = modelObj.updatedAt || moment().unix();
    }

    static get COLLECTION_NAME() { return 'orders'; }
    static convertRawData(data) {
        let modelObj =
            {
                _id: data._id,
                restaurant_id: data.restaurant_id,
                submit_datetime: data.submit_datetime,
                user_id: data.user_id,
                eatables: data.eatables,
                reservation_id: data.reservation_id,
                table_id: data.table_id,
                transaction_id: data.transaction_id,
                preparation_status: data.preparation_status,
                is_gone: data.is_gone,
                order_pnum: data.order_pnum,
                is_table_unknown: data.is_table_unknown,
                is_takeaway: data.is_takeaway,
                description: data.description,
                createdAt: data.createdAt,
                updatedAt: data.updatedAt
            };

        let order = new Order(modelObj);
        return order;
    }

    static findById(orderId) {
        return db.mongodb().collection(Order.COLLECTION_NAME).findOne({_id: ObjectId(orderId)});
    }

    addEatable(eatable) {
        this.eatables.push(eatable);
    }

    upsert() {
        if (this._id){
            this._id = ObjectId(this._id);
            return db.mongodb().collection(Order.COLLECTION_NAME).updateOne({_id: this._id}, {$set: this})
        } else
        // Insert is done
            return db.mongodb().collection(Order.COLLECTION_NAME).insertOne({
                restaurant_id: this.restaurant_id,
                submit_datetime: this.submit_datetime,
                eatables: this.eatables,
                reservation_id: this.reservation_id,
                table_id: this.table_id,
                transaction_id: this.transaction_id,
                preparation_status: this.preparation_status,
                is_gone: this.is_gone,
                order_pnum: this.order_pnum,
                is_table_unknown: this.is_table_unknown,
                is_takeaway: this.is_takeaway,
                description: this.description,
                createdAt: this.createdAt,
                updatedAt: this.updatedAt
            });
    }

    static listBetween(startTimestamp, endTimestamp){
        return mongo_find_promise.find(db.mongodb().collection(Order.COLLECTION_NAME),
            { submit_datetime: { $gt: startTimestamp, $lt: endTimestamp} });
    }
}

module.exports = Order;