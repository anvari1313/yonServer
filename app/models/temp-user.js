const Promise = require('bluebird');
const db = require('../database');
const Model = require('./basemodel');

/**
 * _id          -> integer
 * name         -> varchar(25)
 * cell_phone   -> varchar(15)
 */
class TempUser extends Model {
    constructor(userObj) {
        super();
        if (userObj._id) this._id = userObj._id;
        if (userObj.name) this.name = userObj.name;
        if (userObj.cell_phone) this.cell_phone = userObj.cell_phone;
        if (userObj.createdAt) this.createdAt = userObj.createdAt;
    }

    static get COLLECTION_NAME(){ return 'temp_users'; }

    save(){
        return db.pg().one(TempUser.query.insert, [this.name, this.cell_phone]);
    }

    static findById(id){
        return db.pg().one(TempUser.query.findById, [id]).
        then(rawObject=> Promise.resolve(new TempUser(rawObject)))
    }

    static get query(){
        return {
            insert: 'INSERT INTO ' + TempUser.COLLECTION_NAME + ' (name, cell_phone) VALUES ($1, $2) RETURNING *;',
            findById: 'SELECT * FROM ' + TempUser.COLLECTION_NAME + ' WHERE _id = $1;'
        };
    }
}

module.exports = TempUser;