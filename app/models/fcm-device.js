const Model = require('./basemodel');
const db = require('./../database');
const Promise = require('bluebird');

class FCMDevice extends Model{
    constructor(modelObj){
        super();
        if (modelObj._id) this._id = modelObj._id;
        if (modelObj.fcm_token_id) this.fcm_token_id = modelObj.fcm_token_id;
        if (modelObj.fcm_token_ids) this.fcm_token_ids = modelObj.fcm_token_ids;
        if (modelObj.user_id) this.user_id = modelObj.user_id;
        if (modelObj.createdAt) this.createdAt = modelObj.createdAt;
    }

    static get COLLECTION_NAME(){ return 'fcm_devices'; }

    save(){
        if (this.user_id)
            return db.pg().one(FCMDevice.query.insertWithUser, [this.fcm_token_id, this.user_id]);
        else
            return db.pg().one(FCMDevice.query.insertWithoutUser, [this.fcm_token_id]);
    }

    static updateByLastToken(oldToken, newToken){
        return db.pg().many(FCMDevice.query.updateByLastToken, [oldToken, newToken]);
    }

    static tokensOfUser(userId){
        console.log(userId);
        return new Promise((resolve, reject) => {
            let fcm_token_ids = [];
            db.pg().any(FCMDevice.query.findTokenIdsOfAUser, [userId]).
            then(results =>{ console.log(results); return Promise.each(results, (result) => fcm_token_ids.push(result.fcm_token_id)) } ).
            then(() => resolve(new FCMDevice({user_id : userId, fcm_token_ids: fcm_token_ids}))).
            catch(error => resolve(new FCMDevice({user_id : userId, fcm_token_ids: []})))
        });
    }

    static get query(){
        return {
            insertWithUser: 'INSERT INTO ' + FCMDevice.COLLECTION_NAME + ' (fcm_token_id, user_id) VALUES ($1, $2) RETURNING *;',
            insertWithoutUser: 'INSERT INTO ' + FCMDevice.COLLECTION_NAME + ' (fcm_token_id) VALUES ($1) RETURNING *;',
            updateByLastToken: 'UPDATE ' + FCMDevice.COLLECTION_NAME + ' SET fcm_token_id = $2 WHERE fcm_token_id = $1 RETURNING *;',
            findTokenIdsOfAUser: 'SELECT * FROM ' + FCMDevice.COLLECTION_NAME + ' WHERE user_id = $1;'
        };
    }
}

module.exports = FCMDevice;