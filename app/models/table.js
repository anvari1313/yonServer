/**
 * Created by amirhosein on 3/11/17.
 */
const Model = require('./basemodel');
const Map = require('./map');
const db = require('../database');
const ObjectId = require('mongodb').ObjectId;
const mongo_find_promise = require('./../utils/mongo-find-promise');

class Table extends Model {
    constructor(modelObj) {
        super();

        if (modelObj._id) this._id = modelObj._id;

        this.name = modelObj.name || '';
        this.shape_type = modelObj.shape_type || '';
        this.capacity = modelObj.capacity || 0;
        this.x = modelObj.x || 0.0;
        this.y = modelObj.y || 0.0;
        this.angle = modelObj.angle || 0.0;
        this.restaurant_id = modelObj.restaurant_id;
    }

    static convertRawData(data) {
        let obj = data.obj;
        let restaurant = data.restaurant;

        if (!obj) {
            throw new Error('obj is required');
        }

        let table = new Table({
            _id: obj._id,
            restaurant_id: obj.restaurant_id,
            name: obj.name,
            shape_type: obj.shape_type,
            x: obj.x, y: obj.y,
            angle: obj.angle
        });
        table.restaurant = restaurant || null;

        return table;
    }

    addOne(mapId) {
        let insertedObject = {};
        insertedObject.restaurant_id = this.restaurant_id;
        insertedObject.name = this.name;
        insertedObject.capacity = this.capacity;
        insertedObject.x = this.x;
        insertedObject.y = this.y;
        insertedObject.shape_type = this.shape_type;
        insertedObject.angle = this.angle;


        db.mongodb().collection('tables').insertOne(insertedObject).then((res) => {
            return Map.addTableToMap(mapId, res.ops[0]);
        }).catch((error) => {
            console.log(error);
        });
    }


    static addMany(restaurantId, mapId, tables) {
        let tableArr = [];
        Promise.each(tables, (table) => {
            let t = new Table({
                restaurant_id: restaurantId,
                name: table.name,
                capacity: table.capacity,
                shape_type: table.shape_type,
                x: table.x,
                y: table.y,
                angle: table.angle
            });
            tableArr.push(t);
        }).then(() => {
            db.mongodb().collection('tables').insertMany(tableArr).then((res) => {
                return Map.addTableToMap(mapId, res.ops);
            });
        }).catch((error) => new Promise((resolve, reject) => reject(error)));

        return new Promise((resolve, reject) => {
            resolve();
        });
    }

    static getTable(tableId) {
        return mongo_find_promise.find(db.mongodb().collection('tables'), {_id: ObjectId(tableId)})
    }

    static isValidTable(tableId) {
        return new Promise((resolve, reject) => {
            try {
                let objID = ObjectId(tableId);
                db.mongodb().collection('tables').findOne({_id: objID}).then((table) => {
                    if (table) resolve(table);
                    else reject('Not a table');
                }).catch((error) => {
                    reject('Not a table');
                });
            } catch (error) {
                reject('Not a table');
            }
        });
    }
}

module.exports = Table;