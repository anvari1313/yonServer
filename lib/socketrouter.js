var HashMap = require('hashmap');
const messageQ = require('../app/socket/messageQ/messageQ');

/**
 * A generalized SocketRouter for event handling
 */
class SocketRouter{
    constructor(){
        this._map = new HashMap();
    }

    on(eventHeader, eventHandler){
        this._map.set(eventHeader,eventHandler);
    }

    invoke(event, payload, connection){
        let eventHandler = this._map.get(event);

        if (eventHandler)
            eventHandler(payload, connection);
        else
            messageQ.sendError(connection.restaurantId, 'The specified event is not defined.', payload.requestId, 403)
    }
}

module.exports = SocketRouter;