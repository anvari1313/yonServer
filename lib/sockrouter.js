const HashMap = require('hashmap');

class SocketRouter{

    /**
     * Constructor
     * @param pref
     * @param handler
     */
    constructor(notFoundHandler, pref, handler){
        this._map = new HashMap();

        if (pref) {
            if (Array.isArray(pref)) {
                if (pref.length === 1)
                    this.on(pref[0], handler);
                else {
                    let router = new SocketRouter(this._notFoundHandler, pref.splice(1, pref.length), handler);
                    router.onNotFound(notFoundHandler);
                    this.on(pref[0], router);
                }
            } else
                throw new TypeError('Type of input must be array')
        }
    }

    /**
     * on function
     * @param prefRoute
     * @param routeHandler
     */
    on(prefRoute, routeHandler){
        if (((typeof routeHandler !== 'function') && !(routeHandler instanceof SocketRouter)))
            throw new TypeError('RouteHandler must be of type function or route handler');

        if (typeof prefRoute !== 'string') throw new TypeError('PrefRoute must be of type string');

        let split = prefRoute.split('/').filter(Boolean);
        if (split.length > 1) {
            let preRouteHandler = new SocketRouter(this._notFoundHandler, split.splice(1, split.length), routeHandler);
            let lastHandler = this._map.get(split[0]);
            if (lastHandler instanceof SocketRouter)
                lastHandler.on(split[0], preRouteHandler);
            else
                this._map.set(split[0], preRouteHandler);
        } else {
            this._map.set(split[0], routeHandler);
        }
    }

    /**
     * t
     * @param routeSequence
     * @param authorizationObject
     * @param requestId
     * @param messagePayload
     */
    call(routeSequence, authorizationObject, requestId, messagePayload){
        let payload = messagePayload;

        routeSequence = (Array.isArray(routeSequence)) ? routeSequence : routeSequence.split('/').filter(Boolean);

        let retrievedRouteHandler = this._map.get(routeSequence[0]);

        if (typeof retrievedRouteHandler === 'function') {
            retrievedRouteHandler(authorizationObject, requestId, payload);
        } else if (retrievedRouteHandler instanceof SocketRouter){
            retrievedRouteHandler.call(routeSequence.splice(1, routeSequence.length), authorizationObject, requestId, messagePayload);
        } else {
            this._notFoundHandler(authorizationObject, requestId, payload);
        }
    }

    /**
     *
     * @param notFoundHandler
     */
    onNotFound(notFoundHandler){
        this._notFoundHandler = notFoundHandler;
    }
}

module.exports = SocketRouter;